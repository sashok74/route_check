//---------------------------------------------------------------------------
#ifndef TimerH
#define TimerH

#include <pthread.h>

//---------------------------------------------------------------------------
typedef  void ( *TTimerEvent)(int, void *);
typedef  void * ( *TPThreadFunc) (void *);
//---------------------------------------------------------------------------
class TTimer
{
 private:
   TPThreadFunc    tfuncptr;
   pthread_t       thread_id;
   pthread_mutex_t mutex;
   int number;                // номер для идентификации
 public:
   char runned;
   int interval;              // интервал таймера, c

   void          * user_data;      // пользовательские данные для callback'a
   TTimerEvent     parent_event;   // callback

   int            user_data_i;     // пользовательские данные типа int

   TTimer(const int);
   TTimer(const int, const int);
   ~TTimer(void);

   void set_interval(int);
   void start(void);
   void stop(void);                // Из внешнего потока
   void finish(void);              // Из этого же потока
   void timer(void);

};
//---------------------------------------------------------------------------
void * TimerThread (void *);
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


#endif


