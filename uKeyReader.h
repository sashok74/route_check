//---------------------------------------------------------------------------
#ifndef KeyReaderH
#define KeyReaderH

#include <string>
#include <list>

#include "serial.h"
//---------------------------------------------------------------------------
typedef struct
{
 int32_t       header;
 unsigned char devNumber;
 int64_t       keyNumber;
 unsigned char crc;
 unsigned char data[12];
} TKRMsg; // Key Reader Message
//---------------------------------------------------------------------------
/*typedef union
{
 unsigned char ch[8];
 int64_t       i;
} Tui64;*/
//---------------------------------------------------------------------------
class TKeyReader
{
// public:
//   enum PortState{PortDown, PortUp};
// protected:
//   PortState portState_;
//   pthread_mutex_t mutex_portState;
 public:
   TSerial port;
   TSerialReaded sreaded;     // callback
   std::list<unsigned char> qu;

   void * owner;

   TKeyReader(void);
  virtual ~TKeyReader(void);

  virtual int64_t keyNumber(void) = 0;
  virtual unsigned char devNumber(void) = 0;
  virtual char start(void) = 0;
  virtual char read_queue() = 0;
  virtual void see_queue(const unsigned int) = 0;
  virtual void sendKey(char) = 0;

  void setPort(const std::string);

//  virtual PortState portState();
//  virtual void portUp();
//  virtual void portDown();
};
//---------------------------------------------------------------------------
class TPlanarReader : public TKeyReader
{
 private:
 public:
   TKRMsg msg;

   TPlanarReader(void);
//  virtual ~TKeyReader(void);
  virtual int64_t keyNumber(void);
  virtual unsigned char devNumber(void);
  virtual char start(void);
  virtual char read_queue();
  virtual void see_queue(const unsigned int);
  virtual void sendKey(char);
};
//---------------------------------------------------------------------------
class TAladdinReader : public TKeyReader
{
 private:
  std::list<unsigned char> magic_number;
  int64_t       key;
 public:
  TAladdinReader();

  virtual int64_t keyNumber(void);
  virtual unsigned char devNumber(void);
  virtual char start(void);
  virtual char read_queue();
  virtual void see_queue(const unsigned int);
  virtual void sendKey(char);
};
//---------------------------------------------------------------------------
void port_event(unsigned char v, unsigned int s, void * p);
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
