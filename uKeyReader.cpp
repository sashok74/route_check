//---------------------------------------------------------------------------
#include <string.h>
#include "service.h"
#include "log.h"
#include "uKeyReader.h"


extern TLog * l;
//---------------------------------------------------------------------------
TKeyReader::TKeyReader(void)
{
 owner         = NULL;
 qu.clear();
// pthread_mutex_init(&mutex_portState, NULL);
// portDown();
}
//---------------------------------------------------------------------------
TKeyReader::~TKeyReader(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
 qu.clear();
}
//---------------------------------------------------------------------------
void TKeyReader::setPort(const std::string p)
{
 port.port = p;
}
//---------------------------------------------------------------------------
//TKeyReader::PortState TKeyReader::portState()
//{
// PortState r;
// pthread_mutex_lock(&mutex_portState);
// r = portState_;
// pthread_mutex_unlock(&mutex_portState);
// return(r);
//}
//---------------------------------------------------------------------------
//void TKeyReader::portUp()
//{
// pthread_mutex_lock(&mutex_portState);
// portState_     = PortUp;
// pthread_mutex_unlock(&mutex_portState); 
//}
//---------------------------------------------------------------------------
//void TKeyReader::portDown()
//{
// pthread_mutex_lock(&mutex_portState);
// portState_     = PortDown;
// pthread_mutex_unlock(&mutex_portState);
//}
//---------------------------------------------------------------------------
TPlanarReader::TPlanarReader(void) : TKeyReader()
{
 msg.keyNumber = 0;
}
//---------------------------------------------------------------------------
int64_t TPlanarReader::keyNumber(void)
{
 return (msg.keyNumber);
}
//---------------------------------------------------------------------------
unsigned char TPlanarReader::devNumber(void)
{
 return (msg.devNumber);
}
//---------------------------------------------------------------------------
char TPlanarReader::read_queue(void)
{
 std::list<unsigned char>::iterator itr;
 int i;
 int zcrc  = 0;
 int m = 0;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (qu.size()<12) {return (0);}
 memset(&msg, 0, sizeof(TKRMsg));

 i = 0;
 for(itr = qu.begin(); itr != qu.end(); ++itr)
 {
  if (i == 0) {msg.header = *itr;}
  if (i == 1) {msg.header = (msg.header << 8) + *itr;}
  if (i == 2) {msg.devNumber = *itr;}
  if (i >= 3 && i<= 10)
  {
    int64_t v;
    v = *itr;
    v <<= (8 * m);
    msg.keyNumber += v;
    m++;
  }
  if (i < 11) {zcrc += *itr;}
  msg.data[i] = *itr;
  if (i == 11) 
  {msg.crc = *itr;
   break;}
  i++;
 }

 if ((msg.header & 0xFFFF) == 0xAA55)
 { 
  if ((unsigned char)(zcrc + msg.crc) == 0)
  {
   return (1);
  }
  else
  {
  //printf("msg check failed\nheader=%ld, zcrc=%d, crc=%d\n", msg.header, zcrc, msg.crc );
  std::stringstream stm;
  stm << "Error key CRC " << std::hex << (int) zcrc << " != " << std::hex << (int) msg.crc;
  log(stm.str());

  return (0);
  } // if ((unsigned char)(zcrc + msg.crc) == 0)
 }
 else
 {
  return (0);
 } // if ((msg.header & 0xFFFF) == 0xAA55)
}
//---------------------------------------------------------------------------
char TPlanarReader::start(void)
{
 char ret(0);
 port.sreaded = port_event;
 port.sreaded_param = this;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 port.open_port();
 if (port.opened != 1) {return (ret);}
// portUp(); 
 ret = port.run();
 return (ret);
}
//---------------------------------------------------------------------------
void TPlanarReader::see_queue(const unsigned int s)
{
 std::list<unsigned char>::iterator itr; 
 int i;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 while (qu.size() >= 12)
 {
  if (read_queue() == 1)
  {
   qu.clear();
   sreaded (1, 0, this);
   
 // Write to port 

  } // if
  else
  {
   itr = qu.begin();
   if (itr != qu.end()) {qu.erase(itr);}
  } //else
 } //while
}
//---------------------------------------------------------------------------
void TPlanarReader::sendKey(char on)
{
 int i;
 std::list<unsigned char> wl;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 for(i = 0; i < 12; i++)
 {
  if (on == 0 && (i == 2 || i == 11))
  {
   wl.push_back(msg.data[i] + 128);
  }
  else
  {
   wl.push_back(msg.data[i]);
  }
 } 

 if (port.write_port(&wl) != 12)
 {
  log ((std::string)"Error send True Key");
 }
 else
 {log ((std::string)"write OK to serial port");}
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
TAladdinReader::TAladdinReader() : TKeyReader(), key(0) 
{
 magic_number.clear();

// magic_number.push_back(0xf0);
 magic_number.push_back(0xff); magic_number.push_back(0xff);
 magic_number.push_back(0x00); magic_number.push_back(0x00);
 magic_number.push_back(0xff); magic_number.push_back(0xff);
 magic_number.push_back(0x00); magic_number.push_back(0x00);

 for(int i = 0; i < 64; i++) magic_number.push_back(0xff);

}
//---------------------------------------------------------------------------
int64_t TAladdinReader::keyNumber(void)
{
 return(key);
}
//---------------------------------------------------------------------------
unsigned char TAladdinReader::devNumber(void)
{
 return(0);
}
//---------------------------------------------------------------------------
char TAladdinReader::start(void)
{
 char ret(0);
 port.sreaded = port_event;
 port.sreaded_param = this;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 port.open_port();
 if (port.opened != 1) {return (ret);}
 ret = port.run();
 return (ret);
}
//---------------------------------------------------------------------------
char TAladdinReader::read_queue()
{
 if (qu.size() >= 72)
 {
  int i(0);
  key = 0;
  for(std::list<unsigned char>::iterator itr = qu.begin(); itr != qu.end(), i < 72 ; ++itr, i++)
  {
   if (i >= 8)
   {
    if (*itr == 0xff) {key |= (int64_t(1) << (i - 8));}
    *itr = 0;
   }
  } 
 }
 return(key != 0 ? 1 : 0);
}
//---------------------------------------------------------------------------
void TAladdinReader::see_queue(const unsigned int s)
{
// stm << __PRETTY_FUNCTION__;
 std::list<unsigned char> reset;
 reset.clear(); reset.push_back(0xf0);

 if (s == 1)
 {
  qu.clear();
  port.write_port(&reset);
  port.set_speed(B115200);
  port.write_port(&magic_number);
 }

 else if (qu.size() == 72 && read_queue() == 1)
 {
  qu.clear();
  sreaded (1, 0, this);
  port.write_port(&reset);
  port.set_speed(B9600);
 }

}
//---------------------------------------------------------------------------
void TAladdinReader::sendKey(char v)
{
 (void) v;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void port_event(unsigned char v, unsigned int s, void * p)
{
 TKeyReader * kr;
 kr = (TKeyReader *) p;

 kr->qu.push_back(v);
 kr->see_queue(s);
}
//---------------------------------------------------------------------------
