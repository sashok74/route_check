#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <stdlib.h>
#include "parameter.h"

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
TParameterList::TParameterList()
{
 db.clear();
}
//---------------------------------------------------------------------------
void TParameterList::add(const std::string name, const char require, const char hide, 
                         const std::string v_str)
{
 std::map<std::string, TParameter>::iterator itr;
 std::string s(v_str);
 std::string::iterator i;
 TParameter p;
 char isnum(1);

 for(i = s.begin(); i != s.end(); ++i)
 {
  if (!isdigit(*i))
  {isnum = 0; break;}
 }

 p.require = require;
 p.hide    = hide;
 p.type    = 0;
 p.v_str   = v_str;
 if (isnum == 1) p.v_int = atoi(v_str.c_str()); else p.v_int = 0;

 // Проверка на уникальность ключа
 itr = db.find(name);
 if (itr != db.end()) {db.erase(itr);}

 // Добавление параметра
 db.insert(std::make_pair(name, p));
}
//---------------------------------------------------------------------------
void TParameterList::add(const std::string name, const char require, const char hide,
                         const int v_int)
{
 std::stringstream stm;
 std::map<std::string, TParameter>::iterator itr;
 TParameter p;

 stm << v_int;

 p.require = require;
 p.hide    = hide;
 p.type    = 1;
 p.v_int   = v_int;
 p.v_str   = stm.str();

 // Проверка на уникальность ключа
 itr = db.find(name);
 if (itr != db.end()) {db.erase(itr);}

 // Добавление параметра
 db.insert(std::make_pair(name, p));
}
//---------------------------------------------------------------------------
void TParameterList::set(const std::string name, const std::string str)
{
 TParameter * p = NULL;
 std::string s(str);
 std::string::iterator i;
 char isnum = 1;

 p = get(name);
 if (p != NULL)
 {
  p -> v_str = str;

  for(i = s.begin(); i != s.end(); ++i)
  {
   if (!isdigit(*i))
   {isnum = 0; break;}
  }
  if (isnum == 1) p->v_int = atoi(str.c_str()); else p->v_int = 0;  
 } // if (p != NULL)
}
//---------------------------------------------------------------------------
TParameter * TParameterList::get(const std::string name)
{
 std::map<std::string, TParameter>::iterator itr;
 itr = db.find(name);
 if (itr == db.end())
 {
  TParameter p;
  db.insert(std::make_pair(name, p));
  itr = db.find(name);
 }

 return(&(itr->second));
}
//---------------------------------------------------------------------------
bool TParameterList::exists(const std::string name)
{
 std::map<std::string, TParameter>::iterator itr;
 itr = db.find(name);
 return (itr != db.end());
}
//---------------------------------------------------------------------------
int TParameterList::init_from_enviroment(const std::string prefix)
{
 int ret(1);
 std::string p("");
 std::map<std::string, TParameter>::iterator itr;
 for (itr = db.begin(); itr != db.end(); ++itr)
 {
  p = prefix + itr->first; 
  std::transform(p.begin(), p.end(), p.begin(), toupper);    // upper
  if (::getenv(p.c_str()) != NULL)
  {
   //params.get("dbhost")->v_str     = ::getenv("RKDBHOST"); 
   set(itr->first, getenv(p.c_str())); 
  }
 }
 return(ret);
}
//---------------------------------------------------------------------------
int TParameterList::init_from_file(const std::string fname)
{
 std::ifstream f;

// db.clear();

 f.open(fname.c_str());
 if (f.fail()) {return(-1);}      // file not open

 std::string s;
 while ( getline(f, s) )
 {
  std::string p, v;
  std::string::iterator itr;

  s = s.substr(0, s.find('#'));   // удалить все что правее знака #

  if (s.empty()) {continue;}

  for(itr = s.begin(); itr != s.end(); ++itr)
  {
   if (isspace(*itr))             // удалить пробелы
   {itr = s.erase(itr); --itr;}
  }

  if (s.empty()) {continue;}

  if (s.find('=') != std::string::npos)
  {
   p = s.substr(0, s.find('='));  // параметр
   v = s.substr(s.find('=') + 1, s.length() - s.find('=') - 1); // значение

   if (p.empty() || v.empty()) {continue;} 

   add(p, 0, 0, v);
  }
 } // while

 f.close();
 return(1);
}
//---------------------------------------------------------------------------
int TParameterList::init_from_arg(int argc, char *argv[])
{
 int ret(-1);

 for(int i = 1; i < argc; i++)
 {
  std::string p(argv[i]);

  if (p.substr(0,2) == std::string("--"))
    {p = p.substr(2);}
  else {continue;}

  if (exists(p))
  {
    i++; if (i >= argc) break;
    get(p)->v_str = argv[i]; continue;
  } // if (params.exists(p))

 } // for(int i = 1; i < argc; i++)

 return(ret);
}
//---------------------------------------------------------------------------
std::string TParameterList::print(void)
{
 std::stringstream stm("");
 std::map<std::string, TParameter>::iterator itr;

 for (itr = db.begin(); itr != db.end(); ++itr)
 {
  stm << itr->first << " = ";
  if (itr->second.hide != 1) stm << itr->second.v_str;
  else stm << "*****";
  stm << std::endl;
 } // for (itr = db.begin(); itr != db.end(); ++itr)
 return(stm.str());
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
