#include <iostream>
#include <string.h>

#include <gtk/gtk.h>
#include <gtk/gtktree.h>
#include <gdk/gdkkeysyms.h>

#include <glib-object.h>

#include <gtkextra/gtksheet.h>

//#include <gtk/gtktreeprivate.h>

//#include "gtktreeview210.h"
#include "ibppquery.h"
#include "gtkcellrendererxtext.h"
#include "uMainfrm.h"
#include "uDataLayer.h"
#include "parameter.h"
#include "service.h"
#include "log.h"
//G_LOCK_DEFINE_STATIC (lock_mainfrm);
//static volatile int lock_mainfrm = 1;

extern TLog * l;

static gboolean gtk_tree_view_expose (GtkWidget *widget, GdkEventExpose *event, gpointer user_data);

//---------------------------------------------------------------------------
Tmainfrm * MainfrmShow(TRefresh2 refresh, int t, IBPP::Database db)
{
 extern TParameterList params;
 Tmainfrm * win = new Tmainfrm(refresh, t, db);

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 win->build();
 win->show();
 win->set_page(0);

 win->kr->sreaded = MainfrmKeys;
 win->kr->port.port = params.get("port")->v_str;
 if (win->kr->start() != 1)
 {
  log((std::string)"Error: Port " + win->kr->port.port + (std::string)" not open !");
 }

 return (win);
}
//--------------------------------------------------------
void MainfrmKeys(unsigned char v, unsigned int s, void * p)
{
// std::stringstream stm;
 (void *)s;
 TKeyReader * kr; Tmainfrm * self;
 kr   = (TKeyReader *) p;
 self = (Tmainfrm *) kr->owner;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (self->get_page() != 1 || self->key_required != 1)
 {
  kr->sendKey(1);
  log("Key is not required.");
  return;
 }

// self->key_required = 0;

 if (self->OnEmployeeRegister(kr->keyNumber(), kr->devNumber()))
 {
  kr->sendKey(1);
  log((std::string)"Key found: -" + self->Empl.emp_name + (std::string)"-");
 l->add(LOG_SHOW_DEBUG, (std::string)"before mainfrm_keys_idle_func adding");
 g_idle_add(mainfrm_keys_idle_func, (gpointer)self);
////  gdk_threads_enter();
////  self->next_page();
////  gdk_flush ();
////  gdk_threads_leave();
 }
 else
 {
  std::stringstream stm;
  kr->sendKey(0);
  stm << "Key not found: " << std::hex << kr->keyNumber();
  log (stm.str());
 }
}
//---------------------------------------------------------------------------
static void mainfrm_selection_changed(GtkTreeSelection *widget, gpointer data)
{
 GtkTreeIter iter;
 GtkTreeModel * model;
 gint pack_route_id(0);
 Tmainfrmptr self = NULL;
 self = (Tmainfrmptr)data;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (gtk_tree_selection_get_selected (widget, &model, &iter))
 {
  gtk_tree_model_get (model, &iter, 7, &pack_route_id, -1);
 }

 if (pack_route_id > 0)
 {
  self->set_pri(pack_route_id);
 }

}
//---------------------------------------------------------------------------
static gboolean mainfrm_delete_event(GtkWidget *widget, GdkEvent  *event, gpointer data)
{
  GtkWidget * dialog;
  Tmainfrmptr self = NULL;
  self = (Tmainfrmptr)data;

  gint ret;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

  dialog = gtk_message_dialog_new (GTK_WINDOW(self->window),
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_QUESTION,
                                   GTK_BUTTONS_YES_NO,
                                   "Закрыть программу ?");
  ret = gtk_dialog_run (GTK_DIALOG (dialog));
 
  gtk_widget_destroy (dialog);

  if (ret != -8) log((std::string)"Close program dialog: no");
  else           log((std::string)"Close program dialog: yes");

  return (ret != -8);
}
//---------------------------------------------------------------------------
static void mainfrm_enumber_activate_event(GtkWidget *widget, gpointer data)
{
  GtkWidget * dialog;
  int page;
  Tmainfrmptr self = NULL;
  self = (Tmainfrmptr)data;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

  if (self != NULL)
  {
   page = atoi(gtk_entry_get_text(GTK_ENTRY(widget)));
   if (page == 0) return;
   if (self->read_route_list(page) > 0)
   {
    std::stringstream stm; stm << "read route " << page;
    log(stm.str());
    gtk_label_set_markup (GTK_LABEL (self->label3), "<span size=\"xx-large\">Выберите операцию, приложите ключ.</span>");
    self->key_required = 1;
    self->next_page();
   }
   else
   {
    GtkWidget * dialog;
    gint ret;
    std::stringstream stm; stm << "read not existing route " << page;
    log(stm.str());
    dialog = gtk_message_dialog_new (GTK_WINDOW(self->window),
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_ERROR,
                                   GTK_BUTTONS_OK,
                                   "Маршрута %d не существует !\nДля продолжения работы нажмите ОК.", page);
    ret = gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
   }
  }
}
//---------------------------------------------------------------------------
static void mainfrm_snumber_activate_event(GtkWidget *widget, gpointer data)
{
  Tmainfrmptr self = NULL;
  self = (Tmainfrmptr)data;
  l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
  if (self != NULL)
  {
   self->snumber_activate_event();
   int snumber = atoi(gtk_entry_get_text(GTK_ENTRY(widget)));
   gtk_window_set_focus(GTK_WINDOW(self->window), self->snumber);
  }

}
//---------------------------------------------------------------------------
static void mainfrm_p5_gr_defect_cell_edited(GtkCellRendererText *cell, const gchar *path_string,
                                             const gchar *new_text, gpointer data)
{
 Tmainfrm * frm = (Tmainfrm *) data;
 frm->gr_defect_cell_edited(cell, path_string, new_text);
}
//---------------------------------------------------------------------------
static void mainfrm_p5_cb_def_type_changed(GtkComboBox *widget, gpointer data)
{
 Tmainfrm * frm = (Tmainfrm *) data;
 frm->p5_cb_def_type_changed();
}
//---------------------------------------------------------------------------
//static gboolean mainfrm_p5_gr_selection_func(GtkTreeSelection *selection, GtkTreeModel *model, GtkTreePath *path,
//                                             gboolean path_currently_selected, gpointer data)
//{
// Tmainfrm * frm = (Tmainfrm *) data;
// return(frm->p5_gr_selection_func(selection, model, path, path_currently_selected));
//}
//---------------------------------------------------------------------------
static void mainfrm_p5_gr_selection_changed(GtkTreeSelection * selection, gpointer data)
{
 Tmainfrm * frm = (Tmainfrm *) data;
 frm->p5_gr_selection_changed(selection);
}
//---------------------------------------------------------------------------
static gboolean mainfrm_p5_sheet_traverse(GtkSheet *sheet, gint row, gint column,
                                          gpointer *new_row, gpointer *new_column, gpointer data)
{
 Tmainfrm * frm = (Tmainfrm *) data;
 return(frm->p5_sheet_traverse(row, column, new_row, new_column));
}
//---------------------------------------------------------------------------
static void mainfrm_p5_sheet_changed(GtkSheet *sheet, gint row, gint column, gpointer data)
{
 Tmainfrm * frm = (Tmainfrm *) data;
 frm->p5_sheet_changed(row, column);
}
//---------------------------------------------------------------------------
static void mainfrm_p5_sheet_clear_cell(GtkSheet *sheet, gint row, gint column, gpointer data)
{
 Tmainfrm * frm = (Tmainfrm *) data;
 frm->p5_sheet_clear_cell(row, column);
}
//---------------------------------------------------------------------------
static gboolean mainfrm_enumber_key_release_event(GtkWidget *widget, GdkEventKey *event, gpointer data)
{
  Tmainfrmptr self = NULL;
  self = (Tmainfrmptr)data;
//  guint k = event->keyval;

  l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
//  l->add(LOG_SHOW_TRACE, (std::string)gtk_entry_get_text(GTK_ENTRY(widget)));

  self->enumber_check();

//  if ((k >= GDK_0 && k <= GDK_9) || (k >= GDK_KP_0 && k <= GDK_KP_9))
//  {
   return(FALSE);
//  }
//  else
//  {
//   return(TRUE);
//  }
}
//---------------------------------------------------------------------------
static void mainfrm_eamount_event(GtkWidget *widget, gpointer data)
{
  GtkWidget * dialog;
  Tmainfrmptr self = NULL;
  self = (Tmainfrmptr)data;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

  if (self != NULL)
  {
   if (self->save())
   {
     log((std::string)"save OK");
     self->read_route_list(0);
     gtk_label_set_markup (GTK_LABEL (self->label3), "<span size=\"xx-large\">Операция отмечена. Для продолжения нажмите ESC.</span>");
     self->key_required = 0;
     self->set_page(1);
     //self->timer1.start();
     self->timer1_started = true;
     g_timeout_add(30 * 1000, mainfrm_timer_idle_func, data);
   }
   else
   {
     GtkWidget * dialog;
     log((std::string)"Error: Save is failed.");
     std::stringstream stm("");
     stm <<  "Ошибка записи !" << std::endl << self->save_error_msg;
     dialog = gtk_message_dialog_new (GTK_WINDOW(self->window),
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_ERROR,
                                   GTK_BUTTONS_OK,
                                   stm.str().c_str());
     gtk_dialog_run (GTK_DIALOG (dialog));
     gtk_widget_destroy (dialog);
   }
  }
}
//---------------------------------------------------------------------------
static void mainfrm_destroy(GtkWidget *widget, gpointer data)
{
 Tmainfrmptr self = NULL;
 self = (Tmainfrmptr)data;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
 l->add(LOG_SHOW_INFO, (std::string)"Main window is destroying.");

 delete(self);

// gtk_main_quit ();
}
//---------------------------------------------------------------------------
/*static void mainfrm_notebook_switch_page_event(GtkNotebook *notebook,
                                               GtkNotebookPage *page,
                                               guint          page_num,
                                               gpointer       data)
{
 Tmainfrmptr self = NULL;
 self = (Tmainfrmptr)data;

//log("switch page %d\n", page_num);

 if (self->visible)
 {
  self->current_page = page_num;
  if (page_num == 0)
  {gtk_window_set_focus(GTK_WINDOW(self->window), self->enumber);}
  if (page_num == 1)
  {gtk_window_set_focus(GTK_WINDOW(self->window), self->tree);}
  if (page_num == 2)
  {gtk_window_set_focus(GTK_WINDOW(self->window), self->eamount);}
 }

}*/
//---------------------------------------------------------------------------
static void mainfrm_GtkTreeCellDataFunc (GtkTreeViewColumn *tree_column,
                                  GtkCellRenderer *cell,
                                  GtkTreeModel *model,
                                  GtkTreeIter *iter,
                                  gpointer data)
{
 Tmainfrmptr self = NULL;
 self = (Tmainfrmptr)data;
 gint pack_route_id;
 static  TPackRtItem p(NULL);

// l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 gtk_tree_model_get (model, iter, 7, &pack_route_id, -1);

// TODO: поиск по std::list заменить спец полем в GtkList
 if (p.getData()->pack_route_id != pack_route_id)
   self->get_pri(pack_route_id, &p);

 if (p.getData()->cnt_in > 0 && 
     p.getData()->cnt_out > 0 &&
     (p.getData()->date_e.Hours() + p.getData()->date_e.Minutes() + p.getData()->date_e.Seconds() +
      p.getData()->date_e.SubSeconds()) >0 )
 {
  g_object_set (G_OBJECT (cell), "foreground", "grey", NULL);
 }
 else
 {
  g_object_set (G_OBJECT (cell), "foreground", "black", NULL);
  //if (gtk_tree_view_get_column(GTK_TREE_VIEW(self->tree), 2) == tree_column)
  //{
  // g_object_set (G_OBJECT (cell), "background", "yellow", NULL);
  //}
 }

}
//---------------------------------------------------------------------------
static gboolean mainfrm_key_press_event(GtkWidget   *widget,
                                        GdkEventKey *event,
                                        gpointer     data)
{
 Tmainfrmptr self = NULL;
 self = (Tmainfrmptr)data;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (event->type == GDK_KEY_PRESS && event->keyval == GDK_Escape)
 {
  if (self->get_page() == 3 || self->get_page() == 4) self->set_page(1);
  else                       self->prev_page();
 }
 if (event->type == GDK_KEY_PRESS && event->keyval == GDK_F12)
 {
  if (self->get_page() == 3) self->set_page(1);
  else
  if (self->get_page() == 1) self->set_page(3);
 }
 if (event->type == GDK_KEY_PRESS && event->keyval == GDK_F11)
 {
  if (self->get_page() == 4) self->set_page(1);
  else
  if (self->get_page() == 1) self->set_page(4);
 }
 return (FALSE);
}
//---------------------------------------------------------------------------
Tmainfrm::Tmainfrm(TRefresh2 p_refresh, int p_t, IBPP::Database p_db):
 label_num    (0),
 current_page (0),
 visible      (0),// timer1       (30, 0) //, enumber_timer       (5, 0)
 save_error_msg(""), changed_row(-1), changed_column(-1),
 timeout_source(0)
{
 extern TParameterList params;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 prefresh            = p_refresh;
 p_type              = p_t;
// db       = p_db;
 dl                  = new TDataLayer(p_db);
// kr->owner            = this;
 if (params.get("key_reader")->v_str == std::string("planar"))
 {
  kr = new TPlanarReader();
 }
 else
 {
  kr = new TAladdinReader();
 }
 kr->owner           = this;
 pri                 = new TPackRtItem(dl);
 //timer1.user_data    = this;
 //timer1.parent_event =  mainfrm_timer;
 //enumber_timer.user_data    = this;
 //enumber_timer.parent_event =  enumber_clear_timer;

 p5_mt = new TDynamicList();
}
//---------------------------------------------------------------------------
void Tmainfrm::build(void)
{
 extern const guint8 *pkey_16;
 extern const guint8 *plinux_logo_64;
 extern TParameterList params;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL); gtk_widget_set_name(window, "route_check_main_window");
    gtk_window_set_title(GTK_WINDOW(window),"Ключ");

    {
     int width(params.get("main_window_width")->v_int), height(params.get("main_window_height")->v_int);
     if (width > 0 && height > 0)
     {gtk_window_set_default_size(GTK_WINDOW(window), width, height);}
    }
    gtk_window_set_position(GTK_WINDOW(window),GTK_WIN_POS_CENTER);

    icon = gdk_pixbuf_new_from_inline (-1, pkey_16, FALSE, NULL);
    if (icon != NULL)
    {
     // TODO: replace to icon list
     gtk_window_set_icon(GTK_WINDOW(window), icon);
    }
 
    g_signal_connect (G_OBJECT (window), "delete_event",
		      G_CALLBACK (mainfrm_delete_event), (gpointer)this);
    
    g_signal_connect (G_OBJECT (window), "destroy",
		      G_CALLBACK (mainfrm_destroy), (gpointer)this);
    
    /* Sets the border width of the window. */
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);

    notebook = gtk_notebook_new(); gtk_widget_set_name(notebook, "route_check_main_window_notebook");

    gtk_container_add (GTK_CONTAINER (window), notebook);

    g_signal_connect (G_OBJECT (notebook), "key-press-event",
                      G_CALLBACK (mainfrm_key_press_event), (gpointer)this);

    // page 1
    p1_vbox  = gtk_vbox_new (TRUE, 3);
    p1_label = gtk_label_new_with_mnemonic("Номер маршрута");
    label1 = gtk_label_new_with_mnemonic("ВВЕДИТЕ НОМЕР МАРШРУТА, НАЖМИТЕ ENTER"); gtk_widget_set_name(label1, "route_check_main_window_notebook_p1_label1"); 
    if (params.get("gtkrc")->v_str.empty())
    {
     gtk_label_set_markup (GTK_LABEL (label1), "<span size=\"xx-large\">ВВЕДИТЕ НОМЕР МАРШРУТА, НАЖМИТЕ ENTER</span>");
    }
    //gtk_label_set_line_wrap(GTK_LABEL(label1), TRUE);
    gtk_box_pack_start(GTK_BOX(p1_vbox), label1, FALSE, FALSE, 2);
    enumber = gtk_entry_new(); gtk_widget_set_name(enumber, "route_check_main_window_notebook_p1_enumber");

    if (params.get("gtkrc")->v_str.empty())
    {
     PangoFontDescription *font = pango_font_description_from_string("Sans Serif 56");
     gtk_widget_modify_font(enumber, font);
     pango_font_description_free(font);
    }

    gtk_box_pack_start(GTK_BOX(p1_vbox), enumber, FALSE, FALSE, 2);

    g_signal_connect (G_OBJECT (enumber), "activate",
                      G_CALLBACK (mainfrm_enumber_activate_event), (gpointer)this);
    g_signal_connect (G_OBJECT (enumber), "key-release-event",
                      G_CALLBACK (mainfrm_enumber_key_release_event),(gpointer)this);

    /*pic_linux_logo = gdk_pixbuf_new_from_inline (-1, plinux_logo_64, FALSE, NULL);
    if (pic_linux_logo != NULL)
    {
     img_linux_logo = gtk_image_new_from_pixbuf(pic_linux_logo);
     GtkWidget * imghbox = gtk_hbox_new (FALSE, 2);
     GtkWidget * imgvbox = gtk_hbox_new (FALSE, 2);
     gtk_box_pack_end(GTK_BOX(imghbox), img_linux_logo, FALSE, FALSE, 0);
     gtk_box_pack_end(GTK_BOX(imgvbox), imghbox,        FALSE, FALSE, 0);
     gtk_box_pack_start(GTK_BOX(p1_vbox), imgvbox,      FALSE, FALSE, 2);
    }*/

    // page 3
    p3_vbox  = gtk_vbox_new (TRUE, 2);
    p3_label = gtk_label_new_with_mnemonic("Количество");
    label4 = gtk_label_new_with_mnemonic("ПРОВЕРЬТЕ КОЛИЧЕСТВО, НАЖМИТЕ ENTER");
    gtk_label_set_markup (GTK_LABEL (label4), "<span size=\"xx-large\">ПРОВЕРЬТЕ КОЛИЧЕСТВО, НАЖМИТЕ ENTER</span>");
    gtk_label_set_line_wrap(GTK_LABEL(label4), TRUE);
    gtk_box_pack_start(GTK_BOX(p3_vbox), label4, FALSE, FALSE, 2);
    eamount = gtk_entry_new();
    {
     PangoFontDescription *font = pango_font_description_from_string("Sans Serif 56");
     gtk_widget_modify_font(eamount, font);
     pango_font_description_free(font);
    }


    gtk_box_pack_start(GTK_BOX(p3_vbox), eamount, FALSE, FALSE, 2);

    g_signal_connect (G_OBJECT (eamount), "activate",
                      G_CALLBACK (mainfrm_eamount_event), (gpointer)this);

    // page 4
    p4_vbox = gtk_vbox_new (FALSE, 2);
    {
     GtkWidget * p4_hbox_1, * label, * scrolledwindow;
     p4_label = gtk_label_new_with_mnemonic("Серийный номер");
     p4_hbox_1 = gtk_hbox_new (FALSE, 2);
     label     = gtk_label_new_with_mnemonic("Серийный номер:");
     snumber   = gtk_entry_new(); gtk_widget_set_name(snumber, "route_check_main_window_notebook_p4_snumber");
     p4_scount_label = gtk_label_new_with_mnemonic("0");
     p4_text_view    = gtk_text_view_new();
     scrolledwindow  = gtk_scrolled_window_new(NULL, NULL);
     gtk_text_view_set_editable(GTK_TEXT_VIEW(p4_text_view), FALSE);
     gtk_container_add(GTK_CONTAINER(scrolledwindow), p4_text_view);
     gtk_box_pack_start(GTK_BOX(p4_hbox_1), label, FALSE, FALSE, 2);
     gtk_box_pack_start(GTK_BOX(p4_hbox_1), snumber, FALSE, FALSE, 2);
     gtk_box_pack_start(GTK_BOX(p4_hbox_1), gtk_vseparator_new(), TRUE, TRUE, 2);
     gtk_box_pack_start(GTK_BOX(p4_hbox_1), p4_scount_label, FALSE, FALSE, 2);
     gtk_box_pack_start(GTK_BOX(p4_vbox), p4_hbox_1, FALSE, FALSE, 2);
     gtk_box_pack_start(GTK_BOX(p4_vbox), scrolledwindow, TRUE, TRUE, 2);
     //gtk_box_pack_start(GTK_BOX(p4_vbox), p4_text_view, TRUE, TRUE, 2);
     gtk_box_pack_start(GTK_BOX(p4_vbox), gtk_label_new_with_mnemonic("F12 - выход"), FALSE, FALSE, 2);
     g_signal_connect (G_OBJECT (snumber), "activate",
                       G_CALLBACK (mainfrm_snumber_activate_event), (gpointer)this);
//     g_signal_connect (G_OBJECT (enumber), "key-release-event",
//                       G_CALLBACK (mainfrm_enumber_key_release_event),(gpointer)this);
//     g_signal_connect (G_OBJECT (snumber), "key-press-event",
//                       G_CALLBACK (mainfrm_key_press_event), (gpointer)this);
    }
    // page 5
    {
     p5_vbox = gtk_vbox_new (FALSE, 2); gtk_widget_set_name(p5_vbox, "p5_vbox");
     p5_label = gtk_label_new_with_mnemonic("Дефект");
     p5_cb_def_type = gtk_combo_box_new_text(); gtk_widget_set_name(p5_cb_def_type, "p5_cb_def_type");
     gtk_combo_box_append_text(GTK_COMBO_BOX(p5_cb_def_type), "SMD");
     gtk_combo_box_append_text(GTK_COMBO_BOX(p5_cb_def_type), "Механика");
//     p5_refresh_cb_def_type();
     gtk_combo_box_set_active(GTK_COMBO_BOX(p5_cb_def_type), 0);
     g_signal_connect (G_OBJECT (p5_cb_def_type), "changed",
                       G_CALLBACK (mainfrm_p5_cb_def_type_changed), (gpointer)this);

     GtkWidget * hbox = gtk_hbox_new (FALSE, 0);
     gtk_box_pack_start(GTK_BOX(p5_vbox), hbox, FALSE, FALSE, 0);
     gtk_box_pack_start(GTK_BOX(hbox), p5_cb_def_type, FALSE, FALSE, 2);

     p5_sw = gtk_scrolled_window_new (NULL, NULL);  gtk_widget_set_name(p5_sw, "p5_sw");
     gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (p5_sw),
                                          GTK_SHADOW_ETCHED_IN);
     gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (p5_sw),
                                     GTK_POLICY_AUTOMATIC,
                                     GTK_POLICY_AUTOMATIC);
     gtk_box_pack_start(GTK_BOX(p5_vbox), p5_sw,   TRUE, TRUE, 2);

// List
     p5_sheet_defect = gtk_sheet_new(5, 2, "Дефекты");
     //GTK_SHEET_SET_FLAGS(p5_sheet_defect, GTK_SHEET_AUTORESIZE);
     gtk_sheet_hide_row_titles(GTK_SHEET(p5_sheet_defect));
     gtk_sheet_set_selection_mode(GTK_SHEET(p5_sheet_defect), GTK_SELECTION_SINGLE);
//     gtk_sheet_set_autoresize_rows(GTK_SHEET(p5_sheet_defect), TRUE);
     gtk_sheet_rows_set_resizable(GTK_SHEET(p5_sheet_defect), TRUE);
     gtk_sheet_column_button_add_label(GTK_SHEET(p5_sheet_defect), 0, "Дефект");
     gtk_sheet_column_button_add_label(GTK_SHEET(p5_sheet_defect), 1, "Количество");
//     gtk_sheet_columns_labels_set_visibility(GTK_SHEET(p5_sheet_defect), TRUE);
#if GTKEXTRA_MAJOR_VERSION > 2
     gtk_sheet_column_set_readonly(GTK_SHEET(p5_sheet_defect), 0, TRUE);
#endif
     if (params.get("p5_columnwidth_1")->v_int > 0)
     { p5_sheet_column0_width = params.get("p5_columnwidth_1")->v_int;}
     else
      { p5_sheet_column0_width = 400;}
     gtk_sheet_set_column_width(GTK_SHEET(p5_sheet_defect), 0, p5_sheet_column0_width);

     gtk_sheet_column_set_sensitivity(GTK_SHEET(p5_sheet_defect), 0, FALSE);
     gtk_sheet_column_set_sensitivity(GTK_SHEET(p5_sheet_defect), 1, TRUE);

     g_signal_connect(G_OBJECT(p5_sheet_defect), "changed",
                      G_CALLBACK(mainfrm_p5_sheet_changed), (gpointer)this);
     g_signal_connect(G_OBJECT(p5_sheet_defect), "clear-cell",
                      G_CALLBACK(mainfrm_p5_sheet_clear_cell), (gpointer)this);
     g_signal_connect(G_OBJECT(p5_sheet_defect), "traverse",
                      G_CALLBACK(mainfrm_p5_sheet_traverse), (gpointer)this);

     gtk_container_add(GTK_CONTAINER (p5_sw), p5_sheet_defect);
/*     p5_gr_defect  = gtk_tree_view_new();
     gtk_tree_selection_set_mode(gtk_tree_view_get_selection(GTK_TREE_VIEW(p5_gr_defect)), GTK_SELECTION_SINGLE);
//     gtk_tree_selection_set_select_function(gtk_tree_view_get_selection(GTK_TREE_VIEW(p5_gr_defect)),
//                                            mainfrm_p5_gr_selection_func, this, NULL);
     g_signal_connect (G_OBJECT (gtk_tree_view_get_selection(GTK_TREE_VIEW(p5_gr_defect))), "changed",
                       G_CALLBACK (mainfrm_p5_gr_selection_changed), (gpointer)this);
     gtk_container_add (GTK_CONTAINER (p5_sw), p5_gr_defect);
*/
     gtk_box_pack_start(GTK_BOX(p5_vbox), gtk_label_new_with_mnemonic("F11 - выход"), FALSE, FALSE, 2);
    }
 
    // page 2
//    p2_alignment = gtk_alignment_new(0,0,1,1);
    p2_vbox  = gtk_vbox_new (FALSE, 2);
    p2_label = gtk_label_new_with_mnemonic("Маршрут");
    label2 = gtk_label_new_with_mnemonic("МАРШРУТ");
    gtk_label_set_markup (GTK_LABEL (label2), "<span size=\"xx-large\">Маршрут</span>");
    gtk_label_set_line_wrap(GTK_LABEL(label2), TRUE);
    label3 = gtk_label_new_with_mnemonic("<span size=\"xx-large\">Выберите операцию, приложите ключ.</span>");
   // gtk_label_set_line_wrap(GTK_LABEL(label3), TRUE);
   // gtk_label_set_markup (GTK_LABEL (label3), "<span size=\"xx-large\">Выберите операцию, приложите ключ.</span>");

    sw = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
                                         GTK_SHADOW_ETCHED_IN);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
                                    GTK_POLICY_AUTOMATIC,
                                    GTK_POLICY_AUTOMATIC);

// List
    store = gtk_list_store_new (8, G_TYPE_UINT,
                                   G_TYPE_STRING, 
                                   //G_TYPE_FLOAT, G_TYPE_FLOAT,
                                   G_TYPE_STRING, G_TYPE_STRING,
                                   G_TYPE_STRING, G_TYPE_STRING,
                                   G_TYPE_STRING,
                                   G_TYPE_UINT );

    tree  = gtk_tree_view_new_with_model (GTK_TREE_MODEL(store));

//     g_signal_connect_after (G_OBJECT (tree), "expose-event",
//                      G_CALLBACK (gtk_tree_view_expose), (gpointer)this);
//     g_signal_connect (G_OBJECT (tree), "expose-event",
//                      G_CALLBACK (gtk_tree_view_expose), (gpointer)this);

  //  gtk_tree_view_set_fixed_height_mode (GTK_TREE_VIEW (tree), FALSE);
//#if (GTK_MAJOR_VERSION > 2 || (GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION >= 10))
//    gtk_tree_view_set_grid_lines(tree, GTK_TREE_VIEW_GRID_LINES_BOTH); // сетка в гриде
//#endif

    select = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
//    gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
//    gtk_tree_selection_set_mode (select, GTK_SELECTION_BROWSE);
    g_signal_connect (G_OBJECT (select), "changed",
                      G_CALLBACK (mainfrm_selection_changed), (gpointer)this);
    {
     //GtkWidget * tree_label;
     extern TParameterList params;
     int fixed_height = 2, number_of_line = 1;
     std::string column_name[VISIBLE_COLUMN_COUNT] = {"№", "Наименование операции", "Выдано", "Сдано", "Начало",
                                                      "Конец", "Сотрудник"};
     //tree_label = gtk_label_new_with_mnemonic("Наименование операции");
     //gtk_label_set_line_wrap(GTK_LABEL(tree_label), TRUE);
     //gtk_label_set_line_wrap_mode(GTK_LABEL(tree_label),PANGO_WRAP_CHAR);
     //gtk_widget_show(tree_label);
     for (int i = 0; i < VISIBLE_COLUMN_COUNT; i++)
     {
      GtkCellRenderer *renderer; GtkTreeViewColumn *column;
      std::stringstream stm;

      stm << "columnwidth_" << i + 1;

      renderer = gtk_cell_renderer_xtext_new ();
     //renderer = gtk_cell_renderer_pixbuf_new ();
      g_object_set (G_OBJECT (renderer), "yalign", 1, NULL);
//     g_object_set (G_OBJECT (renderer), "wrap-mode", PANGO_WRAP_CHAR, "wrap-width", 80,  NULL);
      g_object_set (G_OBJECT (renderer), "hor_line", true, "ver_line", true, "number_of_line", number_of_line, NULL);

      gtk_cell_renderer_text_set_fixed_height_from_font(GTK_CELL_RENDERER_TEXT(renderer), fixed_height);

      column = gtk_tree_view_column_new_with_attributes (column_name[i].c_str(), renderer, "text", i, NULL);
//     gtk_tree_view_column_set_sort_column_id (column, 0);
      if (params.get(stm.str())->v_int > 0)
      {
       gtk_tree_view_column_set_sizing(GTK_TREE_VIEW_COLUMN(column), GTK_TREE_VIEW_COLUMN_FIXED);
       gtk_tree_view_column_set_fixed_width(GTK_TREE_VIEW_COLUMN(column), params.get(stm.str())->v_int);
      }
      gtk_tree_view_column_set_resizable(GTK_TREE_VIEW_COLUMN(column), TRUE);
    // gtk_tree_view_column_set_spacing(column, 1);
      gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);
      gtk_tree_view_column_set_cell_data_func(GTK_TREE_VIEW_COLUMN(column), GTK_CELL_RENDERER(renderer),
                                              mainfrm_GtkTreeCellDataFunc, gpointer(this), NULL);
     } // for (int i = 0; i < VISIBLE_COLUMN_COUNT; i++)
    } //

//    gtk_container_add (GTK_CONTAINER (p2_alignment), p2_vbox);
    gtk_container_add (GTK_CONTAINER (sw), tree);

    gtk_box_pack_start(GTK_BOX(p2_vbox), label2, FALSE, FALSE, 2);
    gtk_box_pack_start(GTK_BOX(p2_vbox), sw,     TRUE, TRUE, 2);
    gtk_box_pack_start(GTK_BOX(p2_vbox), label3, FALSE, FALSE, 2);
    gtk_box_pack_start(GTK_BOX(p2_vbox), gtk_label_new_with_mnemonic("ESC - выход, F11 - дефекты, F12 - серийные номера"),
                        FALSE, FALSE, 2);

    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), p1_vbox, p1_label);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), p2_vbox, p2_label);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), p3_vbox, p3_label);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), p4_vbox, p4_label);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), p5_vbox, p5_label);

    gtk_notebook_set_show_tabs(GTK_NOTEBOOK(notebook), FALSE);
}
//---------------------------------------------------------------------------
Tmainfrm::~Tmainfrm(void)
{
 int ret(0);

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (timeout_source > 0)
 {
  g_source_remove(timeout_source);
  timeout_source = 0;
 }

 delete(p5_mt);

 if (kr != NULL)
 {
  delete(kr);
 }

 if (pri != NULL)
 {
  delete(pri);
 }

 if (dl != NULL) 
 {
  delete(dl);
 }

 if (prefresh != NULL)
 {
  prefresh(p_type, ret);
 }

}
//---------------------------------------------------------------------------
void Tmainfrm::show(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 extern TParameterList params;
 std::string state(params.get("main_window_state")->v_str);

 if (state != "natural")
 {
  if (state == "maximize")  {gtk_window_maximize(GTK_WINDOW(window));}
  else
  if (state == "iconify")   {gtk_window_iconify(GTK_WINDOW(window));}
  else
  if (state == "fullscreen"){gtk_window_fullscreen(GTK_WINDOW(window));}
 } 

 gtk_widget_show_all (window);
 visible = 1;
}
//---------------------------------------------------------------------------
int Tmainfrm::read_route_list(int num)
{
 int n = 0, row = 0, i = 0, es = 0;
 GtkTreeIter iter;
 list<RPackRtItem>::iterator itr;

 char t_cnt_in[20], t_cnt_out[20];
 std::string t_date_b, t_date_e;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (num > 0)
 { 
  label_num = num;
 }

 gtk_list_store_clear(store);

 if (dl != NULL)
 {
  n = dl->RtRouteListSMem(&route_list, label_num);
 }

 if (n > 0)
 {
  for(itr = route_list.begin(); itr != route_list.end(); ++itr)
  {
   if (itr->cnt_in != 0)
    {sprintf(t_cnt_in, "%.3f", itr->cnt_in);}
   else
    {sprintf(t_cnt_in, "");}
   if (itr->cnt_out != 0)
    {sprintf(t_cnt_out, "%.3f", itr->cnt_out);}
   else
    {sprintf(t_cnt_out, "");}
   if (!(itr->date_b.Year() == 1970 && itr->date_b.Month() == 1   && 
         itr->date_b.Day() == 1     && itr->date_b.Hours() == 0   && 
         itr->date_b.Minutes() == 0 && itr->date_b.Seconds() == 0 && 
         itr->date_b.SubSeconds() == 0))
    {t_date_b = tst2str(&itr->date_b);}
   else {t_date_b = "";}
   if (!(itr->date_e.Year() == 1970 && itr->date_e.Month() == 1   &&
         itr->date_e.Day() == 1     && itr->date_e.Hours() == 0   &&
         itr->date_e.Minutes() == 0 && itr->date_e.Seconds() == 0 &&
         itr->date_e.SubSeconds() == 0))
    {t_date_e = tst2str(&itr->date_e);}
   else {t_date_e = "";}

   gtk_list_store_append(GTK_LIST_STORE(store), &iter);
   gtk_list_store_set(GTK_LIST_STORE(store), &iter,
                      0, itr->oper_num,
                      1, itr->descript.c_str(),
                      2, t_cnt_in,
                      3, t_cnt_out,
                      4, t_date_b.c_str(),
                      5, t_date_e.c_str(),
                      6, itr->employee_name.c_str(),
                      7, itr->pack_route_id,
                     -1);
   //int t = itr->oper_num;
   //GValue * gt = &t;
   //gtk_list_store_set_value(store, &iter, 1, G_VALUE_TYPE(t));
   if (!(itr->cnt_in != 0 && itr->cnt_out != 0) && row == 0)
   {
    row = i + 1;
   }
   if ( itr->cnt_in == 0 && itr->cnt_out == 0) es++;

   i++;
  } // for(itr = route_list.begin(); itr != route_list.end(); ++itr)
 } // if (n > 0)

 if (row == 0 && n != 0) {row = n;}     // все строки заполнены, встаем на последнюю
 if (es == n) {row = 0;}                // все строки пустые, встаем на первую

 row--;
 if (row > 0) select_row(row);
 else select_row(0);
 
// dl->NomPackRouteLabelSMem(&label_list, label_num);
 if (n > 0)
 {
  dl->NomPackRouteLabelSMem(&mt_ll, label_num); 
  mt_ll.first();
 }
 else
 {mt_ll.clear();}

 return (n);
}
//---------------------------------------------------------------------------
bool Tmainfrm::OnEmployeeRegister (int64_t sn, int num)
{
 float count = 0;
 char c_str[20];
 bool first = false;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 Empl.SetDefault();
 dl->EmpEmployeesInfBySnS(sn, Empl);
 if (Empl.id == 0)
 {
   Empl.SetDefault();
   return false;
 }

// if (route_list.begin()->pack_route_id == pri->getData()->pack_route_id)
// {
//  first = true;
// }

// if (pri->getData()->cnt_in > 0)
//  {count = pri->getData()->cnt_in;}
// else if (!first)
// {
//   count = get_prev_cnt_out();
// }
// else
// {
//   count = mt_ll.field_by_name("cnt")->get_float();
// }

// sprintf(c_str, "%.0f", count);
// gtk_entry_set_text(GTK_ENTRY(eamount), c_str);
 
 return (true);
}
//---------------------------------------------------------------------------
bool Tmainfrm::save(void)
{
 bool ret(false), TimeSet(false);
 float cnt_new(0);
 int time_b, time_e;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 save_error_msg = "";

 cnt_new = atof(gtk_entry_get_text(GTK_ENTRY(eamount)));

 if (cnt_new <= 0)
 {
  std::stringstream stm("Error: Tmainfrm::save() cnt_new = ");
  stm << cnt_new;
  log(stm.str());
 
  save_error_msg = "Количество должно быть больше 0.";

/*     GtkWidget * dialog;
     dialog = gtk_message_dialog_new (GTK_WINDOW(window),
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_ERROR,
                                   GTK_BUTTONS_OK,
                                   "Ошибка !\nКоличество должно быть больше 0.");
     gtk_dialog_run (GTK_DIALOG (dialog));
     gtk_widget_destroy (dialog);
*/

  return (ret);
 }

 time_b = pri->getData()->date_b.Hours() + 
          pri->getData()->date_b.Minutes() +
          pri->getData()->date_b.Seconds() +
          pri->getData()->date_b.SubSeconds();
 time_e = pri->getData()->date_e.Hours() +
          pri->getData()->date_e.Minutes() +
          pri->getData()->date_e.Seconds() +
          pri->getData()->date_e.SubSeconds();
 pri->getData()->employee_id = Empl.id;
 pri->getData()->str_id      = Empl.str_id;

 if (pri->getData()->cnt_in == 0 || time_b == 0)
 {
  TimeSet = true;
  pri->getData()->cnt_in = cnt_new;
  pri->getData()->date_b = Empl.cur_time;
 }
 else
 if (pri->getData()->cnt_out == 0 || time_e == 0)
 {
  TimeSet = true;
  pri->getData()->cnt_out = cnt_new;
  pri->getData()->date_e  = Empl.cur_time;
 }

//  if (   (pri->getData()->cnt_in < pri->getData()->cnt_out)
//      or ( mt_ll.cnt is not null and pri->getData()->cnt_in != 0 and
//           (pri->getData()->cnt_in > mt_ll.cnt and pri->getData()->cnt_in > old_cnt_in)
//         )
//     ) then exception pack_cnt_error;

 if (TimeSet)
 {
  if (pri->Update()) ret = true; 
 }
 else
 {
  log("Tmainfrm::save() TimeSet is FALSE");
/*  if (dl->SQLException_SqlCode == 15)
  {
    GtkWidget * dialog;
    dialog = gtk_message_dialog_new (GTK_WINDOW(window),
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_ERROR,
                                   GTK_BUTTONS_OK,
                                   "Ошибка !\nНеверное количество в маршруте.\nПроверьте:\n кол-во после выполнения операции не может быть больше чем до начала операции,\n кол-во по всем ярлыкам не может быть меньше чем по операции");
    ret = gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
  }*/
  save_error_msg = "Неверное количество в маршруте.\nПроверьте:\n кол-во после выполнения операции не может быть больше чем до начала операции,\n кол-во по всем ярлыкам не может быть меньше, чем по операции.";
 }

 return(ret);
}
//---------------------------------------------------------------------------
void Tmainfrm::set_pri(int pack_route_id)
{
 std::stringstream stm;
 stm << __PRETTY_FUNCTION__ << " pack_route_id=" << pack_route_id;
 l->add(LOG_SHOW_TRACE, stm.str());
 get_pri(pack_route_id, pri);
}
//---------------------------------------------------------------------------
void Tmainfrm::get_pri(int pack_route_id, TPackRtItem  * dst)
{
// std::stringstream stm;
// stm << __PRETTY_FUNCTION__ << " pack_route_id=" << pack_route_id;
// l->add(LOG_SHOW_TRACE, stm.str());

 dst->SetDefault();

 if (pack_route_id > 0)
 {
  for(list<RPackRtItem>::iterator itr = route_list.begin(); itr != route_list.end(); ++itr)
  {
   if (itr->pack_route_id == pack_route_id)
   {
    dst->getData()->pack_route_id = pack_route_id;
    dst->getData()->pack_id       = itr->pack_id;
    dst->getData()->rt_item_id    = itr->rt_item_id;
    dst->getData()->cnt_in        = itr->cnt_in;
    dst->getData()->cnt_out       = itr->cnt_out;
    dst->getData()->cnt_defect    = itr->cnt_defect;
    dst->getData()->date_b        = itr->date_b;
    dst->getData()->date_e        = itr->date_e;
    dst->getData()->employee_id   = itr->employee_id;
    dst->getData()->descript      = itr->descript;
    dst->getData()->oper_price    = itr->oper_price;
    dst->getData()->oper_time     = itr->oper_time;
    dst->getData()->oper_num      = itr->oper_num;
    dst->getData()->str_id        = itr->str_id;
    dst->getData()->label_id      = itr->label_id;
    dst->getData()->cash          = itr->cash;
    break;
   }
 //  prev_cnt_out = itr->cnt_out;
 //log("init prev_cnt_out = %f\n", prev_cnt_out);
  }
 }
}
//---------------------------------------------------------------------------
int Tmainfrm::get_page(void) const
{
// int ret = 0;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

// ret = current_page;
 return (current_page);
}
//---------------------------------------------------------------------------
void Tmainfrm::set_page(int page)
{
 extern TParameterList params;

 std::stringstream stm("");
 stm << "void Tmainfrm::set_page(int page("<< page << "))";
 l->add(LOG_SHOW_TRACE, stm.str());

 gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), page);
 current_page = page;

// g_signal_emit_by_name (G_OBJECT(notebook), "switch-page", NULL, &result);

// g_signal_emit_by_name (G_OBJECT(notebook), "switch-page::", 
//                        GTK_NOTEBOOK(notebook), 
//                        NULL, 
//                        guint(page),
//                        gpointer(this), NULL);

// gdk_flush();
// G_UNLOCK(lock_mainfrm);
// gdk_threads_leave();
// while (gtk_events_pending())
//   gtk_main_iteration_do(FALSE);
  if (current_page == 0)
  {
   //gtk_entry_set_text(GTK_ENTRY(enumber), ""); 
   if (strlen (gtk_entry_get_text(GTK_ENTRY(enumber))) > 0)
   {
    if (params.get("time_clear_route_number")->v_int > 0)
    {
     //enumber_timer.stop();
     //enumber_timer.set_interval(params.get("time_clear_route_number")->v_int);
     //enumber_timer.user_data_i = atoi(gtk_entry_get_text(GTK_ENTRY(enumber))); 
     //enumber_timer.start();
     enumber_value = atoi(gtk_entry_get_text(GTK_ENTRY(enumber)));
     timeout_source =
     g_timeout_add(params.get("time_clear_route_number")->v_int * 1000, enumber_clear_idle_func, (gpointer)this);
    } // if (params.get("time_clear_route_number")->v_int > 0)
   } // if (strlen (gtk_entry_get_text(GTK_ENTRY(enumber))) > 0)
   gtk_window_set_focus(GTK_WINDOW(window), enumber);
  } // if (current_page == 0)
  if (current_page == 1)
  {gtk_window_set_focus(GTK_WINDOW(window), tree);}
  else
  {key_required = 0;}
  if (current_page == 2)
  {gtk_window_set_focus(GTK_WINDOW(window), eamount);}
  if (current_page == 3)
  {
   p4_refresh();
   p4_scount_label_refresh();
   gtk_window_set_focus(GTK_WINDOW(window), snumber);
  }
  if(current_page == 4)
  {
//   GdkColor * color = &gtk_widget_get_style(window)->bg[GTK_STATE_NORMAL];
//   gtk_sheet_set_background(GTK_SHEET(p5_sheet_defect), color);
//   gtk_sheet_range_set_background(GTK_SHEET(p5_sheet_defect))
//   gtk_widget_set_style(p5_sheet_defect, gtk_widget_get_style(window));
   p5_refresh();
//   gtk_window_set_focus(GTK_WINDOW(window), p5_gr_defect);
   gtk_window_set_focus(GTK_WINDOW(window), p5_sheet_defect);
  }
}
//---------------------------------------------------------------------------
void Tmainfrm::next_page(void)
{
 int cpage;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 cpage = get_page();
 if (cpage == 0)
 {
  if (route_list.size()>0)
  {
   char s[512];
//   if (label_list.size() > 0)
   if (mt_ll.get_row_count() > 0)
   {
    gchar * label;
    //list<RNomPackRouteLabel>::iterator itr;
    //itr = label_list.begin();
//    label = g_markup_escape_text(itr->nom_name.c_str(), -1);
    label = g_markup_escape_text(mt_ll.field_by_name("nom_name")->get_string().c_str(), -1);

    sprintf(s, "<span size=\"xx-large\">[%d] %s</span>", label_num, label);
    g_free(label);
   }
   else
   {
    sprintf(s, "<span size=\"xx-large\">Маршрут №%d</span>", label_num);
   }
   gtk_label_set_markup (GTK_LABEL (label2), s); 
   cpage++;
  }
  set_page(cpage);
  return;
 }

 if (cpage == 1)
 {
  if (pri->getData()->cnt_in > 0 && pri->getData()->cnt_out > 0)
  {
   log("Error next page: cnt_in > 0 && cnt_out > 0");
   return;
  }
  if (pri->getData()->pack_route_id > 0 && Empl.id > 0)
  {
   //timer1.stop();
   timer1_started = false;
   cpage++;
   set_page(cpage);
  }
  else
  {
   std::stringstream stm; stm << "Error next page: Pack_route_id=" << pri->getData()->pack_route_id
                              << " id=" << Empl.id;
   log(stm.str());
  }
  return;
 }

 if (cpage == 2)
 {
  cpage = 0;
  set_page(cpage);
  return;
 }

}
//---------------------------------------------------------------------------
void Tmainfrm::prev_page(void)
{

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (current_page == 0)
 {
  return;
 }

 if (current_page == 1)
 {
  //timer1.stop();
  timer1_started = false;
  current_page--;
  set_page(current_page);
  return;
 }

 if (current_page == 2)
 {
  current_page--;
  set_page(current_page);
  return;
 }

}
//---------------------------------------------------------------------------
void Tmainfrm::select_row(int row)
{
 GtkTreePath * path;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (row < 0) return;

 path = gtk_tree_path_new_from_indices(row, -1);
 gtk_tree_view_set_cursor_on_cell(GTK_TREE_VIEW(tree), path, NULL, NULL, FALSE);
 gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(tree), path, gtk_tree_view_get_column(GTK_TREE_VIEW(tree), 0), FALSE, 0, 0);
 gtk_tree_path_free(path);
}
//---------------------------------------------------------------------------
float Tmainfrm::get_prev_cnt_out(void)
{
 float ret = 0;
 list<RPackRtItem>::iterator itr;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (pri->getData()->pack_route_id > 0)
 {
  for(itr = route_list.begin(); itr != route_list.end(); ++itr)
  {
   if (itr->pack_route_id == pri->getData()->pack_route_id)
   {
    break;
   }
   ret = itr->cnt_out;
  }
 }
 return (ret);
}
//---------------------------------------------------------------------------
void Tmainfrm::check_key(void)
{

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
/* std::list<unsigned char> l;

 for(;;)
 {
  if (current_page != 1) return;
  kr->port.open_port();
  kr->port.read_port(&l);
  kr->port.close_port();

  while (gtk_events_pending()) gtk_main_iteration_do(FALSE);
  
  nnsleep(200000);
 }
*/
}
//---------------------------------------------------------------------------
void Tmainfrm::enumber_check(void)
{
 std::string curr;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 curr = (std::string)gtk_entry_get_text(GTK_ENTRY(enumber));

 if (curr.length() > 0)
 {
  std::string::iterator i;
  char isnum = 1;

  for(i = curr.begin(); i != curr.end(); ++i)
  {
   if (!isdigit(*i))
   {isnum = 0; break;}
  }
  if (isnum == 0)
  {
   gtk_entry_set_text(GTK_ENTRY(enumber), current_enumber_value.c_str());
   gtk_editable_set_position (GTK_EDITABLE (enumber), current_enumber_cursor); 
  }

 } // if (curr.lenth() > 0)

 current_enumber_value  = (std::string)gtk_entry_get_text(GTK_ENTRY(enumber));
 current_enumber_cursor = gtk_editable_get_position (GTK_EDITABLE (enumber));
}
//---------------------------------------------------------------------------
void Tmainfrm::enumber_clear(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 gtk_entry_set_text(GTK_ENTRY(enumber), "");
 current_enumber_value  = (std::string)"";
 current_enumber_cursor = gtk_editable_get_position (GTK_EDITABLE (enumber));
}
//---------------------------------------------------------------------------
void Tmainfrm::snumber_activate_event(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 std::string number(gtk_entry_get_text(GTK_ENTRY(snumber)));

 int Pack_route_id = pri->getData()->pack_route_id;

 if (!number.empty() && dl->SnToRouteI(Pack_route_id, number) > 0)
 {
  gtk_entry_set_text(GTK_ENTRY(snumber), "");
 }

 p4_refresh();
 p4_scount_label_refresh(); 

 gtk_window_set_focus(GTK_WINDOW(window), snumber);
}
//---------------------------------------------------------------------------
void Tmainfrm::p4_refresh(void)
{
 std::stringstream stm;
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
 GtkTextBuffer * buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(p4_text_view));
 int Pack_route_id = pri->getData()->pack_route_id;
 lnumber.clear();
 if (dl->SnToRouteSString(Pack_route_id, &lnumber) > 0)
 {
  for(std::list<std::string>::iterator itr = lnumber.begin(); itr != lnumber.end(); ++itr)
  {
   stm << *itr << std::endl;
  }
 }
 gtk_text_buffer_set_text(buffer, stm.str().c_str(), stm.str().length());
}
//---------------------------------------------------------------------------
void Tmainfrm::p4_scount_label_refresh(void)
{
 std::stringstream stm;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 stm << "<span color=\"";
 stm << ((lnumber.size() > pri->getData()->cnt_in) ? "red" : "black");
//stm << "red";
 stm << "\">";
 stm << lnumber.size();
 stm << "</span>";

 gtk_label_set_markup(GTK_LABEL(p4_scount_label), stm.str().c_str());
}
//---------------------------------------------------------------------------
void Tmainfrm::p5_refresh_cb_def_type(void)
{
 TDynamicList mt;
 GtkListStore *store;

 gtk_combo_box_set_model(GTK_COMBO_BOX(p5_cb_def_type), NULL);

 if (dl->DefDefectTypeListS(&mt) >= 0)
 {
  store = mt2model(&mt);
  for(int c = 0; c < mt.get_column_count(); c++){cb_def_type_column_name[Upper(mt.get_column_name(c))] = c;}
  gtk_combo_box_set_model(GTK_COMBO_BOX(p5_cb_def_type), GTK_TREE_MODEL(store));
  gtk_combo_box_set_column_span_column(GTK_COMBO_BOX(p5_cb_def_type), 2);
                                      // cb_def_type_column_name["DEFECT_TYPE_NAME"]);
 }
}
//---------------------------------------------------------------------------
void Tmainfrm::p5_refresh(void)
{
 extern TParameterList params;
// GtkListStore *store;
// TPackRtItem p(NULL);
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 int font_height(25);//(pango_font_description_get_size(gtk_widget_get_style(window)->font_desc) / PANGO_SCALE);
 int margin(3);

 p5_sheet_modify = true;

 gtk_sheet_freeze(GTK_SHEET(p5_sheet_defect));
 gtk_sheet_delete_rows(GTK_SHEET(p5_sheet_defect), 0, gtk_sheet_get_rows_count(GTK_SHEET(p5_sheet_defect)));

 int label_id(pri->getData()->label_id), pack_id(pri->getData()->pack_id);
#if GTKEXTRA_MAJOR_VERSION > 2
 p5_sheet_column0_width = gtk_sheet_get_column_width(GTK_SHEET(p5_sheet_defect), 0);
#endif
 if (dl->DefPackDefectsS(p5_mt, gtk_combo_box_get_active(GTK_COMBO_BOX(p5_cb_def_type)) + 1,
                              pack_id, label_id) >= 1)
 {
  int i(0);
  gtk_sheet_insert_rows(GTK_SHEET(p5_sheet_defect), 0, p5_mt->get_row_count());
  for(p5_mt->first(); !p5_mt->eof(); p5_mt->next(), i++)
  {
   gtk_sheet_set_row_height(GTK_SHEET(p5_sheet_defect), i, font_height * 2 + margin * 2);
//   GtkWidget * def_name = gtk_label_new(mt.field_by_name("defect_name")->get_string().c_str());
   GtkWidget * def_name = gtk_widget_new(GTK_TYPE_LABEL, "label",
                                         p5_mt->field_by_name("defect_name")->get_string().c_str(),
                                         "xalign", 0.0, "yalign", 0.0, NULL);
   gtk_label_set_line_wrap(GTK_LABEL(def_name), TRUE);
   gtk_widget_set_usize(def_name, p5_sheet_column0_width, font_height * 2);
   gtk_widget_show_all(def_name);
   gtk_sheet_attach(GTK_SHEET(p5_sheet_defect), def_name, i, 0, 0, 0, margin, margin);
//   gtk_sheet_set_cell_text(GTK_SHEET(p5_sheet_defect), i, 0,
//                           p5_mt->field_by_name("defect_name")->get_string().c_str());
   gtk_sheet_set_cell_text(GTK_SHEET(p5_sheet_defect), i, 1,
                           p5_mt->field_by_name("defect_count")->get_string().c_str());
  }
 }
 else
 {
  l->add(LOG_SHOW_TRACE | LOG_SHOW_ERROR, dl->errorstr);
 }

 if (p5_mt->get_row_count() > 0)
 {
  GtkSheetRange range; range.row0 = 0; range.col0 = 1; range.rowi = 0; range.coli = 1;
  gtk_sheet_select_range(GTK_SHEET(p5_sheet_defect), &range);
#if GTKEXTRA_MAJOR_VERSION > 2
  GdkColor * color = &gtk_widget_get_style(window)->bg[GTK_STATE_NORMAL];
//   gtk_sheet_set_background(GTK_SHEET(p5_sheet_defect), color);
  range.rowi = p5_mt->get_row_count() - 1;
  gtk_sheet_range_set_background(GTK_SHEET(p5_sheet_defect), &range, color);
#else
  range.row0 = 0; range.col0 = 0; range.rowi = p5_mt->get_row_count() - 1; range.coli = 0;
  gtk_sheet_range_set_editable(GTK_SHEET(p5_sheet_defect), &range, FALSE);
#endif
 }

 gtk_sheet_thaw(GTK_SHEET(p5_sheet_defect));

 p5_sheet_modify = false;
}
//---------------------------------------------------------------------------
void Tmainfrm::gr_defect_cell_edited(GtkCellRendererText *cell, const gchar *path_string, const gchar *new_text)
{
/* std::stringstream stm;
 stm << __PRETTY_FUNCTION__ << "new_text=" << new_text;
 l->add(LOG_SHOW_TRACE, stm.str());
 GValue a = {0};
 GtkTreePath *path = gtk_tree_path_new_from_string(path_string);
 GtkTreeIter iter;
 gint column = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(cell), "column"));
 GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(p5_gr_defect));

 gtk_tree_model_get_iter(model, &iter, path);

 if (gtk_tree_model_get_column_type(model, column) == G_TYPE_STRING)
 {
  g_value_init(&a, G_TYPE_STRING);
  g_value_set_string(&a, new_text);
  gtk_list_store_set_value(GTK_LIST_STORE(model), &iter, column, &a);
  g_value_unset(&a);
 }
 else
 if (gtk_tree_model_get_column_type(model, column) == G_TYPE_FLOAT)
 {
  float new_v(0);
  new_v = atof(new_text);
  g_value_init(&a, G_TYPE_FLOAT);
  g_value_set_float(&a, new_v);
  gtk_list_store_set_value(GTK_LIST_STORE(model), &iter, column, &a);
  g_value_unset(&a);
 }

 gtk_tree_model_get_iter(model, &iter, path);
 gtk_tree_path_free(path);

 int label_id(pri->getData()->label_id), pack_id(pri->getData()->pack_id);
 int pack_defects_id, defect_list_id;
 float defect_count; gchar *defect_descript;

 gtk_tree_model_get(model, &iter, column_name[std::string("PACK_DEFECTS_ID")], &pack_defects_id,
                                  column_name[std::string("DEFECT_LIST_ID")], &defect_list_id,
                                  column_name[std::string("DEFECT_COUNT")], &defect_count,
                                  column_name[std::string("DEFECT_DESCRIPT")], &defect_descript,
                                  -1);
// std::stringstream stm2;
// stm2 << "DefPackDefectsIU pack_defects_id=" << pack_defects_id << " pack_id=" << pack_id
//      << " label_id=" << label_id << " defect_list_id=" << defect_list_id << " defect_count=" << defect_count
//      << " defect_descript=" << std::string(defect_descript);
//  l->add(LOG_SHOW_TRACE, stm2.str());
 if (dl->DefPackDefectsIU(pack_defects_id, pack_id, label_id, defect_list_id, defect_count,
                      std::string(defect_descript)) < 0)
 {
    GtkWidget * dialog;
    gint ret;
    dialog = gtk_message_dialog_new (GTK_WINDOW(window),
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_ERROR,
                                   GTK_BUTTONS_OK,
                                   dl->errorstr.c_str());
    ret = gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
    p5_refresh();
 }*/
}
//---------------------------------------------------------------------------
void Tmainfrm::p5_cb_def_type_changed()
{
 p5_refresh();
}
//---------------------------------------------------------------------------
//gboolean Tmainfrm::p5_gr_selection_func(GtkTreeSelection *selection, GtkTreeModel *model, GtkTreePath *path,
//                                        gboolean path_currently_selected)
//{
// gboolean ret(FALSE);

// return(ret);
//}
//---------------------------------------------------------------------------
void Tmainfrm::p5_gr_selection_changed(GtkTreeSelection * selection)
{
 GtkTreeIter iter;
 GtkTreeModel * model;
 Tmainfrmptr self = NULL;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

// if (gtk_tree_selection_get_selected(selection, &model, &iter))
 {
//  gtk_tree_model_get (model, &iter, 7, &pack_route_id, -1);
// gtk_tree_view_row_activated (GTK_TREE_VIEW(p5_gr_defect),
//                              gtk_tree_model_get_path(model, &iter),
//                              gtk_tree_view_get_column(GTK_TREE_VIEW(p5_gr_defect), 2));
 }

// gtk_tree_view_column_focus_cel(gtk_tree_view_get_column(GTK_TREE_VIEW(p5_gr_defect), 2),
//                                                         GtkCellRenderer *cell);
}
//---------------------------------------------------------------------------
gboolean Tmainfrm::p5_sheet_traverse(gint row, gint column, gpointer *new_row, gpointer *new_column)
{
 {
  std::stringstream stm;
  stm << __PRETTY_FUNCTION__ << " row=" << row << " column=" << column;
  l->add(LOG_SHOW_TRACE, stm.str());
 }

 if (changed_row == row && changed_column == column && changed_row >= 0 && changed_column >= 1)
 {
/*
    PACK_DEFECTS_ID type of column DEF_PACK_DEFECTS.PACK_DEFECTS_ID,
    DEFECT_LIST_ID type of column DEF_PACK_DEFECTS.DEFECT_LIST_ID,
    DEFECT_NAME type of column DEF_DEFECT_LIST.DEFECT_NAME,
    DEFECT_COUNT type of column DEF_PACK_DEFECTS.DEFECT_COUNT,
    DEFECT_DESCRIPT type of column DEF_PACK_DEFECTS.DEFECT_DESCRIPT)
*/
  float defect_count(0);
  std::string new_v(gtk_sheet_cell_get_text(GTK_SHEET(p5_sheet_defect), changed_row, changed_column));
  if (!new_v.empty())
  {
  l->add(LOG_SHOW_TRACE, "atof " + new_v);
   try{defect_count = std::atof(new_v.c_str());}
   catch(...){return(FALSE);}
  }
  p5_mt->move_by(changed_row);

  /*std::stringstream stm;
  stm << "changed_row=" << changed_row << std::endl
      << "PACK_DEFECTS_ID=" << p5_mt->field_by_name("PACK_DEFECTS_ID")->get_integer() << std::endl
      << "pack_id=" << pri->getData()->pack_id << std::endl
      << "label_id=" << pri->getData()->label_id << std::endl
      << "DEFECT_LIST_ID=" << p5_mt->field_by_name("DEFECT_LIST_ID")->get_integer() << std::endl
      << "defect_count=" << defect_count << std::endl
      << "DEFECT_DESCRIPT=" << p5_mt->field_by_name("DEFECT_DESCRIPT")->get_string() << std::endl;
  l->add(LOG_SHOW_TRACE, stm.str());*/

  if (dl->DefPackDefectsIU(p5_mt->field_by_name("PACK_DEFECTS_ID")->get_integer(),
                           pri->getData()->pack_id,
                           pri->getData()->label_id,
                           p5_mt->field_by_name("DEFECT_LIST_ID")->get_integer(),
                           defect_count,
                           p5_mt->field_by_name("DEFECT_DESCRIPT")->get_string()) > 0)
  {
   p5_mt->edit();
   p5_mt->field_by_name("DEFECT_COUNT")->set_float(defect_count);
   p5_mt->post();
   p5_sheet_modify = true;
   gtk_sheet_set_cell_text(GTK_SHEET(p5_sheet_defect), changed_row, changed_column,
                           p5_mt->field_by_name("DEFECT_COUNT")->get_string().c_str());
   p5_sheet_modify = false;
  }
  else
  {
    GtkWidget * dialog;
    gint ret;
    dialog = gtk_message_dialog_new (GTK_WINDOW(window),
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_ERROR,
                                   GTK_BUTTONS_OK,
                                   dl->errorstr.c_str());
    ret = gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
    p5_refresh();
    return(FALSE);
  }
 }

 changed_row = -1;
 changed_column = -1;

 return(TRUE);
// return (new_column != NULL && *(gint *)new_column != 0); // На колонку 0 вставать нельзя
}
//---------------------------------------------------------------------------
void Tmainfrm::p5_sheet_changed(gint row, gint column)
{
 std::stringstream stm;

 if(p5_sheet_modify) return;

 stm << __PRETTY_FUNCTION__ << " row=" << row << " column=" << column;
// l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
 l->add(LOG_SHOW_TRACE, stm.str());
 changed_row = row;
 changed_column = column;
}
//---------------------------------------------------------------------------
void Tmainfrm::p5_sheet_clear_cell(gint row, gint column)
{
 std::stringstream stm;
// static gint r(0), c(1);

 if(p5_sheet_modify) return;

 stm << __PRETTY_FUNCTION__ << " row=" << row << " column=" << column;
// l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
 l->add(LOG_SHOW_TRACE, stm.str());
// changed_row = row;
// changed_column = column;

  p5_mt->move_by(row);
  p5_sheet_modify = true;
  gtk_sheet_freeze(GTK_SHEET(p5_sheet_defect));
  gtk_sheet_set_cell_text(GTK_SHEET(p5_sheet_defect), row, column,
                          p5_mt->field_by_name("DEFECT_COUNT")->get_string().c_str());
  gtk_sheet_thaw(GTK_SHEET(p5_sheet_defect));
  p5_sheet_modify = false;

// l->add(LOG_SHOW_TRACE, p5_mt->field_by_name("DEFECT_COUNT")->get_string());
// gtk_sheet_get_active_cell(GTK_SHEET(p5_sheet_defect), &r, &c);
// p5_sheet_traverse(row, column, NULL, NULL);
}
//---------------------------------------------------------------------------
/*static gboolean gtk_tree_view_expose (GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
  gint i = 0;
  gint current_x = 0, current_y = 0;
  GList *list, *cell_list;

  list = gtk_tree_view_get_columns(GTK_TREE_VIEW(widget));
  for (; list; list = list->next)
  {
    GtkTreeViewColumn *column = (GtkTreeViewColumn*) list->data;

    int spacing = gtk_tree_view_column_get_spacing(column); 

    if (! gtk_tree_view_column_get_visible(column)) continue;
    current_x += column->width;

    gdk_draw_line (event->window, *(widget->style->dark_gc), current_x - 1, 0,
                   current_x - 1, GTK_TREE_VIEW(widget)->priv->height);
  } // for (list = GTK_TREE_VIEW(widget)->priv->columns; list; list = list->next)
  g_list_free(list);

 return(FALSE);
}*/
//---------------------------------------------------------------------------
/*static void mainfrm_timer(int num_timer, void* data)
{
 Tmainfrmptr self = NULL;
 self = (Tmainfrmptr)data;
 
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

#ifdef USE_IDLE_ADD
 l->add(LOG_SHOW_DEBUG, (std::string)"before mainfrm_timer_idle_func adding");
 g_idle_add(mainfrm_timer_idle_func, (gpointer)data);
#else

 gdk_threads_enter();
 self->set_page(0);
 gdk_flush ();
 gdk_threads_leave();

#endif

 self->timer1.stop();
}*/
//---------------------------------------------------------------------------
// Функция для очистки enumber в потоке
// Ждет 5 сек и если номер не изменился очищает
/*static void enumber_clear_timer(int num_timer, void* data)
{
 int page;
 Tmainfrmptr self = NULL;
 self = (Tmainfrmptr)data;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

#ifdef USE_IDLE_ADD
 l->add(LOG_SHOW_DEBUG, (std::string)"before enumber_clear_idle_func adding");
 g_idle_add(enumber_clear_idle_func, (gpointer)data);
#else

 gdk_threads_enter();

 page = atoi(gtk_entry_get_text(GTK_ENTRY(self->enumber)));
 if (page != 0 && page == self->enumber_timer.user_data_i)
 {
  gtk_entry_set_text(GTK_ENTRY(self->enumber), "");
 }

 gdk_flush ();
 gdk_threads_leave();

#endif

 log("enumber timer done");
 self->enumber_timer.finish();
}*/
//---------------------------------------------------------------------------
//#ifdef USE_IDLE_ADD
gboolean enumber_clear_idle_func(gpointer data)
{
 int page;
 Tmainfrmptr self = NULL;
 self = (Tmainfrmptr)data;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 page = atoi(gtk_entry_get_text(GTK_ENTRY(self->enumber)));
// if (page != 0 && page == self->enumber_timer.user_data_i)
 if (page != 0 && page == self->enumber_value)
 {
  self->enumber_clear();
 }
 self->timeout_source = 0;
 return(FALSE);
}
//#endif
//---------------------------------------------------------------------------
//#ifdef USE_IDLE_ADD
gboolean mainfrm_timer_idle_func(gpointer data)
{
 int page;
 Tmainfrmptr self = NULL;
 self = (Tmainfrmptr)data;

 if (self->timer1_started)
 {
 self->timer1_started = false;
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
 self->set_page(0);
 }

 return(FALSE);
}
//#endif
//---------------------------------------------------------------------------
gboolean mainfrm_keys_idle_func(gpointer data)
{
 int page;
 Tmainfrmptr self = NULL;
 self = (Tmainfrmptr)data;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

// if (self->OnEmployeeRegister(self->kr->msg.keyNumber, self->kr->msg.devNumber))
 {
  float count = 0;
  char c_str[20];
  bool first = false;

//  self->kr->sendKey(1);
//  log((std::string)"Key found: -" + self->Empl.emp_name + (std::string)"-");

  if (self->route_list.begin()->pack_route_id == self->pri->getData()->pack_route_id)
  {
   first = true;
  }

  if (self->pri->getData()->cnt_in > 0)
  {count = self->pri->getData()->cnt_in;}
  else if (!first)
  {
   count = self->get_prev_cnt_out();
  }
  else
  {
    count = self->mt_ll.field_by_name("cnt")->get_float();
  }

  sprintf(c_str, "%.0f", count);
  gtk_entry_set_text(GTK_ENTRY(self->eamount), c_str);
  self->key_required = 0;
  if (self->get_page() == 1) {self->next_page();}
 }
// else
// {
//  std::stringstream stm;
//  self->kr->sendKey(0);
//  stm << "Key not found: " << std::hex << self->kr->msg.keyNumber;
//  log (stm.str());
// }

 return(FALSE);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
GtkListStore * mt2model(TDynamicList *mt)
{
 GtkListStore *store;

 GType * types = new GType[mt->get_column_count()];
 for(int c = 0; c < mt->get_column_count(); c++)
 {
  switch(mt->get_column_type(c))
  {
// dlArray, dlBlob, dlDate, dlTime, dlTimestamp, dlString,
//           dlSmallint, dlInteger, dlLargeint, dlFloat, dlDouble, dlNumeric
   case dlDate: types[c] = G_TYPE_STRING; break;
   case dlTime: types[c] = G_TYPE_STRING; break;
   case dlTimestamp: types[c] = G_TYPE_STRING; break;
   case dlString: types[c] = G_TYPE_STRING; break;
   case dlSmallint: types[c] = G_TYPE_INT; break;
   case dlInteger: types[c] = G_TYPE_INT; break;
   case dlLargeint: types[c] = G_TYPE_INT64; break;
   case dlDouble:
   case dlFloat: types[c] = G_TYPE_FLOAT; break;
   case dlNumeric: types[c] = G_TYPE_FLOAT; break;
   default: break;
  }
//   if (mt.get_column_type(c) == )
//  column_name[Upper(mt->get_column_name(c))] = c;
 }
 store = gtk_list_store_newv(mt->get_column_count(), types);
 delete [] types;

 GtkTreeIter iter;
 for(mt->first(); !mt->eof(); mt->next())
 {
  gtk_list_store_append(GTK_LIST_STORE(store), &iter);
  for(int c = 0; c < mt->get_column_count(); c++)
  {
   GValue a = {0};
   switch(mt->get_column_type(c))
   {
// dlArray, dlBlob, dlDate, dlTime, dlTimestamp, dlString,
//           dlSmallint, dlInteger, dlLargeint, dlFloat, dlDouble, dlNumeric
    case dlDate: break;
    case dlTime: break;
    case dlTimestamp: break;
    case dlString:  g_value_init(&a, G_TYPE_STRING);
                    g_value_set_string(&a, g_strdup(mt->field_by_number(c)->get_string().c_str()));
                    gtk_list_store_set_value(GTK_LIST_STORE(store), &iter, c, &a);
                    g_value_unset(&a); break;
    case dlSmallint: break;
    case dlInteger:g_value_init(&a, G_TYPE_INT);
                    g_value_set_int(&a, mt->field_by_number(c)->get_integer());
                    gtk_list_store_set_value(GTK_LIST_STORE(store), &iter, c, &a);
                    g_value_unset(&a); break;
    case dlLargeint: break;
    case dlFloat: break;
    case dlDouble: break;
    case dlNumeric: g_value_init(&a, G_TYPE_FLOAT);
                  //  TNumeric num; num.set_scale(mt->field_by_number(c)->get_numeric().get_scale());
                  //  num.set_v(mt->field_by_number(c)->get_numeric().get_v());
                    float f; f = (mt->field_by_number(c)->get_numeric().get_v() / pow10(mt->field_by_number(c)->get_numeric().get_scale()));
                    g_value_set_float(&a, f);
                    gtk_list_store_set_value(GTK_LIST_STORE(store), &iter, c, &a);
                    g_value_unset(&a); break;
    default: break;
   }
  }
 }
// gtk_tree_view_set_model(GTK_TREE_VIEW(p5_gr_defect), GTK_TREE_MODEL(store));

 return(store);
}
//---------------------------------------------------------------------------
void gtk_tree_vew_clear_column(GtkTreeView * tree)
{
 while(gtk_tree_view_get_column(tree, 0) != NULL)
 {
  gtk_tree_view_remove_column(tree, gtk_tree_view_get_column(tree, 0));
 }
}
//---------------------------------------------------------------------------
