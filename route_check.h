//
//
//

#ifndef ROUTE_CHECKH
#define ROUTE_CHECKH

#include <gtk/gtk.h>

//---------------------------------------------------------------------------
typedef  void (*TRefresh2)(int, int);     // тип переменной, значение
//---------------------------------------------------------------------------

int main(int argc, char *argv[]);
void main_refresh(int t, int v);
void print_help(void);
void signal_handler(int signum);
#endif
