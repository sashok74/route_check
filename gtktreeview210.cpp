#include <gtk/gtktree.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtktreeprivate.h>

#include <string.h>
#include <gdk/gdkkeysyms.h>


#include "gtktreeview210.h"
//---------------------------------------------------------------------------
/*gtktype gtk_tree_view210_get_type ()
{
  static guint ttt_type = 0;
  if (!ttt_type)
    {
      gtktypeinfo ttt_info =
      {
        "GtkTreeView210",
        sizeof (GtkTreeView210),
        sizeof (GtkTreeView210Class),
        (gtkclassinitfunc) gtk_tree_view_class_init,
        (gtkobjectinitfunc) gtr_tree_view_init,
        (gtkargsetfunc) null,
        (gtkarggetfunc) null
      };
      ttt_type = gtk_type_unique (gtk_tree_view_get_type (), &ttt_info);
    }
  return ttt_type;
}*/
//---------------------------------------------------------------------------

static void gtk_tree_view210_class_init (GtkTreeView210Class *p_class);
static void gtk_tree_view210_init (GtkTreeView *tree_view);
static gboolean gtk_tree_view210_expose (GtkWidget *widget, GdkEventExpose *event);

/* GType Methods */
GType gtk_tree_view210_get_type (void)
{
  static GType tree_view210_type = 0;

  //GType tree_view_type = gtk_tree_view_get_type (void);

  if (!tree_view210_type)
    {
      static const GTypeInfo tree_view210_info =
      {
        sizeof (GtkTreeView210Class),
        NULL,           /* base_init */
        NULL,           /* base_finalize */
        NULL, // (GClassInitFunc) gtk_tree_view210_class_init,
        NULL,           /* class_finalize */
        NULL,           /* class_data */
        sizeof (GtkTreeView210),
        0,              /* n_preallocs */
        NULL//(GInstanceInitFunc) gtk_tree_view210_init
      };

      tree_view210_type = g_type_register_static (GTK_TYPE_TREE_VIEW, "GtkTreeView210",
                                &tree_view210_info, (GTypeFlags)0);
    }

  return tree_view210_type;
}

//---------------------------------------------------------------------------
static void gtk_tree_view210_class_init (GtkTreeView210Class *p_class)
{
 GtkTreeViewClass tree;
 //tree.gtk_tree_view_class_init(p_class);
 ((GtkWidgetClass *)p_class)->expose_event = gtk_tree_view210_expose;
}
//---------------------------------------------------------------------------
static void gtk_tree_view210_init (GtkTreeView *tree_view)
{

}
//---------------------------------------------------------------------------
GtkWidget*   gtk_tree_view210_new (void)
{
 return ((GtkWidget*) g_object_new (GTK_TYPE_TREE_VIEW, NULL));
}
//---------------------------------------------------------------------------
static void
gtk_tree_view210_draw_grid_lines (GtkTreeView210 *tree_view,
                                  GdkEventExpose *event,
                                  gint            n_visible_columns)
{
  //_GtkTreeViewPrivate priv;
  //priv = tree_view->treeview.priv;
  
  GList *list = tree_view->treeview.priv->columns;
  
  gint i = 0;
  gint current_x = 0;

//  if (tree_view->priv->grid_lines != GTK_TREE_VIEW_GRID_LINES_VERTICAL
//      && tree_view->priv->grid_lines != GTK_TREE_VIEW_GRID_LINES_BOTH)
//    return;

  /* Only draw the lines for visible rows and columns */
  for (list = tree_view->treeview.priv->columns; list; list = list->next, i++)
    {
      GtkTreeViewColumn *column = (GtkTreeViewColumn*) list->data;

      /* We don't want a line for the last column */
      if (i == n_visible_columns - 1)
        break;

      if (! column->visible)
        continue;

      current_x += column->width;

      gdk_draw_line (event->window,
                     GTK_WIDGET(tree_view)->style->black_gc, // tree_view->grid_line_gc,
                     current_x - 1, 0,
                     current_x - 1, tree_view->treeview.priv->height);
    }
}

//---------------------------------------------------------------------------
static gboolean gtk_tree_view210_expose (GtkWidget *widget, GdkEventExpose *event)
{

}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

