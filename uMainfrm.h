//
//
//

#ifndef MAINFRMH
#define MAINFRMH

#include <gtk/gtk.h>

#include <gtkextra/gtksheet.h>
#include <ibpp/ibpp.h>

#include "route_check.h"
#include "uDataLayer.h"
#include "uKeyReader.h"
#include "uPackRtItem.h"
//#include "timer.h"
#include "dynamic_list.h"
//---------------------------------------------------------------------------
class Tmainfrm
{
 private:
    TDataLayer   * dl;
    int            label_num;
    int            current_page;

    std::string    current_enumber_value;
    gint           current_enumber_cursor;

    std::list<std::string> lnumber;

    gint changed_row, changed_column;
    TDynamicList * p5_mt;
    int p5_sheet_column0_width;

    bool p5_sheet_modify;

 public:
    guint timeout_source;

    TRefresh2      prefresh;
    int            p_type;
    TKeyReader   * kr;
    TEmployee      Empl;
    TPackRtItem  * pri;
    float          prev_cnt_out;
    char           visible;
    char           key_required;
    //TTimer         timer1;
    //TTimer         enumber_timer;
    int            enumber_value;
    bool           timer1_started;

    std::list<RPackRtItem> route_list;
//    list<RNomPackRouteLabel> label_list;
    TDynamicList   mt_ll;

    std::string    save_error_msg;

#define VISIBLE_COLUMN_COUNT 7
    //int            column_width[VISIBLE_COLUMN_COUNT];

    GtkWidget *window;
    GdkPixbuf *icon;
    GtkWidget *notebook;
    GtkWidget *p1_vbox, *p1_label;
    GtkWidget *label1, *enumber;
    GtkWidget *p2_vbox, *p2_label, *p2_alignment;
    GtkWidget *p3_vbox, *p3_label;
    GtkWidget *p4_vbox, *p4_label, *p4_scount_label, *snumber, *p4_text_view;
    GtkWidget *label2, *label3, *label4;
    GtkWidget *eamount;
    GtkListStore *store;
    GtkWidget *tree; GtkTreeSelection *select;
    GtkWidget *sw;

    GdkPixbuf *pic_linux_logo;
    GtkWidget *img_linux_logo;

    GtkWidget *p5_vbox, *p5_label, *p5_cb_def_type;//, *p5_gr_defect;
    std::map<std::string, int> column_name; // column name, column number
    std::map<std::string, int> cb_def_type_column_name; // column name, column number
//    GtkListStore *p5_store_defect;
    GtkWidget *p5_sw, *p5_sheet_defect;
//    GtkTreeSelection *p5_select;
 //public:
   Tmainfrm(TRefresh2, int, IBPP::Database);
  ~Tmainfrm(void);
   void build(void);
   void show(void);

   int read_route_list(int);
   bool OnEmployeeRegister (int64_t sn, int num);
   bool save(void);
   void set_pri(int);
   void get_pri(int, TPackRtItem  *);
   float get_prev_cnt_out(void);
   int  get_page(void) const;
   void set_page(int);
   void next_page(void);
   void prev_page(void);

   void check_key(void);

   void select_row(int);
   
   void enumber_check(void);
   void enumber_clear(void);
   void snumber_activate_event(void);

   void p4_refresh(void);
   void p4_scount_label_refresh(void);

   void p5_refresh_cb_def_type(void);
   void p5_refresh(void);
   void gr_defect_cell_edited(GtkCellRendererText *cell, const gchar *path_string, const gchar *new_text);
   void p5_cb_def_type_changed();
//   gboolean p5_gr_selection_func(GtkTreeSelection *selection, GtkTreeModel *model, GtkTreePath *path,
//                                             gboolean path_currently_selected);
   void p5_gr_selection_changed(GtkTreeSelection *);
   gboolean p5_sheet_traverse(gint row, gint column, gpointer *new_row, gpointer *new_column);
   void p5_sheet_changed(gint row, gint column);
   void p5_sheet_clear_cell(gint row, gint column);
};

typedef Tmainfrm* Tmainfrmptr;

Tmainfrm * MainfrmShow(TRefresh2, int, IBPP::Database);
void MainfrmKeys(unsigned char v, unsigned int s, void * p);

static gboolean mainfrm_delete_event(GtkWidget *widget, GdkEvent  *event, gpointer data);
static void     mainfrm_enumber_activate_event(GtkWidget *widget, gpointer data);
static gboolean mainfrm_enumber_key_release_event(GtkWidget *widget, GdkEventKey *event, gpointer data);
static void     mainfrm_eamount_event(GtkWidget *widget, gpointer data);
static void     mainfrm_destroy(GtkWidget *widget, gpointer data);
static void     mainfrm_selection_changed(GtkTreeSelection *, gpointer);
//static void mainfrm_notebook_switch_page_event(GtkNotebook *notebook,
//                                            GtkNotebookPage *page,
//                                            guint          page_num,
//                                            gpointer       user_data);
static void mainfrm_GtkTreeCellDataFunc (GtkTreeViewColumn *tree_column,
                                         GtkCellRenderer *cell,
                                         GtkTreeModel *tree_model,
                                         GtkTreeIter *iter,
                                         gpointer data);
static gboolean mainfrm_key_press_event(GtkWidget   *widget,
                                        GdkEventKey *event,
                                        gpointer     user_data);
//static void mainfrm_timer(int, void*);
//static void enumber_clear_timer(int num_timer, void* data);
//#ifdef USE_IDLE_ADD
gboolean mainfrm_keys_idle_func (gpointer data);
//#endif
gboolean enumber_clear_idle_func(gpointer data);
gboolean mainfrm_timer_idle_func(gpointer data);
static void mainfrm_snumber_activate_event(GtkWidget *widget, gpointer data);
static void mainfrm_p5_gr_defect_cell_edited(GtkCellRendererText *cell, const gchar *path_string,
                                             const gchar *new_text, gpointer data);
static void mainfrm_p5_cb_def_type_changed(GtkComboBox *widget, gpointer user_data);
//static gboolean mainfrm_p5_gr_selection_func(GtkTreeSelection *selection, GtkTreeModel *model, GtkTreePath *path,
//                                             gboolean path_currently_selected, gpointer data);
static void mainfrm_p5_gr_selection_changed(GtkTreeSelection *, gpointer data);
static gboolean mainfrm_p5_sheet_traverse(GtkSheet *sheet, gint row, gint column,
                                          gpointer *new_row, gpointer *new_column, gpointer user_data);
static void mainfrm_p5_sheet_changed(GtkSheet *sheet, gint row, gint column, gpointer user_data);
static void mainfrm_p5_sheet_clear_cell(GtkSheet *sheet, gint row, gint column, gpointer user_data);

GtkListStore * mt2model(TDynamicList *mt);
void gtk_tree_vew_clear_column(GtkTreeView * tree);
#endif
