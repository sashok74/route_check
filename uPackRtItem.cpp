//---------------------------------------------------------------------------
#include <string>
#include <list>

#include "log.h"
#include "uPackRtItem.h"

extern TLog * l;
//---------------------------------------------------------------------------
TPackRtItem::TPackRtItem(TDataLayer * p_dl)
{
 dl = p_dl;
 data.SetDefault();
}
//---------------------------------------------------------------------------
TPackRtItem::TPackRtItem(TDataLayer * p_dl, int id)
{
 dl = p_dl;
 data.SetDefault();

 if(id > 0)
  {
    data.pack_route_id = id;
    Select();
  }
}
//---------------------------------------------------------------------------
TPackRtItem::~TPackRtItem()
{
}
//---------------------------------------------------------------------------
RPackRtItem * TPackRtItem::getData(void)
{
// l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
 return(&data);
}
//---------------------------------------------------------------------------
bool TPackRtItem::Select(void)
{
 list<RPackRtItem> rs;
 list<RPackRtItem>::iterator itr; 
 bool ret(false);

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (dl->PackRtItemS(&rs, data.pack_route_id))
 {
  itr = rs.begin();
  data.cnt_in        = itr->cnt_in;
  data.cnt_out       = itr->cnt_out;
  data.date_b        = itr->date_b;
  data.date_e        = itr->date_e;
  data.employee_id   = itr->employee_id;
  data.employee_name = itr->employee_name;
  ret = true;
 }
 else
 {ret = false;}
 return(ret);
}
//---------------------------------------------------------------------------
bool TPackRtItem::Update(void)
{
 bool ret(false);

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (dl->PackRtItemIU(&data) > 0)
 {
  ret = true;
 }
 else
 {
  ret = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TPackRtItem::Delete(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
}
//---------------------------------------------------------------------------
void TPackRtItem::SetDefault(void)
{
// l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
 data.SetDefault();
}
//---------------------------------------------------------------------------
void TPackRtItem::Copy(TPackRtItem * v)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 data.pack_route_id = v->getData()->pack_route_id;
 data.label_id      = v->getData()->label_id;
 data.pack_id       = v->getData()->pack_id;
 data.rt_item_id    = v->getData()->rt_item_id;
 data.cnt_in        = v->getData()->cnt_in;
 data.cnt_out       = v->getData()->cnt_out;
 data.cnt_defect    = v->getData()->cnt_defect;
 data.date_b        = v->getData()->date_b;
 data.date_e        = v->getData()->date_e;
 data.employee_id   = v->getData()->employee_id;
 data.descript      = v->getData()->descript;
 data.employee_name = v->getData()->employee_name;
 data.str_id        = v->getData()->str_id;
 data.str_name      = v->getData()->str_name;
 data.oper_price    = v->getData()->oper_price;
 data.oper_time     = v->getData()->oper_time;
 data.oper_num      = v->getData()->oper_num;
 data.cash          = v->getData()->cash;
 data.pack_name     = v->getData()->pack_name; // varchar(150),
 data.doc_id        = v->getData()->doc_id;
 data.bom_id        = v->getData()->bom_id;
 data.time_sec      = v->getData()->time_sec;
 data.time_str_w    = v->getData()->time_str_w; // varchar(20),
 data.time_str_r    = v->getData()->time_str_r; // varchar(20),
 data.rt_time_w     = v->getData()->rt_time_w;
 data.rt_time_r     = v->getData()->rt_time_r;
 data.label_num     = v->getData()->label_num;
 data.priority      = v->getData()->priority;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
