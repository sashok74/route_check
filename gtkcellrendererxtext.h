#ifndef _gtk_cell_renderer_xtext_included_
#define _gtk_cell_renderer_xtext_included_

#include <gtk/gtk.h>

  /* Some boilerplate GObject type check and type cast macros.
   *  'klass' is used here instead of 'class', because 'class'
   *  is a c++ keyword */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

G_BEGIN_DECLS

#define GTK_TYPE_CELL_RENDERER_XTEXT           (gtk_cell_renderer_xtext_get_type())
#define GTK_CELL_RENDERER_XTEXT(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj),  GTK_TYPE_CELL_RENDERER_XTEXT, GtkCellRendererXText))
#define GTK_CELL_RENDERER_XTEXT_CLASS(klass)   (G_TYPE_CHECK_CLASS_CAST ((klass),  GTK_TYPE_CELL_RENDERER_XTEXT, GtkCellRendererXTextClass))
#define GTK_IS_CELL_XTEXT_XTEXT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_CELL_RENDERER_XTEXT))
#define GTK_IS_CELL_XTEXT_XTEXT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass),  GTK_TYPE_CELL_RENDERER_XTEXT))
#define GTK_CELL_RENDERER_XTEXT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj),  GTK_TYPE_CELL_RENDERER_XTEXT, GtkCellRendererXTextClass))

  typedef struct _GtkCellRendererXText GtkCellRendererXText;
  typedef struct _GtkCellRendererXTextClass GtkCellRendererXTextClass;

  /* GtkCellRendererXText: Our custom cell renderer
   *   structure. Extend according to need */

  struct _GtkCellRendererXText
  {
    GtkCellRendererText  parent_instance;
//    GtkVBox              parent_instance;
    //gdouble           xtext;
    bool                 draw_hor_line;
    bool                 draw_ver_line;
    char                 number_of_line;       /*количество строк в ячейке*/

    int                  wrap_width;           /* ширина для wrap-width*/
//    GtkWidget          * text_view;
//    GtkTextBuffer      * text_buffer;
  };


  struct _GtkCellRendererXTextClass
  {
    GtkCellRendererTextClass  parent_class;
//    GtkVBoxClass  parent_class;
  };


  GType                gtk_cell_renderer_xtext_get_type (void);

  GtkCellRenderer     *gtk_cell_renderer_xtext_new (void);


  typedef void (*prender)(GtkCellRenderer *cell,
                                GdkWindow       *window,
                                GtkWidget       *widget,
                                GdkRectangle    *background_area,
                                GdkRectangle    *cell_area,
                                GdkRectangle    *expose_area,
                                GtkCellRendererState     flags);

  typedef void (*pget_size) (GtkCellRenderer *cell,
                                 GtkWidget       *widget,
                                 GdkRectangle    *cell_area,
                                 gint            *x_offset,
                                 gint            *y_offset,
                                 gint            *width,
                                 gint            *height);

#ifdef __cplusplus
}
#endif /* __cplusplus */
G_END_DECLS
#endif /* _gtk_cell_renderer_xtext_included_ */
