#ifndef serviceH
#define serviceH

#include <string>
#include <sstream>
#include <ibpp/ibpp.h>
#include <iconv.h>

using namespace std;

//string     dbhost="", dbname="", dbuser="", dbpassword="", path="";
//string     fdb_path_server;

//string charset  = "koi8-r";
//string log_path = "/home/akhmetov/pfs.log";

std::string time_msg(const string msg);
void log(const char * log,  const char * msg);
void log(const char * file, const string msg);
void log(const string msg);


int cp2koi(string src, string & dst, int direction=1);
string cp2koi(string src, int direction);

IBPP::Timestamp tt2tst(const time_t t);
time_t tst2tt(IBPP::Timestamp * tst);
std::string tt2str(time_t t);
std::string tst2str(const IBPP::Timestamp *);

int create_file(const string * dst);

int parse_path(const string * full_path, string * path, string * name);
string name_server(int32_t fdb_file_id);
//int execSQL(IBPP::Statement st);
string i_to_a(int64_t v);
string timet_to_a(const time_t * tt);
string stat_to_a(const struct stat * stbuf);
void nnsleep(long);


#endif
