//
//
//

#ifndef LOGINFRMH
#define LOGINFRMH

#include <gtk/gtk.h>
#include <ibpp/ibpp.h>

#include "uMainfrm.h"

class Tloginfrm
{
 private:
 public:
    TRefresh2      prefresh;
    int            p_type;
    IBPP::Database * db;
    std::string dbhost, dbport, dbname, dbrole, dbuser, dbpassword;
// public:
    Tloginfrm *self;
    GtkWidget *window;
    GdkPixbuf *icon;
    GtkWidget *wtable;
    GtkWidget *hbox0, *hbox;
    GtkWidget *bok, *bcancel;
    GtkWidget *ldbserver, *edbserver, *ldbport, *edbport, *ldbname, *edbname, *ldbrole, *edbrole;
    GtkWidget *ldblogin, *edblogin, *ldbpass, *edbpass;
    GdkPixbuf *pix_sec;
    GtkWidget *img_sec;
 //public:
   Tloginfrm(TRefresh2, int, IBPP::Database * db);
  ~Tloginfrm(void);
   void build(void);
   void show(void);
   char connect(void);
};

typedef Tloginfrm* Tloginfrmptr;

//void LoginfrmShow(TRefresh2 p, int, IBPP::Database * db);
void LoginfrmShow(TRefresh2 p, int, IBPP::Database * db, std::string dbhost, std::string dbport, std::string dbname,  std::string dbrole,  std::string dbuser,  std::string dbpassword);

static void loginfrm_evnok(GtkWidget *widget, gpointer data);
static void loginfrm_evncancel(GtkWidget *widget, gpointer data);
static gboolean loginfrm_delete_event(GtkWidget *widget, GdkEvent  *event, gpointer data);
static void loginfrm_destroy(GtkWidget *widget, gpointer data);
static void loginfrm_entry_activate(GtkWidget *widget, gpointer data);

#endif
