#ifndef GTKTREEVIEW210
#define GTKTREEVIEW210

#include <gtk/gtktreeview.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_TYPE_TREE_VIEW210       (gtk_tree_view210_get_type ())
#define GtkTreeView210(obj)          gtk_check_cast (obj, GtkTreeView210_get_type (), GtkTreeView210)
#define GtkTreeView210_class(klass)  gtk_check_class_cast (klass, GtkTreeView210_get_type (), GtkTreeView210class)
#define is_GtkTreeView210(obj)       gtk_check_type (obj, GtkTreeView210_get_type ())
typedef struct _GtkTreeView210       GtkTreeView210;
typedef struct _GtkTreeView210Class  GtkTreeView210Class;

struct _GtkTreeView210
{
  GtkTreeView treeview;
//  gtkwidget *buttons[3][3];
  GdkGC * grid_line_gc;
};

struct _GtkTreeView210Class
{
  GtkTreeViewClass parent_class;
  //void (* GtkTreeView210) (GtkTreeView210 *ttt);
};

GType        gtk_tree_view210_get_type        (void);
GtkWidget*   gtk_tree_view210_new             (void);
//void         GtkTreeView210_clear           (GtkTreeView210 *ttt);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // GTKTREEVIEW210
