#ifndef parameterH
#define parameterH

#include <string>
#include <map>

//---------------------------------------------------------
class TParameter
{
 public:
   TParameter():require(0), hide(0), type(0), v_str(""), v_int(0){};
//   std::string name;
   char require; // 1 - обязательный
   char hide;    // 1 - скрывать
   char type;    // 0 - string, 1 - integer

   std::string v_str;
   int v_int;

//   void set();
//   get();
};
//---------------------------------------------------------
class TParameterList
{
 private:
   std::map<std::string, TParameter> db;

 public:
   TParameterList();

//   std::string program_name;

   void add(const std::string name, const char require, const char hide, const std::string v_str);
   void add(const std::string name, const char require, const char hide, const int v_int);
   void set(const std::string name, const std::string str);

   TParameter * get(const std::string name);
   bool exists(const std::string name);

   int init_from_enviroment(const std::string);
   int init_from_file(const std::string);
   int init_from_arg(int argc, char *argv[]);
//   int save_to_file(const std::string);
   std::string print(void);
};
//---------------------------------------------------------
//---------------------------------------------------------
//---------------------------------------------------------
//---------------------------------------------------------
//---------------------------------------------------------
#endif
