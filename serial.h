//---------------------------------------------------------------------------
#ifndef SerialH
#define SerialH

#include <fcntl.h>
#include <pthread.h>
#include <termios.h>
#include <string>
#include <list>

//---------------------------------------------------------------------------
/*typedef struct
{
 char v;
} RElement;*/
//---------------------------------------------------------------------------
typedef  void ( *TSerialReaded)(unsigned char, unsigned int, void *);
typedef  void * ( *TPThreadFunc) (void *);
//---------------------------------------------------------------------------
class TSerial
{
 private:
   int fd;                    // файловый дескриптор для порта 
   TPThreadFunc    tfuncptr;
   pthread_t       thread_id;
   pthread_mutex_t mutex;
 public:
   char opened;               // 1 - открыт, 0 - закрыт
   char runned;
   std::string port;          // port device name
   int flags;                 // open flags
   std::string errorstr;
   int interval;              // пауза между опросами порта, мс
   int serial_port_interval;  // пауза после обращений к COM-порту, мс

   TSerialReaded sreaded;     // callback
   void * sreaded_param;

   TSerial(void);
   ~TSerial(void);

   int  open_port(void);
   void close_port(void);

   int read_port(std::list<unsigned char> *);
   int write_port(std::list<unsigned char> *);

   int run(void);             // открывает поток и слушает порт

   int set_speed(speed_t);
};
//---------------------------------------------------------------------------
void * SerialThread (void *);
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


#endif


