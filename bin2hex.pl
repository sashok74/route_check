# convert binary file to hex

use Getopt::Long;

# config variable ########################################
$version              =  '2007-11-27';

%p = ('file'          => '',
      'do'            => 'help',
      'strlen'        => 14
);
################################################################
# run
 &main(\%p);

################################################################
sub main
{
 my $p = shift;
 &init_param($p);

 if (length($p->{'file'}) > 0) {do_file($p); exit(0);}
 if ($p->{'help'} || $p->{'do'} eq 'help') {do_help($p); exit(0);}

}
################################################################
sub init_param
{
 my $p = shift;

 GetOptions("help"          => \($p->{'help'}),
            "file=s"	    => \($p->{'file'}),
            "strlen=i"      => \($p->{'strlen'})
           );
# Check parametrs
 
}
################################################################
sub do_help
{
 my $p = shift;

 print("File converter bin -> hex Version:$version\n");
 print("Usage: .pl [option]\n");
 print("Option are:\n");
 print(" --help             list help\n");
 print(" --do               \n");
 return (1);
}
################################################################
sub do_file
{
 my $p = shift;
 my ($f, $s, $v, $c, $first);

 open($f,"<".$p->{'file'});
 if (!$f)
 {
  return (-1);
 }

 binmode($f);
 # seek ($f,0,2);
 # $ret = tell($f);

 $c = 0;
 $first = 1;
 while(read($f,$s,1))
 {

  if ($first == 1) {$first = 0;}
  else {print (",");} 

  $v = unpack("C",$s);
  printf("0x%02x",$v);   # 2 знака в hex
  if ($p->{'strlen'} > 0 && $c >= $p->{'strlen'})
  {
   printf("\n");
   $c = 0;
  }
  $c = $c + 1;
 } # while

  close($f);


 return (1);
}
################################################################
