//
#include <unistd.h>
#include "log.h"
#include "timer.h"

extern TLog * l;
//---------------------------------------------------------------------------
TTimer::TTimer(const int n):
 interval(n),
 number(0),
 runned(0)
{
 parent_event = NULL;
 tfuncptr     = TimerThread;
 pthread_mutex_init(&mutex, NULL);
}
//---------------------------------------------------------------------------
TTimer::TTimer(const int n, const int num):
 interval(n),
 number  (num),
 runned  (0)
{
 parent_event = NULL;
 tfuncptr     = TimerThread;
 pthread_mutex_init(&mutex, NULL);
}
//---------------------------------------------------------------------------
TTimer::~TTimer()
{
 stop();
}
//---------------------------------------------------------------------------
void TTimer::start(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 stop();
 pthread_mutex_lock(&mutex);

 runned = 1;
 pthread_create(&thread_id, NULL, tfuncptr, this);

 pthread_mutex_unlock(&mutex);
}
//---------------------------------------------------------------------------
void TTimer::stop(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 pthread_mutex_lock(&mutex);
// if (runned == 1)
// {
//  pthread_cancel(thread_id);
//  runned = 0;
// }
 finish();
 pthread_mutex_unlock(&mutex);
}
//---------------------------------------------------------------------------
void TTimer::finish(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
// pthread_mutex_lock(&mutex);
 if (runned == 1)
 {
  pthread_cancel(thread_id);
  runned = 0;
 }
// pthread_mutex_unlock(&mutex);
}
//---------------------------------------------------------------------------
void TTimer::set_interval(int n)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 stop();
 pthread_mutex_lock(&mutex);
 interval = n;
 pthread_mutex_unlock(&mutex);
}
//---------------------------------------------------------------------------
void TTimer::timer(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);
 
 if (parent_event != NULL)
 {
  parent_event(number, user_data);
 }

}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void * TimerThread(void * p)
{
 TTimer * s;
 s = (TTimer *) p;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
 while(s->runned == 1)
 {
  sleep(s->interval);
  pthread_testcancel();
  s->timer();
 }
 return (NULL);
}

//---------------------------------------------------------------------------
