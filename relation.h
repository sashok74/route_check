//
//
//

#ifndef RELATION
#define RELATION

//#include <gtk/gtk.h>
//#include "dynamic_list.h"
#include <list>
//---------------------------------------------------------
#define RELATION_DL_NEXT 1
#define RELATION_DL_PREV 2
#define RELATION_DL_GOTO 3
#define RELATION_DL_SETVAL 4
//---------------------------------------------------------
class Trelation
{
 private:
   std::list<Trelation *> relobj;

  virtual void send_to_relation(int command, void * param);
  virtual void receive_from_relation(int command, void * param);

 public:
//  Trelation();
//  virtual ~Trelation();

  virtual void append_relation(void *);
  virtual void remove_relation(void *);
};
typedef Trelation* Trelationptr;

//---------------------------------------------------------
//---------------------------------------------------------
#endif
