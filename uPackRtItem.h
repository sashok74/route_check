//---------------------------------------------------------------------------
#ifndef uPackRtItemH
#define uPackRtItemH
#include <string>
#include <list>

#include "uDataLayer.h"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TPackRtItem
{
 private:
  TDataLayer * dl;
  RPackRtItem  data;
 public:
   TPackRtItem(TDataLayer * p_dl);
   TPackRtItem(TDataLayer * p_dl, int v);
   ~TPackRtItem(void);
   RPackRtItem * getData(void);

  bool Select(void);
  bool Update(void);
  bool Delete(void);
  void SetDefault(void);
  void Copy(TPackRtItem *);
};
//---------------------------------------------------------------------------
#endif
