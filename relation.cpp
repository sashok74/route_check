//
//
//
//#include <gtk/gtk.h>
//#include "dynamic_list.h"
#include <list>
#include "relation.h"
//---------------------------------------------------------
//---------------------------------------------------------
//class Trelation
//{
// private:
//   std::list<int> relobj;
// public:
//  Trelation();
//  virtual ~Trelation();
//---------------------------------------------------------
void Trelation::append_relation(void * obj)
{
 Trelation * rel = (Trelation *) obj;
 relobj.push_back(rel); 
}
//---------------------------------------------------------
void Trelation::remove_relation(void * obj)
{
 std::list<Trelation *>::iterator itr;
 Trelation * rel = (Trelation *) obj;

 for(itr = relobj.begin(); itr != relobj.end(); ++itr)
 {
  if (rel == *itr) {relobj.erase(itr);break;}
 }
}
//---------------------------------------------------------
void Trelation::send_to_relation(int command, void * param)
{
 std::list<Trelation *>::iterator itr;
 for(itr = relobj.begin(); itr != relobj.end(); ++itr)
 {
  (*itr)->receive_from_relation(command, param);
 }

}
//---------------------------------------------------------
void Trelation::receive_from_relation(int command, void * param)
{

}
//---------------------------------------------------------
//---------------------------------------------------------
//---------------------------------------------------------
