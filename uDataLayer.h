//---------------------------------------------------------------------------
#ifndef uDataLayerH
#define uDataLayerH
#include <pthread.h>
#include <string>
#include <list>
#include <ibpp/ibase.h>
#include <ibpp/ibpp.h>
#include <ibpp/iberror.h>

#include "ibppquery.h"
#include "dynamic_list.h"
//---------------------------------------------------------------------------
using namespace std;
//---------------------------------------------------------------------------
/*typedef struct RRtRouteListS
{
 int    pack_route_id;
 int    pack_id;
 int    rt_item_id;
 float  cnt_in;
 float  cnt_out;
 float  cnt_defect; // numeric(18,4),
 time_t date_b; //timestamp,
 time_t date_e; // timestamp,
 int    employee_id;
 string descript; // varchar(100),
 float  oper_price; // numeric(18,4),
 int    str_id;
 string str_name; // varchar(50),
 float  cash;
 string pack_name; // varchar(150),
 string emp_name; // varchar(64),
 int    doc_id;
 int    bom_id;
 int    oper_time;
 int    time_sec;
 string time_str_w; // varchar(20),
 string time_str_r; // varchar(20),
 float   rt_time_w;
 float   rt_time_r;
 int   label_id;
 int   label_num;
 int   oper_num;
 int   priority;
 void clear(void)
 {
  pack_route_id = 0;
  pack_id = 0;
  rt_item_id = 0;
  cnt_in = 0;
  cnt_out = 0;
  cnt_defect = 0; // numeric(18,4),
  date_b = 0; //timestamp,
  date_e = 0; // timestamp,
  employee_id = 0;
  descript = ""; // varchar(100),
  oper_price = 0; // numeric(18,4),
  str_id = 0;
  str_name = ""; // varchar(50),
  cash = 0;
  pack_name = ""; // varchar(150),
  emp_name = ""; // varchar(64),
  doc_id = 0;
  bom_id = 0;
  oper_time = 0;
  time_sec = 0;
  time_str_w = ""; // varchar(20),
  time_str_r = ""; // varchar(20),
  rt_time_w = 0;
  rt_time_r = 0;
  label_id = 0;
  label_num = 0;
  oper_num = 0;
  priority = 0;
 };
 RRtRouteListS()
 {
  clear();
 };
} RRtRouteListS*/;

//---------------------------------------------------------------------------
typedef struct // class TPackRtItem
{
  int pack_route_id;
  int label_id;
  int pack_id;
  int rt_item_id;
  float cnt_in;
  float cnt_out;
  float cnt_defect;
  IBPP::Timestamp date_b;
  IBPP::Timestamp date_e;
  int employee_id;
  std::string descript;
  std::string employee_name;
  int str_id;
  std::string str_name;
  float oper_price;
  int oper_time;
  int oper_num;
  float cash;
  std::string pack_name; // varchar(150),
// std::string emp_name; // varchar(64),
  int    doc_id;
  int    bom_id;
  int    time_sec;
  std::string time_str_w; // varchar(20),
  std::string time_str_r; // varchar(20),
  float   rt_time_w;
  float   rt_time_r;
  int   label_num;
  int   priority;
 void SetDefault(void)
 {
  pack_route_id = 0;
  label_id      = 0;
  pack_id       = 0;
  rt_item_id    = 0;
  cnt_in      = 0;
  cnt_out     = 0;
  cnt_defect  = 0;
  date_b.Clear(); date_b.SetDate(1970,1,1);
  date_e.Clear(); date_e.SetDate(1970,1,1);
  employee_id   = 0;
  descript = "";
  employee_name = "";
  str_id        = 0;
  str_name = "";
  oper_price  = 0;
  oper_time     = 0;
  oper_num      = 0;
  cash        = 0;
  pack_name = ""; // varchar(150),
  doc_id     = 0;
  bom_id     = 0;
  time_sec   = 0;
  time_str_w = ""; // varchar(20),
  time_str_r = ""; // varchar(20),
  rt_time_w = 0;
  rt_time_r = 0;
  label_num   = 0;
  priority    = 0;
 };
} RPackRtItem;
//---------------------------------------------------------------------------
struct TEmployee
{
  int64_t id;
  std::string first_name;
  std::string middle_name;
  std::string last_name;
  int firm_id;
  std::string car_number;
  int permissions;
  IBPP::Time work_begin;
  IBPP::Time work_end;
  int staff_id;
  int employee_id;
  int prof_id;
  int category;
  std::string prof_name;
  std::string emp_name;
  int str_id;
  IBPP::Timestamp cur_time;
  std::string str_name;
  TEmployee()
  {
   SetDefault();
  };
  void SetDefault(void)
  {
    id = 0;
    first_name = "";
    middle_name = "";
    last_name = "";
    firm_id = 0;
    car_number = "";
    permissions = 0;
    work_begin.Clear();
    work_end.Clear();
    staff_id = 0;
    employee_id = 0;
    prof_id = 0;
    category = 0;
    prof_name = "";
    emp_name = "";
    str_id = 0;
    str_name = "";
  };
};
//---------------------------------------------------------------------------
struct RNomPackRouteLabel
{
 int number;// integer,
 int nom_id;// integer,
 int pack_id;// integer,
 int doc_id;// integer,
 std::string doc_number;// varchar(10),
 int pack_route_id;// integer,
 int start_rt_item_id;// integer,
 int stop_rt_item_id;// integer,
 float cnt;// numeric(18,4),
 float cnt_in;// numeric(18,4),
 IBPP::Timestamp time_b;// timestamp,
 IBPP::Timestamp time_e;// timestamp,
 int prev_label_id;// integer,
 int prev_label_number;// integer,
 int printed;// integer,
 int user_id;// integer,
 IBPP::Timestamp date_create;// timestamp,
 std::string nom_name;// varchar(100),
 std::string nom_sheet;// varchar(30),
 std::string user_name;// varchar(128),
 std::string   emp_name;// varchar(128),
 int str_id;// integer,
 std::string str_name;// varchar(50),
 int status;// smallint,
 int doc_registed;// integer,
 int priority;// integer,
 int archiv;// smallint,
 int first_label_id;// integer,
 int first_label_number;// integer
 void clear(void)
 {
  number = 0;// integer,
  nom_id = 0;// integer,
  pack_id = 0;// integer,
  doc_id = 0;// integer,
  doc_number.clear();// varchar(10),
  pack_route_id = 0;// integer,
  start_rt_item_id = 0;// integer,
  stop_rt_item_id = 0;// integer,
  cnt = 0;// numeric(18,4),
  cnt_in = 0;// numeric(18,4),
  time_b.SetDate(1970,1,1); time_e.SetTime(0,0,0,0);// timestamp,
  time_e.SetDate(1970,1,1); time_e.SetTime(0,0,0,0);// timestamp,
  prev_label_id = 0;// integer,
  prev_label_number = 0;// integer,
  printed = 0;// integer,
  user_id = 0;// integer,
  date_create.SetDate(1970,1,1); date_create.SetTime(0,0,0,0);// timestamp,
  nom_name.clear();// varchar(100),
  nom_sheet.clear();// varchar(30),
  user_name.clear();// varchar(128),
  emp_name.clear();// varchar(128),
  str_id = 0;// integer,
  str_name.clear();// varchar(50),
  status = 0;// smallint,
  doc_registed = 0;// integer,
  priority = 0;// integer,
  archiv = 0;// smallint,
  first_label_id = 0;// integer,
  first_label_number = 0;// intege
 };
 RNomPackRouteLabel(void)
 {
  clear();
 };
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TDataLayer
{
 private:
   IBPP::Database    db;
   IBPP::Transaction tr;
   IBPP::Statement   st;
//        TStatementEx st;
   TIBPPQuery      * q;
   pthread_mutex_t   mutex;
 public:
   std::string errorstr;
   int SQLException_SqlCode;
   int SQLException_EngineCode;

   TDataLayer(IBPP::Database p_db);
   ~TDataLayer(void);

   int RtRouteListSMem (std::list<RPackRtItem> * rs, int label_num);

   int PackRtItemS(std::list<RPackRtItem> * rs, int pack_route_id);
   int PackRtItemIU(RPackRtItem * rec);

   int EmpEmployeesInfBySnS(int64_t sn, TEmployee &prm);

 //  int NomPackRouteLabelSMem(list<RNomPackRouteLabel> * rs, int label_num);
   int NomPackRouteLabelSMem(TDynamicList * mt, const int label_num);
   int SnToRouteI(const int pack_route_id, const std::string& sn_text);
   int SnToRouteSString(const int pack_route_id, std::list<std::string> * Items);
   int DefPackDefectsS(TDynamicList * mt, const int defect_type_list_id, const int pack_id, const int label_id);
   int DefPackDefectsIU(const int pack_defects_id, const int pack_id, const int label_id, const int defect_list_id,
                        const float defect_count, const std::string defect_descript);
   int DefDefectTypeListS(TDynamicList * mt);

//   void test(void);
};
//---------------------------------------------------------------------------
#endif


