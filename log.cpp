#include <sys/time.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/types.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include <errno.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string>

#include "log.h"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//                                   TLog
//---------------------------------------------------------------------------
 TLog::TLog(void) : log_level(LOG_SHOW_INFO | LOG_SHOW_ERROR), show_time(true)
{
 gettimeofday(&last_flush, NULL);
 pthread_mutex_init(&mutex, NULL);
}
//---------------------------------------------------------------------------
 TLog::TLog(int16_t p) : log_level(p), show_time(true)
{
 gettimeofday(&last_flush, NULL);
 pthread_mutex_init(&mutex, NULL);
}
//---------------------------------------------------------------------------
//                                   TFileLog
//---------------------------------------------------------------------------
std::string TFileLog::now(void) const
{
 std::stringstream stm;
 time_t tt; struct tm loc_time; struct tm * pt = &loc_time;
 struct timeval tv;

 gettimeofday(&tv,NULL);
 tt = tv.tv_sec;
 pt = localtime(&tt);

// fprintf(f,"%04d-%02d-%02d %02d:%02d:%02d.%03ld %s\n",
//         pt->tm_year+1900,pt->tm_mon + 1,pt->tm_mday,
//         pt->tm_hour,pt->tm_min,pt->tm_sec, tv.tv_usec/1000,
//         msg);
 
 stm << pt->tm_year + 1900 << "-" << std::setw(2) << std::setfill ('0') << std::right
     << pt->tm_mon  + 1    << "-" << std::setw(2) << std::setfill ('0') << std::right
     << pt->tm_mday        << " " << std::setw(2) << std::setfill ('0') << std::right
     << pt->tm_hour        << ":" << std::setw(2) << std::setfill ('0') << std::right
     << pt->tm_min         << ":" << std::setw(2) << std::setfill ('0') << std::right
     << pt->tm_sec         << "." << std::setw(3) << std::setfill ('0') << std::right
     << tv.tv_usec / 1000  << std::left;
 return (stm.str());
}
//---------------------------------------------------------------------------
bool TFileLog::check_space(int64_t p_size) const
{
 bool ret(false);
 struct statvfs stbuf;

 if (log_file.length() > 0)
 {
  if (statvfs(log_file.c_str(), &stbuf) != -1)
  {
   if (stbuf.f_bsize > 0)
   {
    ret = (stbuf.f_bsize * stbuf.f_bavail) >= p_size;
   } // if (stbuf.f_bsize > 0)
  } // if (statvfs(log_file.c_str(), &stbuf) != -1)
 } // if (log_file.length() > 0)

 return(ret);
}
//---------------------------------------------------------------------------
void TFileLog::add(const int16_t level, const std::string msg)
{
 std::string tmsg(""); // = now() + " " + msg;

// std::cout << __PRETTY_FUNCTION__ << std::endl;

 if ( (log_level & level) == 0) {/*std::cout<<__PRETTY_FUNCTION__<<" log_level & level" <<std::endl;*/ return;}

 if (show_time) {tmsg += now() + (std::string)" ";}
 tmsg += msg;

 if (log_file.empty() || log_file == (std::string)"stdout")
 {
  std::cout << tmsg << std::endl;
 }
 else
 if (log_file == (std::string)"cerr")
 {
  std::cerr << tmsg << std::endl;
 }
 else
 {
  std::ofstream fout;
  fout.open(log_file.c_str(), std::ios::binary | std::ios::app);
  if (fout.fail())
  { std::cerr << "file not open:" << log_file << std::endl; return; }
  fout << tmsg << std::endl;
  fout.close();
 }

// append(tmsg);
// flush();

 return;
}
//---------------------------------------------------------------------------
void TFileLog::flush(void)
{
 std::list<std::string>::iterator itr;
 std::stringstream stm;

 pthread_mutex_lock(&mutex);

 if (messages.empty()) return;

 for(itr = messages.begin(); itr != messages.end(); ++itr)
 {
  stm << (*itr) << std::endl;
 } 

 if (log_file.empty())
 {
  std::cout << stm;
 }
 else
 if (log_file == (std::string)"cerr")
 {
   std::cerr << stm;
 }
 else
 {
  std::ofstream fout;
  fout.open(log_file.c_str(), std::ios::binary | std::ios::app);
  if (fout.fail())
  { 
   std::cerr << "file not open:" << log_file << std::endl;
   std::cerr << stm;
   return; 
  }
  fout << stm;
  fout.close();
 }

 messages.clear();
 gettimeofday(&last_flush,NULL);

 pthread_mutex_unlock(&mutex);
 return;
}
//---------------------------------------------------------------------------
void TFileLog::add_bin(void * buf, int buf_size) const
{
 (void) buf;
 (void) buf_size;
}
//---------------------------------------------------------------------------
void TFileLog::append(const std::string msg)
{
 pthread_mutex_lock(&mutex);
 messages.push_back(msg);
 pthread_mutex_unlock(&mutex);
}
//---------------------------------------------------------------------------
void TFileLog::post(void)
{
 std::list<std::string>::iterator itr;
 std::string s("");
 
 pthread_mutex_lock(&mutex);

 for(itr = messages.begin(); itr != messages.end(); ++itr)
 {
  s += " " + (*itr); 
 }

 if (!stm.str().empty())
  {s += " " + stm.str();}

 TLog::add(s);
 messages.clear();
 stm.str("");
 pthread_mutex_unlock(&mutex);
 return;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
