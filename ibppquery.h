//---------------------------------------------------------------------------
#ifndef ibppqueryH
#define ibppqueryH
//---------------------------------------------------------------------------

#include <string>
#include <list>
#include <algorithm>
#include <ibpp/ibase.h>
#include <ibpp/ibpp.h>
#include <ibpp/iberror.h>

#if defined USE_DYNAMIC_LIST
#include "dynamic_list.h"
#endif

#if defined __BCPLUSPLUS__
#include <system.hpp>
typedef __int64 int64_t;
#endif

//---------------------------------------------------------------------------
// ---------------_------  TQParami _----------------------------------------
//---------------------------------------------------------------------------
//typedef struct
class  TQParam
{
 private:
 public:
  int number;             ///< номер параметра
  int pos;                ///< позиция параметра в оригинальном запросе
  std::string paramName;       ///< Название параметра
  char isNull;            ///< 1 - Null, 0 - not Null

  IBPP::SDT       ft;
  std::string     v_str;       ///< значение
  int             v_int;
  int64_t         v_int64;
  double          v_float;
  IBPP::Timestamp v_ts;
  IBPP::Date      v_date;
  IBPP::Time      v_time;

  TQParam(void);
  TQParam(TQParam *);

  void SetString(const std::string);
  void SetInt(const int);
  void SetInt64(const int64_t);
  void SetFloat(const double);
  void SetDateTime(const IBPP::Timestamp);
  void SetDate(const IBPP::Date);
  void SetTime(const IBPP::Time);
  void SetNull(void);

  const std::string Value() const;
};
//---------------------------------------------------------------------------
bool TQParamSortPos(const TQParam& p1, const TQParam& p2);
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// ---------------------  TIBPPQuery ----------------------------------------
//---------------------------------------------------------------------------
/// Запросы к Firebird через IBPP
/** Обертка для IBPP*/
class TIBPPQuery
{
 private:
  IBPP::Database    db;
  IBPP::Transaction tr;
  IBPP::Statement   st;

  std::string u_sql;           ///< то что задал пользователь
  std::string p_sql;           ///< отпарсенный
  int numParams;               ///< Кол-во параметров
  std::list<TQParam> pqu;      ///< Параметры

  std::string db_enc, cl_enc;  ///< кодировка БД, пользователя

  void parse(void);       ///< Замена параметров
  //int InString(AnsiString str, AnsiString substr, int start); // Поиск вхождения подстроки с заданной позиции

  std::string Upper(const std::string);

  int cp2koi(const std::string & src, std::string & dst, const int direction) const;
  std::string cp2koi(const std::string & src, const int direction) const;
 public:
  IBPP::TAM am;

// SQLException
  int SQLException_SqlCode;
  int SQLException_EngineCode;
  std::string errorstr;

  TIBPPQuery();
  TIBPPQuery(IBPP::Database db);
  TIBPPQuery(IBPP::Database db, const std::string db_enc, const std::string cl_enc);
  ~TIBPPQuery(void);
  /// Задание запроса
  void SetQuery(const std::string sql);
  void SetQuery(const std::string sql, IBPP::TAM am);
  void SetNewQuery(const std::string sql, IBPP::TAM am);
  /// Задание параметров
  void SetParam(const std::string paramName, const std::string val);
  void SetParam(const std::string paramName, const int val);
  void SetParam(const std::string paramName, const int64_t val);
  void SetParam(const std::string paramName, const double val);
  void SetParam(const std::string paramName, const IBPP::Timestamp val);
  void SetParam(const std::string paramName, const IBPP::Date val);
  void SetParam(const std::string paramName, const IBPP::Time val);
#if defined __BCPLUSPLUS__
  void SetParam(const std::string paramName, const TDateTime val);
#endif
  void SetParamNull(const std::string paramName);

  const std::string ParamValueStr();

  /// Выполнение запроса
  bool exec(void);
  /// Выполнение запроса
  bool exec(IBPP::Statement& st);
  /// commit
  bool commit(void);
  /// Очистить параметры и запрос
  void reset(void);

  bool Fetch(void);
  bool IsNull(const std::string paramName);
  bool IsNull(const int paramNumber);
  bool GetValue (const std::string paramName, std::string& val);
  bool GetValue (const int paramNumber, std::string& val);
  bool GetValue (const std::string paramName, int& val);
  bool GetValue (const int paramNumber, int& val);
  bool GetValue (const std::string paramName, int64_t& val);
  bool GetValue (const int paramNumber, int64_t& val);
  bool GetValue (const std::string paramName, float& val);
  bool GetValue (const int paramNumber, float& val);
  bool GetValue (const std::string paramName, double& val);
  bool GetValue (const int paramNumber, double& val);
  bool GetValue (const std::string paramName, IBPP::Timestamp& val);
  bool GetValue (const int paramNumber, IBPP::Timestamp& val);
  bool GetValue (const std::string paramName, IBPP::Date& val);
  bool GetValue (const int paramNumber, IBPP::Date& val);
  bool GetValue (const std::string paramName, IBPP::Time& val);
  bool GetValue (const int paramNumber, IBPP::Time& val);

  void set_db(IBPP::Database db);
  void set_enc(const std::string db_enc, const std::string cl_enc);

  int         Columns()                const {return st->Columns();};
  IBPP::SDT   ColumnType(const int i)  const {return st->ColumnType(i);};
  std::string ColumnAlias(const int i) const {return cp2koi((std::string)st->ColumnAlias(i),1);};
  std::string ColumnName(const int i)  const {return cp2koi((std::string)st->ColumnName(i),1);};

#if defined USE_DYNAMIC_LIST
   int st_to_mt(TDynamicList * mt, IBPP::Statement);
   int st_to_mt(TDynamicList *);
#endif
};
//---------------------------------------------------------------------------
#endif
