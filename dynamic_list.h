
#ifndef DYNAMICLIST
#define DYNAMICLIST

#include <string>
#include <sstream>
#include <iomanip>

#include <list>
#include <vector>
#include <map>

#include <math.h>

#include <ibpp/ibpp.h>
#include "relation.h"
//---------------------------------------------------------------------------
//
// TODO:
// - сортировка
// - поиск
// - Up/Down - перемещение строк
//
// ??? iterator for TDynamic_list
//
//
//---------------------------------------------------------------------------
/*
 *                    std::exception
 *                           |
 *                 TDynamicListException
 *                 /                 \
 *    TDynamicListLogicException    TDynamicListDataException
 *
*/
class TDynamicListException //: public std::exception
{
 protected:
  std::string errormsg;
 public:
  virtual const std::string what(void) const throw() = 0;
//  virtual ~Exception() throw();
};
//---------------------------------------------------------------------------
class TDynamicListLogicException : public TDynamicListException
{
 public:
  TDynamicListLogicException(std::string msg)
  {
   errormsg = msg;
  }
//  virtual ~TDynamicListLogicException() throw();
  const std::string what(void) const throw()
  {
   return(" *** TDynamicListLogicException ***\n" + errormsg);
  };
};
//---------------------------------------------------------------------------
class TDynamicListDataException : public TDynamicListException
{
 public:
  TDynamicListDataException(std::string msg)
  {
   errormsg = msg;
  };
//  virtual int SqlCode() const throw() = 0;
//  virtual int EngineCode() const throw() = 0;
//  virtual ~TDynamicListDataException() throw();
  const std::string what(void) const throw()
  {
   return(" *** TDynamicListDataException ***\n" + errormsg);
  };
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TBytea                     // class for BLOB field
{
// friend class TBytea;
 private:
  int64_t         size;
  int64_t         position;
  unsigned char * data;
 public:
  TBytea();
  TBytea(const int64_t size);
  TBytea(const TBytea&);
  virtual ~TBytea();
  int64_t       set_size(const int64_t);
  const int64_t get_size(void) const;
  int64_t       set_position(const int64_t);
  const int64_t get_position(void) const;
  void          clear(void);
  void          free(void);
  int64_t       read(void *, const int64_t size);   // чтение из потока
  int64_t       write(void *, const int64_t size);  // запись в поток
// оператор =, [], +, <<, >>
  int64_t       load_from_file(const std::string);
  int64_t       save_to_file(const std::string) const;

  unsigned char * operator[](const int64_t) const;
  bool operator == (const TBytea n);
  TBytea& operator = (const int64_t n) {set_size(n); return (*this);};
};
//---------------------------------------------------------------------------
class TNumeric
{
 private:
  int64_t v;        // значение
  int     scale;    // кол-вр знаков после запятой

  int pow10(int p) const {return (int(pow((float)10, p)));}
 public:
  TNumeric():                               v(0),                  scale(4){};
//  TNumeric(const TNumeric &p):              v(p.v),                scale(p.scale){};

// TNumeric
  TNumeric& operator + (TNumeric n)
   {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v += n.v; return (*this);};
  TNumeric& operator - (TNumeric n)
   {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v -= n.v; return (*this);};
  TNumeric& operator * (TNumeric n)
   {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v=v*n.v/pow10(scale); return (*this);};
  TNumeric& operator / (TNumeric n)
   {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v=v/(n.v/pow10(scale)); return (*this);};
  TNumeric& operator += (TNumeric n)
   {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v += n.v; return (*this);};
  TNumeric& operator -= (TNumeric n)
   {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v -= n.v; return (*this);};
  TNumeric& operator *= (TNumeric n)
   {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v=v*n.v/pow10(scale); return (*this);};
  TNumeric& operator /= (TNumeric n)
   {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v=v/(n.v/pow10(scale)); return (*this);};
  TNumeric& operator ++ () {v += pow10(scale); return (*this);};
  TNumeric& operator -- () {v -= pow10(scale); return (*this);};

//  TNumeric operator ++
//  TNumeric operator +=
//  TNumeric operator --
//  TNumeric operator -=
//  TNumeric operator /=

  bool operator >  (TNumeric n) const {return ((v * pow10(scale))  <  (n.v * pow10(n.scale)) );};
  bool operator <  (TNumeric n) const {return ((v * pow10(scale))  >  (n.v * pow10(n.scale)) );};
  bool operator >= (TNumeric n) const {return ((v * pow10(scale))  <= (n.v * pow10(n.scale)) );};
  bool operator <= (TNumeric n) const {return ((v * pow10(scale))  >= (n.v * pow10(n.scale)) );};
  bool operator == (TNumeric n) const {return ((v / pow10(scale))  == (n.v / pow10(n.scale)) );};
  bool operator != (TNumeric n) const {return ((v / pow10(scale))  != (n.v / pow10(n.scale)) );};

// int
  TNumeric(const int val):                  v(val*pow10(4)),       scale(4){};
  TNumeric(const int val, const int sc):    v(val*pow10(sc)),      scale(sc){};
  TNumeric& operator = (int n)    {v = n * pow10(scale);     return (*this);};
  operator int()    { return(int    (v / pow10(scale)));};
  TNumeric& operator + (int n) {v += (n * scale); return (*this);};
  TNumeric& operator - (int n) {v -= (n * scale); return (*this);};
  TNumeric& operator * (int n) {v *= n; return (*this);};
  TNumeric& operator / (int n) {v /= n; return (*this);};

// int64_t
  TNumeric(const int64_t val):              v(val*pow10(4)),       scale(4){};
  TNumeric(const int64_t val,const int sc): v(val*pow10(sc)),      scale(sc){};
  TNumeric& operator = (int64_t n){v = n * pow10(scale);     return (*this);};
  operator int64_t(){ return(int64_t(v / pow10(scale)));};

// double
  TNumeric(const double val):               v(int(val*pow10(4))),  scale(4){};
  TNumeric& operator = (double n){v = int(n * pow10(scale)); return (*this);};
  operator double() { return(double (v / pow10(scale)));};

  TNumeric(const double val, const int sc): v(int(val*pow10(sc))), scale(sc){};

// float
  TNumeric(const float val):                v(int(val*pow10(4))),  scale(4){};
  TNumeric& operator = (float n){v = int(n * pow10(scale)); return (*this);};
  operator float() { return(float (v / pow10(scale)));};

  TNumeric(const float val, const int sc):  v(int(val*pow10(sc))), scale(sc){};

// std::string
  operator std::string() 
  {std::stringstream stm; int x,y; 
   x = int(v/pow10(scale)); y = v - x * pow10(scale);
   stm << x << "." << std::setw(scale) << std::setfill ('0') << std::right << y;
   return (stm.str()); };

  void set_scale(const int sc)
   {if (sc>scale) v *= pow10(sc-scale); if (sc<scale) v /= pow10(scale-sc); scale = sc;};
  int  get_scale(void) const {return (scale);};
  int64_t get_v(void) const {return (v);};
  void set_v(const int64_t val) {v = val;};
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//      Dynamic List Data Types
enum DLDT {dlArray, dlBlob, dlDate, dlTime, dlTimestamp, dlString,
           dlSmallint, dlInteger, dlLargeint, dlFloat, dlDouble, dlNumeric};
// FixMe append dlBoolean
//---------------------------------------------------------------------------
typedef enum{Browse, Edit, Append, Remove} DLState;
//---------------------------------------------------------------------------
class TDynamicCell // содержимое ячейки
{
 private:
//  bool is_null;
 public:
  TDynamicCell(): is_null(true){};
  TDynamicCell(const bool p) : is_null(p){};
  virtual TDynamicCell * make_new(void) = 0;
  virtual void clear(void) = 0;

  bool is_null;

//  virtual bool operator == (const TDynamicCell p) {return (is_null == p.is_null);};
  virtual bool touch(const TDynamicCell * p) {return (is_null == p->is_null);};
};
//---------------------------------------------------------------------------
template <typename CellType>
class TDynamicCellType : public TDynamicCell
{
 private:
 protected:
  CellType v;
 public:
  TDynamicCellType():v(0){};
//  TDynamicCellType(){};
  TDynamicCellType(const bool p):TDynamicCell(p){};
  TDynamicCellType(const CellType p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellType(const CellType p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  CellType  get_value(void) const  {return (v);}
  void set_value(const CellType n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellType(v, is_null); };
  void clear(void) {is_null = true; v = 0;}
  bool touch(const TDynamicCellType * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};
//---------------------------------------------------------------------------

//template <> TDynamicCellType<std::string>::TDynamicCellType():v(""){};
//template <> void TDynamicCellType<std::string>::clear(void) {is_null = true; v = "";};

template <>
class TDynamicCellType<std::string> : public TDynamicCell
{
 private:
 protected:
  std::string v;
 public:
  TDynamicCellType():v(""){};
//  TDynamicCellType(){};
  TDynamicCellType(const bool p):TDynamicCell(p){};
  TDynamicCellType(const std::string p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellType(const std::string p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  std::string  get_value(void) const  {return (v);}
  void set_value(const std::string n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellType<std::string>(v, is_null); };
  void clear(void) {is_null = true; v = "";};
  bool touch(const TDynamicCellType * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};
//template <>
//void TDynamicCellType<std::string>::clear(void) {is_null = true; v = "";};
//---------------------------------------------------------------------------
template <>
class TDynamicCellType<IBPP::Date> : public TDynamicCell
{
 private:
  IBPP::Date v;
 public:
  TDynamicCellType():v(1970, 1, 1){};
  TDynamicCellType(const IBPP::Date p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellType(const IBPP::Date p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  IBPP::Date  get_value(void) const  {return (v);}
  void set_value(const IBPP::Date n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellType<IBPP::Date>(v, is_null); };
  void clear(void) {is_null = true; v.SetDate(1970,1,1);};
  bool touch(const TDynamicCellType * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};
//---------------------------------------------------------------------------
template <>
class TDynamicCellType<IBPP::Timestamp> : public TDynamicCell
{
 private:
  IBPP::Timestamp v;
 public:
  TDynamicCellType():v(1970, 1, 1, 0,0,0,0){};
  TDynamicCellType(const IBPP::Timestamp p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellType(const IBPP::Timestamp p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  IBPP::Timestamp  get_value(void) const  {return (v);}
  void set_value(const IBPP::Timestamp n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellType<IBPP::Timestamp>(v, is_null); };
  void clear(void) {is_null = true; v.SetDate(1970,1,1); v.SetTime(0,0,0,0);};
  bool touch(const TDynamicCellType<IBPP::Timestamp> * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};
//---------------------------------------------------------------------------
template <>
class TDynamicCellType<IBPP::Time> : public TDynamicCell
{
 private:
  IBPP::Time v;
 public:
  TDynamicCellType():v(1970, 1, 1){};
  TDynamicCellType(const IBPP::Time p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellType(const IBPP::Time p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  IBPP::Time  get_value(void) const  {return (v);}
  void set_value(const IBPP::Time n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellType<IBPP::Time>(v, is_null); };
  void clear(void) {is_null = true; v.SetTime(0,0,0,0);};
  bool touch(const TDynamicCellType<IBPP::Time> * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};
//---------------------------------------------------------------------------
/*template <>
class TDynamicCellType<TBytea> : public TDynamicCell
{
 private:
  TBytea v;
 public:
  TDynamicCellType():v(){};
  TDynamicCellType(const TBytea p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellType(const TBytea p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  TBytea  get_value(void) const  {return (v);}
  void set_value(const TBytea n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellType<TBytea>(v, is_null); };
  void clear(void) {is_null = true; v.free();};
  bool touch(const TDynamicCellType<TBytea> * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};*/
//---------------------------------------------------------------------------
/*class TDynamicCellInteger : public TDynamicCellType<int>
{
 public:
  TDynamicCellInteger(){v = 0;};
  TDynamicCellInteger(const int p_v): TDynamicCellType<int>(false){v = p_v;};
};*/

/*class TDynamicCellInteger : public TDynamicCell
{
 private:
  int v;
 public:
  TDynamicCellInteger():v(0){};
  TDynamicCellInteger(const int p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellInteger(const int p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  int  get_value(void) const  {return (v);}
  void set_value(const int n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellInteger(v, is_null); };
  void clear(void) {is_null = true; v = 0;};
  bool touch(const TDynamicCellInteger * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};*/ 
//---------------------------------------------------------------------------
/*class TDynamicCellLargeInt : public TDynamicCell
{
 private:
  int64_t v;
 public:
  TDynamicCellLargeInt():v(0){};
  TDynamicCellLargeInt(const int64_t p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellLargeInt(const int64_t p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  int64_t  get_value(void) const  {return (v);}
  void set_value(const int64_t n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellLargeInt(v, is_null); };
  void clear(void) {is_null = true; v = 0;};
  bool touch(const TDynamicCellLargeInt * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};*/
//---------------------------------------------------------------------------
/*class TDynamicCellTNumeric : public TDynamicCell
{
 private:
  TNumeric v;
 public:
  TDynamicCellTNumeric():v(0){};
  TDynamicCellTNumeric(const TNumeric p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellTNumeric(const TNumeric p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  TNumeric  get_value(void) const  {return (v);}
  void set_value(const TNumeric n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellTNumeric(v, is_null); };
  void clear(void) {is_null = true; v = 0;};
  bool touch(const TDynamicCellTNumeric * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};*/
//---------------------------------------------------------------------------
/*class TDynamicCellString : public TDynamicCell
{
 private:
  std::string v;
 public:
  TDynamicCellString():v(""){};
  TDynamicCellString(const std::string p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellString(const std::string p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  std::string  get_value(void) const  {return (v);}
  void set_value(const std::string n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellString(v, is_null); };
  void clear(void) {is_null = true; v = "";};
  bool touch(const TDynamicCellString * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};*/
//---------------------------------------------------------------------------
/*class TDynamicCellFloat : public TDynamicCell
{
 private:
  float v;
 public:
  TDynamicCellFloat():v(0){};
  TDynamicCellFloat(const float p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellFloat(const float p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  float  get_value(void) const  {return (v);}
  void set_value(const float n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellFloat(v, is_null); };
  void clear(void) {is_null = true; v = 0;};
  bool touch(const TDynamicCellFloat * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};*/
//---------------------------------------------------------------------------
/*class TDynamicCellDouble : public TDynamicCell
{
 private:
  double v;
 public:
  TDynamicCellDouble():v(0){};
  TDynamicCellDouble(const double p_v): v(p_v), TDynamicCell(false){};
  TDynamicCellDouble(const double p_v, const bool p_is_null): v(p_v), TDynamicCell(p_is_null){};
  double  get_value(void) const  {return (v);}
  void set_value(const double n) {v = n; is_null = false;}
  TDynamicCell * make_new(void) {return (TDynamicCell *) new TDynamicCellDouble(v, is_null); };
  void clear(void) {is_null = true; v = 0;};
  bool touch(const TDynamicCellDouble * p) {return ( (v==p->v) && (TDynamicCell::touch(p)) );};
};*/
//---------------------------------------------------------------------------
class TDynamicRow   // строка таблицы
{
 private:
 public:
  std::list<TDynamicCell *> cell;
  TDynamicRow(){};
  TDynamicRow(const TDynamicRow &p) { from(&p); }
  //TDynamicRow operator=(const TDynamicRow);
  ~TDynamicRow();
  void from (const TDynamicRow * pr);
  TDynamicCell * get_cell(const int field_num);
  //std::list<TDynamicCell *> * get_list(void) {return (&cell);}; 

//  bool operator <  (TDynamicRow &r) const
//   {return ( true ); };
};
//---------------------------------------------------------------------
class TDynamicColumn // Описание столбца, структура
{
 private:
//  TDynamicCell * cell;
 public:
  int                     pos;
  TDynamicRow           * current_row;  
//  DLDT   data_type;
  std::string             name;         // название колонки
  DLDT                    column_type;
                              // другие параметры
  virtual int             get_integer(void) {return (0);};
  virtual int64_t         get_largeint(void){return (0);};
  virtual TNumeric        get_numeric(void) {return (0);};
  virtual float           get_float(void) {return (0);};
  virtual double          get_double(void) {return (0);};
  virtual IBPP::Timestamp get_timestamp(void) {return IBPP::Timestamp(1970,1,1,0,0,0,0);};
  virtual IBPP::Date      get_date(void) {return IBPP::Date(1970,1,1);};
  virtual IBPP::Time      get_time(void) {return IBPP::Time(0,0,0,0);};
  virtual TBytea          get_bytea(void) {return TBytea();};
  virtual std::string     get_string(void) {return (std::string("undefined"));};

  virtual void            set_integer(const int p) {};
  virtual void            set_largeint(const int64_t p) {};
  virtual void            set_numeric(const TNumeric p) {};
  virtual void            set_float(const float p) {};
  virtual void            set_double(const double p) {};
  virtual void            set_timestamp(const IBPP::Timestamp p) {};
  virtual void            set_date(const IBPP::Date p) {};
//  virtual void            set_date(const int year, const char month, const char day) {};
  virtual void            set_time(const IBPP::Time p) {};
  virtual void            set_bytea(const TBytea p) {};
  virtual void            set_string(const std::string p) {};
  virtual void            set_null(void) { current_row->get_cell(pos)->is_null = true;}

};
//---------------------------------------------------------------------------
template <typename ColumnType>
class TDynamicColumnType : public  TDynamicColumn
{
 private:
  //ColumnType value;
 public:
  //TDynamicColumnType(){column_type = dlInteger;};
  int                     pos;
  TDynamicRow           * current_row;
//  DLDT   data_type;
  std::string             name;         // название колонки
  std::string             type_name;
                              // другие параметры
 
 virtual ColumnType get_value(void)
 {TDynamicCellType<ColumnType> * cellptr = dynamic_cast<TDynamicCellType<ColumnType> *>(current_row->get_cell(pos));
    return (cellptr->get_value());};
 virtual void       set_value(ColumnType p)
 { TDynamicCellType<ColumnType> * cellptr =dynamic_cast<TDynamicCellType<ColumnType> *>(current_row->get_cell(pos));
    cellptr->set_value(p); };

};
//---------------------------------------------------------------------------
class TDynamicColumnInteger : public  TDynamicColumn
{
 private:
 public:
  TDynamicColumnInteger(){column_type = dlInteger;};

  int             get_integer(void)
   {TDynamicCellType<int> * cellptr = dynamic_cast<TDynamicCellType<int> *>(current_row->get_cell(pos));
    return (cellptr->get_value());};
  int64_t         get_largeint(void)
   {TDynamicCellType<int> * cellptr = dynamic_cast<TDynamicCellType<int> *>(current_row->get_cell(pos));
    return ((int64_t)cellptr->get_value());};
  std::string     get_string(void)
   {std::stringstream stm; stm << get_integer(); return (stm.str());};

  void            set_integer(const int p)
   { TDynamicCellType<int> * cellptr =dynamic_cast<TDynamicCellType<int> *>(current_row->get_cell(pos));
    cellptr->set_value(p); };
  void            set_largeint(const int64_t p)
   { TDynamicCellType<int> * cellptr =dynamic_cast<TDynamicCellType<int> *>(current_row->get_cell(pos));
    cellptr->set_value((int)p); };
};
//---------------------------------------------------------------------------
class TDynamicColumnLargeInt : public  TDynamicColumn
{
 private:
 public:
  TDynamicColumnLargeInt(){column_type = dlLargeint;};

  int             get_integer(void)
   {TDynamicCellType<int64_t> * cellptr = dynamic_cast<TDynamicCellType<int64_t> *>(current_row->get_cell(pos));
    return ((int)cellptr->get_value());};
  int64_t         get_largeint(void)
   {TDynamicCellType<int64_t> * cellptr = dynamic_cast<TDynamicCellType<int64_t> *>(current_row->get_cell(pos));
    return (cellptr->get_value());};
  std::string     get_string(void)
   {std::stringstream stm; stm << get_largeint(); return (stm.str());};

  void            set_integer(const int p)
   { TDynamicCellType<int64_t> * cellptr =dynamic_cast<TDynamicCellType<int64_t> *>(current_row->get_cell(pos));
    cellptr->set_value(p); };
  void            set_largeint(const int64_t p)
   { TDynamicCellType<int64_t> * cellptr =dynamic_cast<TDynamicCellType<int64_t> *>(current_row->get_cell(pos));
    cellptr->set_value((int)p); };
};
//---------------------------------------------------------------------------
class TDynamicColumnTNumeric : public  TDynamicColumn
{
 private:
 public:
  TDynamicColumnTNumeric(){column_type = dlNumeric;};

  TNumeric        get_numeric(void)
   {TDynamicCellType<TNumeric> * cellptr = dynamic_cast<TDynamicCellType<TNumeric> *>(current_row->get_cell(pos));
    return (cellptr->get_value());};
  int             get_integer(void)
   {TDynamicCellType<TNumeric> * cellptr = dynamic_cast<TDynamicCellType<TNumeric> *>(current_row->get_cell(pos));
    return ((int)cellptr->get_value());};
  int64_t         get_largeint(void)
   {TDynamicCellType<TNumeric> * cellptr = dynamic_cast<TDynamicCellType<TNumeric> *>(current_row->get_cell(pos));
    return ((int64_t)cellptr->get_value());};
  float           get_float(void)
   {TDynamicCellType<TNumeric> * cellptr = dynamic_cast<TDynamicCellType<TNumeric> *>(current_row->get_cell(pos));
    return ((float)cellptr->get_value());};
  double           get_double(void)
   {TDynamicCellType<TNumeric> * cellptr = dynamic_cast<TDynamicCellType<TNumeric> *>(current_row->get_cell(pos));
    return ((double)cellptr->get_value());};
  std::string     get_string(void)
   {std::stringstream stm; stm << (std::string)get_numeric(); return (stm.str());};

  void            set_numeric(const TNumeric p)
   { TDynamicCellType<TNumeric> * cellptr =dynamic_cast<TDynamicCellType<TNumeric> *>(current_row->get_cell(pos));
    cellptr->set_value(p); };
  void            set_integer(const int p)
   { TDynamicCellType<TNumeric> * cellptr =dynamic_cast<TDynamicCellType<TNumeric> *>(current_row->get_cell(pos));
    cellptr->set_value((TNumeric)p); };
  void            set_largeint(const int64_t p)
   { TDynamicCellType<TNumeric> * cellptr =dynamic_cast<TDynamicCellType<TNumeric> *>(current_row->get_cell(pos));
    cellptr->set_value((TNumeric)p); };
  void            set_float(const float p)
   { TDynamicCellType<TNumeric> * cellptr =dynamic_cast<TDynamicCellType<TNumeric> *>(current_row->get_cell(pos));
    cellptr->set_value((TNumeric)p); };
  void            set_double(const double p)
   { TDynamicCellType<TNumeric> * cellptr =dynamic_cast<TDynamicCellType<TNumeric> *>(current_row->get_cell(pos));
    cellptr->set_value((TNumeric)p); };
};
//---------------------------------------------------------------------------
class TDynamicColumnString : public  TDynamicColumn
{
 private:
 public:
  TDynamicColumnString(){column_type = dlString;};

  std::string     get_string(void)
   {TDynamicCellType<std::string> * cellptr = dynamic_cast<TDynamicCellType<std::string> *>(current_row->get_cell(pos));
    return (cellptr->get_value());};

  void            set_string(const std::string p)
   { TDynamicCellType<std::string> * cellptr =dynamic_cast<TDynamicCellType<std::string> *>(current_row->get_cell(pos));
    cellptr->set_value(p); };
};
//---------------------------------------------------------------------------
class TDynamicColumnFloat : public  TDynamicColumn
{
 private:
 public:
  TDynamicColumnFloat(){column_type = dlFloat;};

  float             get_float(void)
   {TDynamicCellType<float> * cellptr = dynamic_cast<TDynamicCellType<float> *>(current_row->get_cell(pos));
    return ((float)cellptr->get_value());};
  double         get_double(void)
   {TDynamicCellType<float> * cellptr = dynamic_cast<TDynamicCellType<float> *>(current_row->get_cell(pos));
    return ((double)cellptr->get_value());};
  std::string     get_string(void)
   {std::stringstream stm; stm << get_float(); return (stm.str());};

  void            set_float(const float p)
   { TDynamicCellType<float> * cellptr =dynamic_cast<TDynamicCellType<float> *>(current_row->get_cell(pos));
    cellptr->set_value(p); };
  void            set_double(const double p)
   { TDynamicCellType<float> * cellptr =dynamic_cast<TDynamicCellType<float> *>(current_row->get_cell(pos));
    cellptr->set_value((float)p); };
};
//---------------------------------------------------------------------------
class TDynamicColumnDouble : public  TDynamicColumn
{
 private:
 public:
  TDynamicColumnDouble(){column_type = dlDouble;};

  float          get_float(void)
   {TDynamicCellType<double> * cellptr = dynamic_cast<TDynamicCellType<double> *>(current_row->get_cell(pos));
    return ((float)cellptr->get_value());};
  double         get_double(void)
   {TDynamicCellType<double> * cellptr = dynamic_cast<TDynamicCellType<double> *>(current_row->get_cell(pos));
    return ((double)cellptr->get_value());};
  std::string     get_string(void)
   {std::stringstream stm; stm << get_double(); return (stm.str());};

  void            set_float(const float p)
   { TDynamicCellType<double> * cellptr =dynamic_cast<TDynamicCellType<double> *>(current_row->get_cell(pos));
    cellptr->set_value(p); };
  void            set_double(const double p)
   { TDynamicCellType<double> * cellptr =dynamic_cast<TDynamicCellType<double> *>(current_row->get_cell(pos));
    cellptr->set_value((float)p); };
};
//---------------------------------------------------------------------------
class TDynamicColumnTimestamp : public  TDynamicColumn
{
 private:
 public:
  TDynamicColumnTimestamp(){column_type = dlTimestamp;};

  IBPP::Timestamp    get_timestamp(void)
   {TDynamicCellType<IBPP::Timestamp> * cellptr = dynamic_cast<TDynamicCellType<IBPP::Timestamp> *>(current_row->get_cell(pos));
    return (cellptr->get_value());};
//  IBPP::Date         get_date(void)
//   {TDynamicCellType<IBPP::Timestamp> * cellptr = dynamic_cast<TDynamicCellType<IBPP::Timestamp> *>(current_row->get_cell(pos));
//    return ((IBPP::Date)cellptr->get_value());};
//  IBPP::Time         get_date(void)
//   {TDynamicCellType<IBPP::Timestamp> * cellptr = dynamic_cast<TDynamicCellType<IBPP::Timestamp> *>(current_row->get_cell(pos));
//    return ((IBPP::Time)cellptr->get_value());};
  std::string     get_string(void)
   {std::stringstream stm;
    IBPP::Timestamp dt(get_timestamp()); 
    stm << dt.Year()    << "-" << std::setw(2) << std::setfill ('0') << std::right
        << dt.Month()   << "-" << std::setw(2) << std::setfill ('0') << std::right
        << dt.Day()     << " " << std::setw(2) << std::setfill ('0') << std::right
        << dt.Hours()   << ":" << std::setw(2) << std::setfill ('0') << std::right
        << dt.Minutes() << ":" << std::setw(2) << std::setfill ('0') << std::right
        << dt.Seconds(); 
    return (stm.str());};

  void            set_timestamp(const IBPP::Timestamp p)
   { TDynamicCellType<IBPP::Timestamp> * cellptr =dynamic_cast<TDynamicCellType<IBPP::Timestamp> *>(current_row->get_cell(pos));
    cellptr->set_value(p); };
//  void            set_date(const IBPP::Date p)
//   { TDynamicCellType<IBPP::Timestamp> * cellptr =dynamic_cast<TDynamicCellType<IBPP::Timestamp> *>(current_row->get_cell(pos));
//    cellptr->set_value((IBPP::Timestamp)p); };
//   void            set_date(const IBPP::Time p)
//   { TDynamicCellType<IBPP::Timestamp> * cellptr =dynamic_cast<TDynamicCellType<IBPP::Timestamp> *>(current_row->get_cell(pos));
//    cellptr->set_value((IBPP::Timestamp)p); };
};
//---------------------------------------------------------------------------
class TDynamicColumnDate : public  TDynamicColumn
{
 private:
 public:
  TDynamicColumnDate(){column_type = dlDate;};

  IBPP::Date    get_date(void)
   {TDynamicCellType<IBPP::Date> * cellptr = dynamic_cast<TDynamicCellType<IBPP::Date> *>(current_row->get_cell(pos));
    return (cellptr->get_value());};
  std::string     get_string(void)
   {std::stringstream stm;
    IBPP::Date dt(get_date());
    stm << dt.Year()    << "-" << std::setw(2) << std::setfill ('0') << std::right
        << dt.Month()   << "-" << std::setw(2) << std::setfill ('0') << std::right
        << dt.Day();
    return (stm.str());};

  void          set_date(const IBPP::Date p)
   { TDynamicCellType<IBPP::Date> * cellptr =dynamic_cast<TDynamicCellType<IBPP::Date> *>(current_row->get_cell(pos));
    cellptr->set_value(p); };
 // void          set_date(const int year, const char month, const char day)
 //  {IBPP::Date p(year, month, day);
 //   TDynamicCellType<IBPP::Date> * cellptr =dynamic_cast<TDynamicCellType<IBPP::Date> *>(current_row->get_cell(pos));
 //   cellptr->set_value(p); };
};
//---------------------------------------------------------------------------
class TDynamicColumnTime : public  TDynamicColumn
{
 private:
 public:
  TDynamicColumnTime(){column_type = dlTime;};

  IBPP::Time    get_time(void)
   {TDynamicCellType<IBPP::Time> * cellptr = dynamic_cast<TDynamicCellType<IBPP::Time> *>(current_row->get_cell(pos));
    return (cellptr->get_value());};
  std::string     get_string(void)
   {std::stringstream stm;
    IBPP::Time dt(get_time());
    stm << std::setw(2) << std::setfill ('0') << std::right
        << dt.Hours()   << ":" << std::setw(2) << std::setfill ('0') << std::right
        << dt.Minutes() << ":" << std::setw(2) << std::setfill ('0') << std::right
        << dt.Seconds();
    return (stm.str());};

  void          set_time(const IBPP::Time p)
   { TDynamicCellType<IBPP::Time> * cellptr =dynamic_cast<TDynamicCellType<IBPP::Time> *>(current_row->get_cell(pos));
    cellptr->set_value(p); };
};
//---------------------------------------------------------------------------
class TDynamicColumnBytea : public  TDynamicColumn
{
 private:
 public:
  TDynamicColumnBytea(){column_type = dlBlob;};

  TBytea        get_bytea(void)
   {TDynamicCellType<TBytea> * cellptr = dynamic_cast<TDynamicCellType<TBytea> *>(current_row->get_cell(pos));
    return (cellptr->get_value());};
  std::string     get_string(void)
   {std::stringstream stm; TBytea b(get_bytea()); stm << "bytea size=" << b.get_size(); return (stm.str());};

  void          set_bytea(const TBytea p)
   { TDynamicCellType<TBytea> * cellptr =dynamic_cast<TDynamicCellType<TBytea> *>(current_row->get_cell(pos));
    cellptr->set_value(p); };
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
/*class TDynamicListIndexKey
{
 private:
 protected:
   TDynamicListIndex * idx;
   TDynamicRow       * row;
 public:
};*/
//---------------------------------------------------------------------------
/*class TDynamicListMultiColIndex
{
 private:
 protected:
   TDynamicRow * row;
   std::list<int> fields; std::list<int>::iterator itr_fields;
 public:
   virtual void clear_field(void)         { fields.clear(); itr_fields = fields.begin(); };
   virtual bool append_field(const int v) { fields.push_back(v); };
   virtual int  get_field(const int f_pos)
   virtual int  get_field_count(void) const {return(fields.count());};
};*/
//---------------------------------------------------------------------------
class TDynamicListIndex
{
 private:
 protected:
   bool is_eof, is_bof;
 public:
   TDynamicListIndex():index_name(""), field_num(0), is_eof(true), is_bof(true){};
//   virtual ~TDynamicListIndex(){};
   std::string index_name;               // название индекса
   int field_num;                        // номер ключевого поля

   virtual bool add(TDynamicRow *)                    = 0;
   virtual void remove(void)                          = 0;
   virtual void remove(const TDynamicRow *)           = 0;
   virtual void update(TDynamicRow *, TDynamicRow *)  = 0;
   virtual void clear_row(void)                       = 0;

   virtual TDynamicRow * first(void)       = 0;
   virtual TDynamicRow * last(void)        = 0;

   virtual TDynamicRow * next(void)        = 0;
   virtual TDynamicRow * prior(void)       = 0;
   virtual TDynamicRow * move_by(const int)= 0;

   virtual bool eof(void) const {return (is_eof);};
   virtual bool bof(void) const {return (is_bof);};

   virtual TDynamicRow * locate(const TDynamicCell *) = 0;
//   virtual TDynamicRow * lookup(const TDynamicCell *) = 0;

//   virtual void rebuild(void)                         = 0;
};
//---------------------------------------------------------------------------

//template <>
//bool std::less<IBPP::Date> (IBPP::Date a, IBPP::Date b) const
//{
// return (true);
//};
//---------------------------------------------------------------------------
template <typename IndexType>
class TDynamicListIndexType : public TDynamicListIndex
{
//  typedef typename std::multimap< IndexType, TDynamicRow *> TIndexTMap;
 private:
   typename std::multimap<IndexType, TDynamicRow *> index;
   typename std::multimap<IndexType, TDynamicRow *>::iterator current_pos;
 public:

   virtual ~TDynamicListIndexType()
   {
    index.clear();
   }; 

   bool add(TDynamicRow * row)
   {
    bool ret(false);
// Получить значение ключевого поля
    TDynamicCellType<IndexType> * cell;
    IndexType key;
    cell = dynamic_cast<TDynamicCellType<IndexType> *>(row->get_cell(field_num));
    key  = cell->get_value();
// Добавить
//    ret = (index.insert(std::make_pair(key, row))).second;
    index.insert(std::make_pair(key, row));
    ret = true;
    return(ret);
   };

   void remove(void)
   {
    if (!index.empty())
    {
     typename std::multimap<IndexType, TDynamicRow *>::iterator itr = current_pos;
     next();
     index.erase(itr);
    }    
   };
   void remove(const TDynamicRow * row)
   {
    if (!index.empty())
    {
     typename std::multimap<IndexType, TDynamicRow *>::iterator itr;
     for(itr = index.begin(); itr != index.end(); ++itr)
     {
      if (itr->second == row) {next(); index.erase(itr); break;}
     }
    }
   };
   void update(TDynamicRow * old_row, TDynamicRow * new_row)
   {
    typename std::multimap<IndexType, TDynamicRow *>::iterator itr;
    TDynamicCellType<IndexType> * old_cell, * new_cell;
    IndexType old_key, new_key;
    old_cell = dynamic_cast<TDynamicCellType<IndexType> *>(old_row->get_cell(field_num));
    new_cell = dynamic_cast<TDynamicCellType<IndexType> *>(new_row->get_cell(field_num));

    old_key  = old_cell->get_value();
    new_key  = new_cell->get_value();
    
    if (old_key == new_key) return;  // ключевое поле не изменилось
   // if (old_key.touch(new_key)) return;
    for(itr = index.find(old_key); itr != index.end(); ++itr)
    {
     if(itr->first != old_key) break;
     if(itr->second == old_row)
     {
      index.erase(itr);
      index.insert(std::make_pair(new_key, old_row)); // новый ключ старый указатель
      break;
     } // if
    } // for(itr = index.find(old_key)

   }
   void clear_row(void)
   {
    index.clear();
    is_eof = true; is_bof = true;
   };
  TDynamicRow * first(void)
  {
   if (!index.empty())
   {
    current_pos = index.begin();
    is_eof = false; is_bof = false;
   }
   else
   { 
    is_eof = true; is_bof = true;
    return (NULL);
   }
   return(current_pos->second);
  };
  TDynamicRow * last(void)
  {
   if (!index.empty())
   {
    current_pos = index.end();
    current_pos --;
    is_eof = false; is_bof = false;
   }
   else
   {
    is_eof = true; is_bof = true;
    return (NULL);
   }
   return(current_pos->second);
  };
  TDynamicRow * next(void)
  {
   if (!index.empty())
   {
    is_bof = false;
    current_pos ++;
    if (current_pos == index.end())
    { current_pos --; is_eof = true;}
   }
   else
   {
    is_eof = true; is_bof = true;
    return (NULL);
   }
   return(current_pos->second);
  };
  TDynamicRow * prior(void)
  {
   if (!index.empty())
   {
    is_eof = false;
    if (current_pos != index.begin()) {current_pos --; is_bof = false;}
    else {is_bof = true;}
   }
   else
   {
    is_eof = true; is_bof = true;
    return (NULL);
   }
   return(current_pos->second);
  };
  TDynamicRow * move_by(const int num)
  {
   //std::multimap<int, TDynamicRow *>::iterator itr;
   int i;
   int count = index.size();
   if (count < num)
   {
    std::stringstream stm;
    stm << "TDynamicListIndexType<Type>::move_by: num = " << num << " is more then count = " << count << ".";
    throw TDynamicListLogicException(stm.str());    
   }
   if (count > 0)
   {
   // if (num <= count/2)
    {
     current_pos = index.begin();
     for(i = 0; i < num; i++) current_pos++;
    }
   /* else
    {
     current_pos = index.end(); current_pos--;
     for(i = 0; i < (count - num); i++) current_pos--;
    }*/
    is_eof = false; is_bof = false;
    return (current_pos->second);
   } // if (count > 0)
   else return(NULL);
  };

  TDynamicRow * locate(const TDynamicCell * p)
  {
   //std::multimap<int, TDynamicRow *>::iterator itr;
   const TDynamicCellType<IndexType> * cell = dynamic_cast<const TDynamicCellType<IndexType> *>(p);
   IndexType key  = cell->get_value();
   current_pos = index.find(key);
   if (current_pos != index.end())
   {
    is_eof = false; is_bof = false;
    return(current_pos->second);
   }
   else
    return(NULL);
  };
//  TDynamicRow * lookup(const TDynamicCell * p)
//  {
   //std::multimap<int, TDynamicRow *>::iterator itr;
   //const TDynamicCellType<int> * cell = dynamic_cast<const TDynamicCellType<int> *>(p);
   //int key  = cell->get_value();
   // TODO: this func
//  };
}; 
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
/*class TDynamicListIndexInteger : public TDynamicListIndex
{
 private:
   std::multimap<int, TDynamicRow *> index;
   std::multimap<int, TDynamicRow *>::iterator current_pos;
 public:

   virtual ~TDynamicListIndexInteger()
   {
    index.clear();
   }; 

   bool add(TDynamicRow * row)
   {
// Получить значение ключевого поля
    TDynamicCellType<int> * cell;
    int key;
    cell = dynamic_cast<TDynamicCellType<int> *>(row->get_cell(field_num));
    key  = cell->get_value();
// Добавить
    index.insert(std::make_pair(key, row));
   };
   void remove(void)
   {
    if (!index.empty())
    {
     std::multimap<int, TDynamicRow *>::iterator itr = current_pos;
     next();
     index.erase(itr);
    }    
   };
   void remove(const TDynamicRow * row)
   {
    if (!index.empty())
    {
     std::multimap<int, TDynamicRow *>::iterator itr;
     for(itr = index.begin(); itr != index.end(); ++itr)
     {
      if (itr->second == row) {next(); index.erase(itr); break;}
     }
    }
   };
   void update(TDynamicRow * old_row, TDynamicRow * new_row)
   {
    std::multimap<int, TDynamicRow *>::iterator itr;
    TDynamicCellType<int> * old_cell, * new_cell;
    int old_key, new_key;
    old_cell = dynamic_cast<TDynamicCellType<int> *>(old_row->get_cell(field_num));
    new_cell = dynamic_cast<TDynamicCellType<int> *>(new_row->get_cell(field_num));

    old_key  = old_cell->get_value();
    new_key  = new_cell->get_value();
    
    if (old_key == new_key) return;  // ключевое поле не изменилось

    for(itr = index.find(old_key); itr != index.end(); ++itr)
    {
     if(itr->first != old_key) break;
     if(itr->second == old_row)
     {
      index.erase(itr);
      index.insert(std::make_pair(new_key, old_row)); // новый ключ старый указатель
      break;
     } // if
    } // for(itr = index.find(old_key)

   }
   void clear_row(void)
   {
    index.clear();
    is_eof = true; is_bof = true;
   };
  TDynamicRow * first(void)
  {
   if (!index.empty())
   {
    current_pos = index.begin();
    is_eof = false; is_bof = false;
   }
   else
   { 
    is_eof = true; is_bof = true;
    return (NULL);
   }
   return(current_pos->second);
  };
  TDynamicRow * last(void)
  {
   if (!index.empty())
   {
    current_pos = index.end();
    current_pos --;
    is_eof = false; is_bof = false;
   }
   else
   {
    is_eof = true; is_bof = true;
    return (NULL);
   }
   return(current_pos->second);
  };
  TDynamicRow * next(void)
  {
   if (!index.empty())
   {
    is_bof = false;
    current_pos ++;
    if (current_pos == index.end())
    { current_pos --; is_eof = true;}
   }
   else
   {
    is_eof = true; is_bof = true;
    return (NULL);
   }
   return(current_pos->second);
  };
  TDynamicRow * prior(void)
  {
   if (!index.empty())
   {
    is_eof = false;
    if (current_pos != index.begin()) {current_pos --; is_bof = false;}
    else {is_bof = true;}
   }
   else
   {
    is_eof = true; is_bof = true;
    return (NULL);
   }
   return(current_pos->second);
  };
  TDynamicRow * move_by(const int num)
  {
   //std::multimap<int, TDynamicRow *>::iterator itr;
   int i;
   int count = index.size();
   if (count < num)
   {
    std::stringstream stm;
    stm << "TDynamicListIndexInteger::move_by: num = " << num << " is more then count = " << count << ".";
    throw TDynamicListLogicException(stm.str());    
   }
   if (count > 0)
   {
    if (num <= count/2)
    {
     current_pos = index.begin();
     for(i = 0; i < num; i++) current_pos++;
    }
    else
    {
     current_pos = index.end(); current_pos--;
     for(i = 0; i < (count - num); i++) current_pos--;
    }
    is_eof = false; is_bof = false;
    return (current_pos->second);
   } // if (count > 0)
   else return(NULL);
  };

  TDynamicRow * locate(const TDynamicCell * p)
  {
   //std::multimap<int, TDynamicRow *>::iterator itr;
   const TDynamicCellType<int> * cell = dynamic_cast<const TDynamicCellType<int> *>(p);
   int key  = cell->get_value();
   current_pos = index.find(key);
   if (current_pos != index.end())
   {
    is_eof = false; is_bof = false;
    return(current_pos->second);
   }
   else
    return(NULL);
  };
  //TDynamicRow * lookup(const TDynamicCell * p)
  //{
   //std::multimap<int, TDynamicRow *>::iterator itr;
   //const TDynamicCellType<int> * cell = dynamic_cast<const TDynamicCellType<int> *>(p);
   //int key  = cell->get_value();
   // TODO: this func
  //};
};*/
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
typedef void *  TUserData;
typedef void (*TCallbackEvent_void_TUserData)(TUserData);
typedef bool (*TCallbackEvent_bool_TUserData)(TUserData);
//---------------------------------------------------------------------------
/// Динамический список
/** Класс для хранения табличных данных */
class TDynamicList : public Trelation
{
 private:
 protected:
   std::vector<TDynamicColumn *> header;       // структура таблицы
   DLState state;
   std::list<TDynamicRow *> row;               // строки
   std::list<TDynamicRow *>::iterator itr_row;  
   TDynamicRow * current_row;                   // текущая строка

   bool is_eof, is_bof;

   std::list<TDynamicListIndex *> idx;          // индексы
   std::list<TDynamicListIndex *>::iterator itr_idx;
   int current_index_num;
 public:
   TDynamicList();
   TDynamicList(const TDynamicList&);
   virtual ~TDynamicList();

   //! убрать все столбцы
   virtual void clear_column(void);
   //! добавить столбец
   virtual void add_column(const IBPP::SDT, const std::string);
   //! добавить столбец
   virtual void add_column(const DLDT, const std::string);
   //! Получить количество стобцов

 template <typename IndexType>  
   void add_column(const std::string typeName, const std::string columnName)
   {
    //IndexType p;
   
    TDynamicColumn * col   = new TDynamicColumnType<IndexType>();
    TDynamicCell   * ncell = new TDynamicCellType<IndexType>();

   //col->current_row = current_row;
   //col->pos         = header.size();
   //col->name        = p_name;
   //header.push_back(col);

    current_row->cell.push_back(ncell);
 
   };

   virtual int  get_column_count(void) const;

   virtual int get_column_num(const std::string) const;
   virtual std::string get_column_name(const int) const;
   virtual DLDT get_column_type(const std::string) const;
   virtual DLDT get_column_type(const int) const;

   //! Встать на первую строку
   virtual void first(void);
   //! Встать на последнюю строку
   virtual void last(void);
   //! Перейти на следущую запись
   virtual void next(void);
   //! Перейти на предыдущую запись
   virtual void prior(void);
   virtual void move_by(const int);   

   virtual bool eof(void) const;
   virtual bool bof(void) const;

   virtual TDynamicColumn * field_by_name(const std::string);
   //! нумерация с 0 
   virtual TDynamicColumn * field_by_number(const int);

   //! получить кол-во строк в таблице
   virtual int  get_row_count(void) const;
   //! удалить все строки
   virtual void clear_row(void);

   //! очистить строку (все NULL)
   virtual void clear(void);

   virtual void append(void);
   virtual void edit(void);
   virtual void remove(void);

   virtual void post(void);
   virtual void cancel(void);

//   --- Index
   virtual bool create_index(const std::string);      // название ключевого поля
   virtual bool create_index(const int);              // номер ключевого поля
   virtual void use_index(const std::string);
   virtual void use_index(const int);
   virtual void remove_index(const std::string);
   virtual void remove_index(const int);
   virtual void clear_index(void);                    // удалить все индексы

   virtual void rebuild_index(const std::string);     // перестроить индекс  
   virtual void rebuild_index(const int);

/*! TODO: Locate
 поиск по значению поля, если найдено: возвращается true и курсор встает на искомую строку
 если не найдено: возвращается false и курсор остается на текущей строке
 при наличии индекса он используется
 текущий индекс не меняется */
   virtual bool locate(const std::string, const TDynamicCell * p);
   virtual bool locate(const std::string, const int);
   virtual bool locate(const std::string, const int64_t);
//   virtual bool locate(const std::string, const TNumeric);
//   virtual bool locate(const std::string, const float);
//   virtual bool locate(const std::string, const double);
//   virtual bool locate(const std::string, const IBPP::Timestamp);
//   virtual bool locate(const std::string, const IBPP::Date);
//   virtual bool locate(const std::string, const IBPP::Time);
//   virtual bool locate(const std::string, const std::string);    

// TODO: Lokup 
// Поиск значения в таблице
//   virtual bool lookup(const std::string key_field, const TDynamicCell * key_value, const std::string res_field);

// Callback
   TCallbackEvent_void_TUserData event_after_scroll;
   TUserData                     user_data_after_scroll;
   virtual void after_scroll(void) {/*if (event_after_scroll!=NULL) (*event_after_scroll)(user_data_after_scroll);*/};

   TCallbackEvent_void_TUserData event_after_insert;
   TUserData                     user_data_after_insert;
   virtual void after_insert(void){/*if (event_after_insert!=NULL) (*event_after_insert)(user_data_after_insert);*/};

   TCallbackEvent_void_TUserData event_after_edit;
   TUserData                     user_data_after_edit;
   virtual void after_edit(void) {/*if (event_after_edit!=NULL) (*event_after_edit)(user_data_after_edit);*/};

   TCallbackEvent_bool_TUserData event_before_delete;
   TUserData                     user_data_before_delete;
   virtual bool before_delete(void)
   {/*if (event_before_delete!=NULL) return ((*event_before_delete)(user_data_before_delete)); else*/ return(true);};

   TCallbackEvent_void_TUserData event_after_delete;
   TUserData                     user_data_after_delete;
   virtual void after_delete(void) {/*if (event_after_delete!=NULL) (*event_after_delete)(user_data_after_delete);*/};

// relation
   void receive_from_relation(const int command, void * param);

};
std::string Upper(const std::string p);
//---------------------------------------------------------------------------
/*template < class T >
class Funct
{
public:
        Funct()
        {
         x = 0; 
        };
	Funct(T value)
	{
		x=value;
	};
	T GetValue()
	{
		return (x*x)-(2*x);
	};
private:
	T x;
};

class Func2 : public Funct<int>
{
};*/

#endif
