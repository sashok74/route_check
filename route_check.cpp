#include <gtk/gtk.h>
#include <ibpp/ibpp.h>

#include <string.h>
#include <signal.h>

#include "route_check.h"
#include "service.h"
#include "uMainfrm.h"
#include "uLoginfrm.h"
#include "parameter.h"
#include "log.h"

Tmainfrm * main_window;
IBPP::Database  db;
//std::string     port     = "/dev/ttyS0";
//std::string     log_path = "";
//int column_width[VISIBLE_COLUMN_COUNT];
TParameterList  params;
TFileLog file_log;
TLog * l = &file_log;

//---------------------------------------------------------------------------
int main(int argc, char *argv[])
{
 Tmainfrmptr wmain;
 TRefresh2 refresh = main_refresh; 

 std::string file_conf[5] = {"","route_check.conf", "~/.route_check.conf","/etc/route_check.conf",""};
// std::string dbport="3050", dbname="", dbrole="", dbuser="", dbpassword="";

// memset(column_width, 0, sizeof (column_width));

// обработка сигналов
{
 struct sigaction sa;
 memset (&sa, 0, sizeof(sa));
 sa.sa_handler = signal_handler;
 sigaction(SIGUSR1, &sa, NULL);       // reconnect DB
 sigaction(SIGTERM, &sa, NULL);
 sigaction(SIGHUP,  &sa, NULL);
}

// значения по-умолчанию
 params.add("dbhost",                  0, 0, "localhost");
 params.add("dbport",                  0, 0, 3050);
 params.add("dbname",                  0, 0, "planar");
 params.add("dbuser",                  1, 0, "");
 params.add("dbrole",                  0, 0, "");
 params.add("dbpassword",              1, 1, "");
 params.add("port",                    0, 0, "/dev/ttyS0");
 params.add("log",                     0, 0, "");
 params.add("log_level",               0, 0, LOG_SHOW_INFO | LOG_SHOW_ERROR);
 params.add("time_clear_route_number", 0, 0, 5);
 params.add("main_window_width",       0, 0, 640);
 params.add("main_window_height",      0, 0, 480);
 params.add("main_window_state",       0, 0, "maximize"); // maximize/iconify/fullscreen/natural
 params.add("key_reader",              0, 0, "aladdin");  // planar

 params.add("p5_columnwidth_1",        0, 0, 400);
 params.add("p5_columnwidth_2",        0, 0, 100);
// params.add("p5_columnwidth_3",        0, 0, 200);


 for(int a = 0; a < VISIBLE_COLUMN_COUNT; a++)
 {
  std::stringstream stm("");
  stm << "columnwidth_" << a + 1;
  params.add(stm.str(), 0, 0, 0);
 } //for

// read command line
 for(int i = 1; i < argc; i++)
 {
  if (strcmp(argv[i],"--help") == 0)
   {
    print_help();
    return(0);
   }
  if (strcmp(argv[i],"--config") == 0)
   {
    i++; if (i>=argc) break;
    file_conf[0] = argv[i];break;
   }
 }

 if (file_conf[0].empty())
 {if (::getenv("RKCONFIG")   !=NULL)     file_conf[0]   = ::getenv("RKCONFIG");}

// read config file
 for (int i = 0; i < 4; i++)
 {
  if (params.init_from_file(file_conf[i]) == 1)
  {
   file_conf[4] = file_conf[i];
   break;
  }
 }

 // read enviroment
 /*if (::getenv("RKDBHOST")!=NULL)     params.get("dbhost")->v_str     = ::getenv("RKDBHOST");
 if (::getenv("RKDBPORT")!=NULL)     params.get("dbport")->v_str     = ::getenv("RKDBPORT");
 if (::getenv("RKDBNAME")!=NULL)     params.get("dbname")->v_str     = ::getenv("RKDBNAME");
 if (::getenv("RKDBUSER")!=NULL)     params.get("dbuser")->v_str     = ::getenv("RKDBUSER");
 if (::getenv("RKDBROLE")!=NULL)     params.get("dbrole")->v_str     = ::getenv("RKDBROLE");
 if (::getenv("RKDBPASSWORD")!=NULL) params.get("dbpassword")->v_str = ::getenv("RKDBPASSWORD");
 if (::getenv("RKPORT")  !=NULL)     params.get("port")->v_str       = ::getenv("RKPORT");
 if (::getenv("RKLOG")   !=NULL)     params.get("log")->v_str        = ::getenv("RKLOG");


 for(int i = 0; i < VISIBLE_COLUMN_COUNT; i++)
 {
  std::stringstream stm, stm2;
  stm << "RKCOLUMNWIDTH_" << i + 1;
  stm2 << "columnwidth_" << i + 1;
  if (::getenv(stm.str().c_str()) != NULL) params.get(stm2.str())->v_int = atoi(::getenv(stm.str().c_str()));
 }*/

 params.init_from_enviroment("RK");

 params.init_from_arg(argc, argv);
// read command line
/* for(int i = 1; i < argc; i++)
 {
  //if (params.exists())
  // TODO: заменить перечисления на перебор
  if (strcmp(argv[i],"--dbhost") == 0)
   {
    i++; if (i>=argc) break;
    params.get("dbhost")->v_str = argv[i]; continue;
   }
  if (strcmp(argv[i],"--dbport") == 0)
   {
    i++; if (i>=argc) break;
    params.get("dbport")->v_str = argv[i];continue;
   }
  if (strcmp(argv[i],"--dbname") == 0)
   {
    i++; if (i>=argc) break;
    params.get("dbname")->v_str = argv[i];continue;
   }
  if (strcmp(argv[i],"--dbrole") == 0)
   {
    i++; if (i>=argc) break;
    params.get("dbrole")->v_str = argv[i];continue;
   }

  if (strcmp(argv[i],"--dbuser") == 0)
   {
    i++; if (i>=argc) break;
    params.get("dbuser")->v_str = argv[i];continue;
   }
  if (strcmp(argv[i],"--dbpassword") == 0)
   {
    i++; if (i>=argc) break;
    params.get("dbpassword")->v_str = argv[i];continue;
   }
  if (strcmp(argv[i],"--port") == 0)
   {
    i++; if (i>=argc) break;
    params.get("port")->v_str = argv[i];continue;
   }
  if (strcmp(argv[i],"--log") == 0)
   {
    i++; if (i>=argc) break;
    params.get("log")->v_str = argv[i];continue;
   }
  if (strcmp(argv[i],"--log_level") == 0)
  {
    i++; if (i>=argc) break;
    params.set("log_level", argv[i]);continue;
  }
  if (strcmp(argv[i],"--time_clear_route_number") == 0)
  {
    i++; if (i >= argc) break;
    params.set("time_clear_route_number", argv[i]);continue;
  }

  for(int a = 0; a < VISIBLE_COLUMN_COUNT; a++)
  {
   std::stringstream stm, stm2;
   stm << "--columnwidth_" << a + 1;
   stm2 << "columnwidth_" << a + 1;
   if (strcmp(argv[i], stm.str().c_str()) == 0)
   {
    i++; if (i>=argc) break;
    params.get(stm2.str())->v_int = atoi(argv[i]);
    break;
   } // if
  } //for

 } // for
*/
 file_log.log_file  = params.get("log")      -> v_str;
 file_log.log_level = params.get("log_level")-> v_int;

 l->add((std::string)"RouteCheck started, build: " 
       +(std::string)__DATE__ + (std::string)" " + (std::string)__TIME__);

 /*{
  extern const char *psvninfo;
  std::stringstream stm("SVN info:");
  stm << std::endl << psvninfo;
  l->add(stm.str());
 }*/

 l->add((std::string)"host_name:" + (std::string)g_get_host_name());
 l->add((std::string)"user_name:" + (std::string)g_get_user_name());

 if (!file_conf[4].empty())
  log((std::string)"Config file: " + file_conf[4]);

 log("Parameters:\n" + params.print());

 g_thread_init (NULL);
 gdk_threads_init ();

 if (!params.get("gtkrc")->v_str.empty())
 {
  char * rcfiles[2];
  rcfiles[0] = g_strdup(params.get("gtkrc")->v_str.c_str());
  rcfiles[1] = NULL;
  gtk_rc_set_default_files(rcfiles);
 }

 gtk_init (&argc, &argv);

//    wmain = MainfrmShow(); 

 LoginfrmShow(refresh, 1, &db, params.get("dbhost")->v_str, 
                               params.get("dbport")->v_str,
                               params.get("dbname")->v_str,
                               params.get("dbrole")->v_str,
                               params.get("dbuser")->v_str,
                               params.get("dbpassword")->v_str);

 gdk_threads_enter ();
 gtk_main();
 gdk_threads_leave ();
 //delete(wlogin);

 return (0);
}
//---------------------------------------------------------------------------
void main_refresh(int t, int v)
{
 TRefresh2 refresh = main_refresh;

 std::stringstream stm("");
 stm << "void main_refresh(int t("<< t <<"), int v("<< v <<"))";
 l->add(LOG_SHOW_TRACE, stm.str());

 if (t == 1)  // login
 {

  if (db->Connected())
  {
   std::stringstream stm;
   stm << "main_refresh Connect to " << db->ServerName() << ":" << db->DatabaseName() << " is successfull !";
   l->add(LOG_SHOW_INFO, stm.str());
  }

  if (v == 0) {db->Disconnect(); gtk_main_quit();}
  if (v == 1) {main_window = MainfrmShow(refresh, 2, db);}
 }

 if (t == 2)  // mainfrm
 {
  if (v == 0) 
  {
   try{db->Disconnect();}catch(...){}
   gtk_main_quit();
  }
 }
}
//---------------------------------------------------------------------------
void print_help(void)
{
  printf("Usage: ./route_check <options>\n");
  printf("Options are:\n");
  printf("--help                 print this help and exit\n");
  printf("--dbhost               database host\n");
  printf("--dbport               database port (3050)\n");
  printf("--dbname               database name\n");
  printf("--dbuser               database user name\n");
  printf("--dbrole               database role\n");
  printf("--dbpassword           database user password\n");
  printf("--port                 serial port device name (/dev/ttyS0)\n");
  printf("--columnwidth_<1...7>  width of column number, can be 1...7\n");
  printf("--log                  log file, if empty then STDOUT, cerr for STDERR\n");
  printf("--config               config file\n");
  printf("--key_reader           key reader model (aladdin)\n");
  printf(" * - required parameter\n");
  printf("Enviroment variable: RKDBHOST, RKDBNAME, RKDBUSER, RKDBROLE, RKDBPASSWORD, RKPORT, RKLOG, RKCONFIG, RKCOLUMNWIDTH_<1...7>\n");
  printf("Example: export RKDBPASSWORD=mypass; ./route_check --dbhost 192.168.68.250 --dbname mydb --dbuser sysdba --dbrole planar --port /dev/ttyS0 --key_reader planar columnwidth_1=20 columnwidth_2=120\n");
 return;
}
//---------------------------------------------------------------------------
void signal_handler(int signum)
{
 std::stringstream stm(""), msg("");
 stm << "void signal_handler(int signum(";

 switch(signum)
 {
  case SIGTERM:
  {
    stm << "SIGTERM";
    //g_signal_emit_by_name(G_OBJECT(main_window->window),"delete-event");
    gtk_widget_destroy (main_window->window);
    break;
  }
  case SIGUSR1:
  {
    stm << "SIGUSR1";
    msg << "DB reconnect";
    try{db->Disconnect();}catch(IBPP::Exception& e) {msg << std::endl << e.what();}
    try{db->Connect();}   catch(IBPP::Exception& e) {msg << std::endl << e.what();}
    break;
  }

  default: { stm << signum; }
 } // switch(signum)

 stm << "))";
 l->add(LOG_SHOW_INFO, stm.str());
 if (msg.str().length() > 0){l->add(LOG_SHOW_INFO, msg.str());}
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
