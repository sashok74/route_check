//---------------------------------------------------------------------------
#include <iostream>
#include "dynamic_list.h"
//---------------------------------------------------------------------------
int main(int argc, char *argv[])
{
 TDynamicList dl;
 std::cout << "dynamic_test" << std::endl;

 dl.add_column(dlInteger, "col_1");
 dl.add_column(dlString,  "col_2");
 dl.add_column<std::string>("std::string", "echo");

 for(int i = 10000; i > 0; i--)
 {
  std::stringstream stm("");
  dl.append();
  dl.field_by_number(0)->set_integer(i);
  stm << "i = " << i;
  dl.field_by_number(1)->set_string(stm.str());
  //dl.field_by_number(2)->set_value(stm.str());
  dl.post();
 }

 dl.create_index(0);
 dl.use_index(0);

 int c(0);
 dl.first();
 while(!dl.eof())
 {
//  if (c == 3)
//  {
//std::cout << "00" << std::endl;
//   dl.remove();
//std::cout << " 01" << std::endl;
//   dl.post();
//std::cout << "  02" << std::endl;
//   c = 0;
//  }
//  c++;
 std::cout << dl.field_by_number(1)->get_string() << std::endl;
  dl.next();
 }


 std::cout << "dl.get_row_count()=" << dl.get_row_count() << std::endl;

}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

