#ifndef LogH
#define LogH

//#include <string>
//#include <sstream>
//#include <iomanip>

//#include <list>
//#include <vector>
//#include <map>

#include <iostream>
#include <sstream>
#include <list>
#include <string>

#include <sys/time.h> 
#include <pthread.h>
#include <stdint.h>

const int16_t LOG_SHOW_ERROR = 1;          // Ошибки
const int16_t LOG_SHOW_TRACE = 1 << 1;     // Вызов функций
const int16_t LOG_SHOW_DEBUG = 1 << 2;     // Отладка
const int16_t LOG_SHOW_INFO  = 1 << 3;     // Информация
const int16_t LOG_SHOW_FULL  = 0xFFFF;     // Показывать все

//---------------------------------------------------------------------------
//                                   TLog
//---------------------------------------------------------------------------
class TLog
{
 protected:
// public: 
  std::list<std::string> messages; // буфер сообщений
  struct timeval last_flush;
  static const int flush_interval = 60;
  pthread_mutex_t  mutex;
 public:

 std::stringstream stm;

 int16_t log_level;
 bool    show_time;

   TLog(void);
   TLog(int16_t p);

 virtual ~TLog(void) {flush();};

 virtual int open(void) = 0;
 virtual int close(void)= 0;

// virtual void add(const char *) const = 0;
 virtual void add(const std::string p) {add(0xFF, p);};

 virtual void add(const int16_t level, const std::string) = 0;

 virtual void add_bin(void *, int buf_size) const = 0;

// virtual void append(const char *) = 0;
 virtual void append(const std::string) = 0;
 virtual void post(void) = 0;

 virtual int branch_open(void){return(0);};
 virtual void branch_close(int){};

 virtual void flush(void){};
};
//---------------------------------------------------------------------------
//                                   TFileLog
//---------------------------------------------------------------------------
class TFileLog : public TLog
{
 private:
   std::string now(void) const; 
   bool check_space(int64_t) const;
 public:

 std::string log_file;

 TFileLog(void):log_file("stdout") {};
 TFileLog(const std::string f):log_file(f) {};
 virtual ~TFileLog(void) {flush();};

 virtual int open(void) {return(0);};
 virtual int close(void) {return(0);};

// virtual void add(const char * msg) const {add((std::string)msg);};
 virtual void add(const int16_t level, const std::string);
 virtual void add_bin(void *, int buf_size) const;


// virtual void append(const char *) = 0;
 virtual void append(const std::string);

 virtual void post(void);
// virtual int branch_open(void){};
// virtual void branch_close(int){};

 virtual void flush(void);

};
//---------------------------------------------------------------------------
#endif
