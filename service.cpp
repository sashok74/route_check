#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include <errno.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
//#include <ibpp/iberror.h>

#include "parameter.h"
#include "log.h"
#include "service.h"

extern TLog * l;
std::string charset = "utf-8";
//-----------------------------------------------------------------------------
std::string time_msg(const string msg)
{
 std::stringstream stm;
 IBPP::Timestamp dt;
 dt.Now();

 stm << dt.Year() << "-"    << std::setw(2) << std::setfill ('0') << std::right
     << dt.Month()<< "-"    << std::setw(2) << std::setfill ('0') << std::right
     << dt.Day()<< " "      << std::setw(2) << std::setfill ('0') << std::right
     << dt.Hours()<< ":"    << std::setw(2) << std::setfill ('0') << std::right
     << dt.Minutes() << ":" << std::setw(2) << std::setfill ('0') << std::right
     << dt.Seconds() << " " << std::left
     << msg;
 return (stm.str());
}
//-----------------------------------------------------------------------------
/*void log(const char * log,  const char * msg)
{
 extern std::string log_path;

 FILE * f; time_t tt; struct tm loc_time; struct tm * pt = &loc_time;
 struct timeval tv;

 if (log == NULL)
 {
  if (log_path.length()==0) return;
  else log = log_path.c_str();
 }

 f=fopen(log,"a");
 if (f==NULL) {printf("Error open file:%s\n",log);return;}

 //tt = time(NULL);
 gettimeofday(&tv,NULL);
 tt = tv.tv_sec;
 pt = localtime(&tt);

 fprintf(f,"%04d-%02d-%02d %02d:%02d:%02d.%03ld %s\n",
             pt->tm_year+1900,pt->tm_mon + 1,pt->tm_mday,
             pt->tm_hour,pt->tm_min,pt->tm_sec, tv.tv_usec,
             msg);
 fclose(f);
}
void log(const char * file, const string msg)
{
 log(file, msg.c_str());
}*/
//-----------------------------------------------------------------------------
void log(const std::string msg)
{
 /*extern TParameterList params;
 std::string log_path = "";

 if (params.exists("log")) log_path = params.get("log")->v_str;

 std::string tmsg = time_msg(msg);
 if (log_path.empty())
 {
  std::cout << tmsg << std::endl;
 }
 else
 if (log_path == (std::string)"cerr")
  {
  std::cerr << tmsg << std::endl;
 }
 else
 {
  std::ofstream fout;
  fout.open(log_path.c_str(), ios::binary | ios::app);
  if (fout.fail())
  { std::cerr << "file not open:" << log_path << std::endl; return; }
  fout << tmsg << std::endl;  
  fout.close();
 }*/
 l->add(msg);
}
//-----------------------------------------------------------------------------
/* CP1251 -> KOI8-R  */
// 1 - win -> koi, 2 - koi -> win
int cp2koi(string src, string & dst, int direction)
{
 iconv_t cd;
 size_t k, f, t;
 int se;

 if (src.length() == 0) {dst.clear(); return(0);}

 char * code = (char*) src.c_str();
 char * in   = code;
 // char buf[100];
 char * buf = new char[(src.length()+1)*2];
 char * out = buf;

 if (direction==1) cd = iconv_open(charset.c_str(),"cp1251");
 else cd = iconv_open("cp1251",charset.c_str());
 if( cd == (iconv_t)(-1) ) std::cerr << "Error iconv" << std::endl;
  // err( 1, "iconv_open" );
 f = strlen(code);
 t = (src.length()+1) * 2;
 memset( buf, 0, t );
 errno = 0;

 k = iconv(cd, &in, &f, &out, &t);

 se = errno;

 dst = buf;

 delete [] buf;
 iconv_close(cd);

 return(1);
}
                                            
//-----------------------------------------------------------------------------
string cp2koi(string src, int direction)
{
 string ret="";
 if (cp2koi(src, ret, direction) == 1) return (ret);
 else return ("");
}
//-----------------------------------------------------------------------------
IBPP::Timestamp tt2tst(const time_t t)
{
 IBPP::Timestamp ret;
 struct tm loc_time; struct tm * pt = &loc_time;
 time_t tt = t;

 pt = localtime(&tt);
 ret.SetDate(pt->tm_year, pt->tm_mon, pt->tm_mday);
 ret.SetTime(pt->tm_hour, pt->tm_min, pt->tm_sec);

 return(ret);
}
//-----------------------------------------------------------------------------
time_t tst2tt(IBPP::Timestamp * tst)
{
 struct tm time;
 time_t ret;
 if (tst == NULL) return 0;

 tst->GetDate(time.tm_year,time.tm_mon,time.tm_mday);
 tst->GetTime(time.tm_hour,time.tm_min,time.tm_sec);
 time.tm_year  -= 1900;
 time.tm_mon   -= 1;
 time.tm_isdst = -1;

 ret = mktime(&time);
 if (ret!= -1) return(ret);
 else return(0);
}
//-----------------------------------------------------------------------
std::string tt2str(time_t t)
{
 std::string ret;
 char s[20];
 struct tm loc_time; struct tm * pt = &loc_time;
 time_t tt = t; 
 
 pt = localtime(&tt);

 sprintf(s,"%04d-%02d-%02d %02d:%02d:%02d\n",
             pt->tm_year+1900,pt->tm_mon + 1,pt->tm_mday,
             pt->tm_hour,pt->tm_min,pt->tm_sec);

 ret = s;
 return (ret);
}
//-----------------------------------------------------------------------
std::string tst2str(const IBPP::Timestamp * t)
{
 std::string ret;
 int year, month, day, hour, minute, sec, msec; 
 char s[20];

 t->GetDate(year, month, day);
 t->GetTime(hour, minute, sec, msec);
 
 sprintf(s,"%04d-%02d-%02d %02d:%02d:%02d\n",
             year, month, day, hour, minute, sec);

 ret = s;
 return (ret);
}
//-----------------------------------------------------------------------
/*int execSQL(IBPP::Statement st)
{
 int ret=0;
 try
 {
  st->Execute();
  ret = 1;
 }
 catch (IBPP::SQLException& e)
 {
  log(NULL,"execSQL");
  log(NULL,(char *) e.what());
  ret = 0;
  if (e.EngineCode() == isc_lost_db_connection) // SqlCode
  { // Коннект потерян
   //if (doReConnect())
   // {
   //   try        // Коннект есть, пробуем выполнить еще раз
   //   {
   //    st->Execute();
   //    ret = 1;
   //   }
   //   catch (IBPP::Exception& e)
   //   {
   //    log(NULL,"execSQL catch1");
   //    log(NULL,(char *) e.what());
    ret = 0;
   //   }
   // } //  if (doReConnect())
   } //  if (e.EngineCode() == isc_lost_db_connection)
 } //  catch (IBPP::SQLException& e)
 catch (IBPP::Exception& e)
 {
  log(NULL,"execSQL catch2");
  log(NULL,(char *) e.what());
  ret = 0;
  }
 return (ret);
}
*/
//-----------------------------------------------------------------------
int create_file(const string * dst)
{
 int ret = -1;
 int fd;
 struct stat stt;

 stt.st_mode = 0600;      //  rw- --- ---
 fd = open(dst->c_str(), O_WRONLY | O_CREAT, stt.st_mode);
 if (fd != -1)
 {
  ret = 1;
 }

 close(fd);
 return (ret); 
}
//-----------------------------------------------------------------------
int parse_path(const string * full_path, string * path, string * name)
{
 int ret = 0;
 string::size_type x, l;
 const string separator ="/"; 

 path->clear();
 name->clear();

// /hello/word
// /hello/
// /hello
// hello
// /

 l = full_path->length();
 if (l == 0) return (ret);
 x = full_path->rfind(separator);

 if (x == string::npos) // separator not found
 {
  name->assign(*full_path);
 }
 else
 {
//  path = full_path->substr(0, x);
//  name = full_path->substr(x + 1, l - x - 1);
  path->assign(*full_path, 0, x + 1);
  name->assign(*full_path, x + 1, l - x - 1);
  ret = 1;
 }

 return (ret);
}
//-----------------------------------------------------------------------
string name_server(int32_t fdb_file_id)
{
 string ret="";
 char s[32];
 sprintf(s, "%08ld", (long int) fdb_file_id);
 ret = s;
 return (ret);
}
//-----------------------------------------------------------------------
int open_for_write()
{
 return 1;
}
//-----------------------------------------------------------------------
string i_to_a(int64_t v)
{
 char s[64];
 string ret;

 sprintf(s,"%lld",v);
 ret = s;
 return (ret);
}
//-----------------------------------------------------------------------
string timet_to_a(const time_t * tt)
{
 string ret;
 char s[512];
 struct tm loc_time; struct tm * pt = &loc_time;

 pt = localtime(tt);
 sprintf(s,"%04d-%02d-%02d %02d:%02d:%02d",
           pt->tm_year+1900,pt->tm_mon + 1,pt->tm_mday,
           pt->tm_hour,pt->tm_min,pt->tm_sec);

 ret = s;
 return (ret);
}
//-----------------------------------------------------------------------
string stat_to_a(const struct stat * stbuf)
{
 string ret;
 char s[512];
// sprintf(s,"mode=%u nlink=%u size=%lld blocks=%lld blksize=%ld at=%s mt=%s ct=%s", 
//            stbuf->st_mode, stbuf->st_nlink, stbuf->st_size,stbuf->st_blocks, stbuf->st_blksize, 
//            timet_to_a(&stbuf->st_atime).c_str(), 
//            timet_to_a(&stbuf->st_mtime).c_str(), timet_to_a(&stbuf->st_ctime).c_str());
// ret = s;
 sprintf(s,"st_dev=%lld st_ino=%ld st_rdev=%lu",stbuf->st_dev, stbuf->st_ino, stbuf->st_rdev);
 ret = (string)s;
 return (ret);
}
//-----------------------------------------------------------------------
void nnsleep(long nsec)
{
 timespec ts,ts2;
 ts.tv_sec  = 0;
 ts.tv_nsec = nsec;
 if (nanosleep(&ts, &ts2) !=0 )
  {nanosleep(&ts2,NULL);}
 return;
}
//-----------------------------------------------------------------------
