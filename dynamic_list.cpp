//---------------------------------------------------------------------------
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <functional>
#include <string.h>
#include "dynamic_list.h"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
std::string Upper(const std::string p)
{
  std::string ret = p;
  transform(ret.begin(), ret.end(), ret.begin(), toupper);
  return (ret);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
TBytea::TBytea():
 size(0), position(0)
{
 data     = 0;
}
//---------------------------------------------------------------------------
TBytea::TBytea(const int64_t new_size):size(0)
{
 set_size(new_size);
}
//---------------------------------------------------------------------------
TBytea::TBytea(const TBytea& orig):
 position(orig.position),
 size(orig.size)
{
 data = new unsigned char (size);
 memcpy(data, orig.data, size);
}
//---------------------------------------------------------------------------
TBytea::~TBytea()
{
 if (data != 0)
 {
  delete [] data;
 }
}
//---------------------------------------------------------------------------
int64_t TBytea::set_size(const int64_t new_size)
{
 unsigned char * new_data, * old_data;
 int64_t cp_size;
 
 if (new_size == 0)
 {
  free();
  return (0);
 }

 if (size > 0 && new_size == 0)
 {
  delete [] data;
  size = 0;
  position = 0;
  return (size);
 }

 if (size == 0 && new_size > 0)
 {
  data = new unsigned char (new_size);
  size = new_size;
 }
 else
 {
  new_data = new unsigned char (new_size);
  old_data = data;
  if (new_size > size) cp_size = size;
  else cp_size = new_size;
  memcpy(new_data, old_data, cp_size);
  data = new_data;
  delete [] old_data;
 }

 return (size);
}
//---------------------------------------------------------------------------
const int64_t TBytea::get_size(void) const
{
 return (size);
}
//---------------------------------------------------------------------------
int64_t TBytea::set_position(const int64_t new_pos)
{
 if (size <= new_pos)
  return (position = new_pos);
 else
  return (position = size);
}
//---------------------------------------------------------------------------
const int64_t TBytea::get_position(void) const
{
 return (position);
}
//---------------------------------------------------------------------------
void TBytea::clear(void)
{
 memcpy(data, NULL, size);
 return ;
}
//---------------------------------------------------------------------------
void TBytea::free(void)
{
 delete [] data;
 data     = NULL;
 size     = 0;
 position = 0;
 return ;
}
//---------------------------------------------------------------------------
// read from stream
int64_t TBytea::read(void * dst, const int64_t buf_size)
{
 int64_t ret = 0;

 if ((position + buf_size) > size) ret = size - position;
 else ret = buf_size;

 memcpy(dst, data, ret); 
 position += buf_size;  

 return (ret);
}
//---------------------------------------------------------------------------
// write to stream
int64_t TBytea::write(void * src, const int64_t buf_size)
{
 int64_t ret = 0;

 if (buf_size > (size - position)) ret = size - position;
 else ret = buf_size;

 memcpy(data, src, ret);
 position += buf_size;

 return (ret);
}
//---------------------------------------------------------------------------
int64_t TBytea::load_from_file(const std::string file_name)
{
 int64_t ret = 0, new_size = 0;
 std::ifstream fin;

 fin.open(file_name.c_str(), std::ios::binary);
 if (fin.fail())
 {
  std::cout << "Error open file " << file_name << std::endl;
  return(-1);
 }

 if (fin.seekg(0, std::ios::end))
 {
  new_size = fin.tellg();
 }
 else
 {
  // TODO: Error
 }

 set_size(new_size);

 fin.seekg(0);
 fin.read((char *)data, size);
 if (fin.fail())
 {
  std::cout << "Error read file " << file_name << std::endl;
  return(-1);
 }

 fin.close();

 position = 0;
 ret      = size;

 return (ret); 
}
//---------------------------------------------------------------------------
int64_t TBytea::save_to_file(const std::string file_name) const
{
 int64_t ret = 0;
 std::ofstream fout;

 fout.open(file_name.c_str(), std::ios::binary);
 if (fout.fail())
 {
  std::cout << "Error open file " << file_name << std::endl;
  return (-1);
 }

 fout.write((char *)data, size);

 fout.close();

 ret = size;
 return (ret);
}
//---------------------------------------------------------------------------
unsigned char * TBytea::operator[](const int64_t idx) const
{
 unsigned char * ret = NULL;

 if (idx > (size-1) || idx < 0)
 {
  // TODO : Error
 }
 else
 {
  ret = &data[idx];
 }

 return(ret);
}
//---------------------------------------------------------------------------
bool TBytea::operator == (const TBytea n)
{
 bool ret = false;

 if (n.size != size) return(ret);

 if (size == 0) return(true);

 for (int64_t i = 0; i < size; i++)
 {
  if (data[i] != n.data[i]) break;
 }
 return (ret);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
TDynamicRow::~TDynamicRow()
{
 std::list<TDynamicCell *>::iterator itr;
 for (itr = cell.begin(); itr != cell.end(); ++itr) { delete(*itr); }
 cell.clear();
}
//---------------------------------------------------------------------------
void TDynamicRow::from (const TDynamicRow * pr)
{
 std::list<TDynamicCell *> o(pr->cell);
 std::list<TDynamicCell *>::iterator itr;

 for (itr = cell.begin(); itr != cell.end(); ++itr) { delete(*itr); }
 cell.clear();

 for (itr  = o.begin(); itr != o.end(); ++itr)
  { cell.push_back((*itr)->make_new()); }
}
//---------------------------------------------------------------------------
TDynamicCell * TDynamicRow::get_cell(const int field_num)
{
 std::list<TDynamicCell *>::iterator itr;
 int c;

 if (field_num < 0)
 {
  throw TDynamicListLogicException ((std::string)" TDynamicRow::get_cell: field number -1 can't be found.");
 }

 for(itr = cell.begin(), c = 0; itr != cell.end(); ++itr, ++c)
 {
  if (c == field_num) return (*itr);
 }

 std::stringstream stm;
 stm << "TDynamicRow::get_cell: field number " << field_num << " not found.";
 throw TDynamicListLogicException (stm.str());

 return (NULL);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
/*TDynamicRow::TDynamicRow(const TDynamicRow &p)
{
 std::list<TDynamicCell *>::iterator itr;
}
//---------------------------------------------------------------------------
TDynamicRow TDynamicRow::operator=(const TDynamicRow p)
{

}
//---------------------------------------------------------------------------
TDynamicRow TDynamicRow::operator=(const TDynamicRow * p)
{

}*/
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
TDynamicList::TDynamicList():
is_eof(true), is_bof(true), current_index_num(-1),
user_data_after_scroll(NULL), user_data_after_insert(NULL),
user_data_after_edit(NULL), user_data_before_delete(NULL), user_data_after_delete(NULL)
{
 current_row = new TDynamicRow();
 state = Browse;
}
//---------------------------------------------------------------------------
TDynamicList::TDynamicList(const TDynamicList &old)
{
 // state = Browse;
 // current_index_num = -1;
 // TODO :
}
//---------------------------------------------------------------------------
TDynamicList::~TDynamicList()
{
 clear_row();
 clear_column();
 clear_index();
 delete(current_row);
}
//---------------------------------------------------------------------------
//void TDynamicList::init_column(void)
//{
// current_row
//}
//---------------------------------------------------------------------------
void TDynamicList::clear_column(void)
{
 std::list<TDynamicRow *>::iterator itr_row;
 std::vector<TDynamicColumn *>::iterator itr;

 for(itr_row = row.begin(); itr_row != row.end(); ++itr_row)
 {
  delete(*itr_row);
 }
 row.clear();

 for(itr = header.begin(); itr != header.end(); ++itr)
 {
  delete(*itr);
 }
 header.clear();

 is_eof = true; is_bof = true;
}
//---------------------------------------------------------------------------
void TDynamicList::add_column(const IBPP::SDT type, const std::string p_name)
{
 DLDT l_type;
 
 switch (type)
 {
  case IBPP::sdSmallint:
  {
   l_type = dlSmallint;
   break;
  }
  case IBPP::sdInteger:
  {
   l_type = dlInteger;
   break;
  }
  case IBPP::sdLargeint:
  {
   l_type = dlLargeint;
   break;
  }
  case IBPP::sdString:
  {
   l_type = dlString;
   break;
  }
  case IBPP::sdFloat:
  {
   l_type = dlFloat;
   break;
  }
  case IBPP::sdDouble:
  {
   l_type = dlDouble;
   break;
  }
  case IBPP::sdTimestamp:
  {
   l_type = dlTimestamp;
   break;
  }
  case IBPP::sdDate:
  {
   l_type = dlDate;
   break;
  }
  case IBPP::sdTime:
  {
   l_type = dlTime;
   break;
  }
  case IBPP::sdBlob:
  {
   l_type = dlBlob;
   break;
  }
  default: /*TODO: Exception*/ break;
 } // switch
 add_column(l_type, p_name);
}
//---------------------------------------------------------------------------
void TDynamicList::add_column(const DLDT type, const std::string p_name)
{
 TDynamicColumn * col   = NULL;
 TDynamicCell   * ncell = NULL;

 switch (type)
 {
  case dlSmallint:
  {
   col   = new TDynamicColumnInteger();
   ncell = new TDynamicCellType<int>();
   break;
  }
  case dlInteger:
  {
   col   = new TDynamicColumnInteger();
   ncell = new TDynamicCellType<int>();
   break;
  }
  case dlLargeint:
  {
   col   = new TDynamicColumnLargeInt();
   ncell = new TDynamicCellType<int64_t>();
   break;
  }
  case dlString:
  {
   col   = new TDynamicColumnString();
   ncell = new TDynamicCellType<std::string>();
   break;
  }
  case dlFloat:
  {
   col   = new TDynamicColumnFloat();
   ncell = new TDynamicCellType<float>();
   break;
  }
  case dlDouble:
  {
   col   = new TDynamicColumnDouble();
   ncell = new TDynamicCellType<double>();
   break;
  }
  case dlNumeric:
  {
   col   = new TDynamicColumnTNumeric();
   ncell = new TDynamicCellType<TNumeric>();
   break;
  }
  case dlTimestamp:
  {
   col   = new TDynamicColumnTimestamp();
   ncell = new TDynamicCellType<IBPP::Timestamp>();
   break;
  }
  case dlDate:
  {
   col   = new TDynamicColumnDate();
   ncell = new TDynamicCellType<IBPP::Date>();
   break;
  }
  case dlTime:
  {
   col   = new TDynamicColumnTime();
   ncell = new TDynamicCellType<IBPP::Time>();
   break;
  }
  case dlBlob:
  {
   col   = new TDynamicColumnBytea();
   ncell = new TDynamicCellType<TBytea>();
   break;
  }
  default: /*TODO: Exception*/ break;
 } // switch

 if (col != NULL)
 {
  col->current_row = current_row;
  col->pos         = header.size();
  col->name        = p_name;
  header.push_back(col);

  current_row->cell.push_back(ncell);  
 } 
}
//---------------------------------------------------------------------------
int TDynamicList::get_column_count(void) const
{
 return (header.size());
}
//---------------------------------------------------------------------------
int  TDynamicList::get_column_num(const std::string column_name) const
{
 std::vector<TDynamicColumn *>::iterator itr;
 std::vector<TDynamicColumn *> loc_h (header);  // ???
 int c; std::string name(Upper(column_name));
 for(itr = loc_h.begin(), c = 0; itr != loc_h.end(); ++itr, ++c)
 {
  if (Upper((*itr)->name) == name)
  {
   return (c);
  }
 }
 // throw Exception
 throw TDynamicListLogicException("TDynamicList::get_column_num column name " + column_name + " not found.");

 return (-1);
}
//---------------------------------------------------------------------------
std::string  TDynamicList::get_column_name(const int column_num) const
{

 if (column_num < header.size() )
 {
  return ((header[column_num])->name);
 }

 // throw Exception
 std::stringstream stm;
 stm << "TDynamicList::get_column_name column num " << column_num << " not found.";
 throw TDynamicListLogicException (stm.str());

 return ("");
}
//---------------------------------------------------------------------------
DLDT  TDynamicList::get_column_type(const std::string fname) const
{
 return(get_column_type(get_column_num(fname)));
}
//---------------------------------------------------------------------------
DLDT  TDynamicList::get_column_type(const int column_num) const
{

 if (column_num < header.size())
 {
  return ((header[column_num])->column_type);
 }

 // throw Exception
 std::stringstream stm;
 stm << "TDynamicList::get_column_type column num " << column_num << " not found.";
 throw TDynamicListLogicException (stm.str());

 return (dlInteger);
}
//---------------------------------------------------------------------------
void TDynamicList::first(void)
{
 if (!row.empty())
 {
  is_eof = false; is_bof = false;
  if (current_index_num < 0)
  {
   itr_row      = row.begin();
   current_row->from(*itr_row);
  }
  else
  {
   current_row->from((*itr_idx)->first());
   is_eof = (*itr_idx)->eof();
   is_bof = (*itr_idx)->bof();
  }
  after_scroll();
 } // if (!row.empty())
 else
 {
  is_eof = true; is_bof = true;
 }
 
}
//---------------------------------------------------------------------------
void TDynamicList::last(void)
{
 if (!row.empty())
 {
  is_eof = false; is_bof = false;
  if (current_index_num < 0)
  {
   itr_row      = row.end();
   itr_row--;
   current_row->from(*itr_row);
  }
  else
  {
   current_row->from((*itr_idx)->last());
   is_eof = (*itr_idx)->eof();
   is_bof = (*itr_idx)->bof();
  }
  after_scroll();
 } // if (!row.empty())
 else
 {
  is_eof = true; is_bof = true;
 }
}
//---------------------------------------------------------------------------
void TDynamicList::next(void)
{
 if (!row.empty())
 {
  is_bof = false; is_eof = false;
  if (current_index_num < 0)
  {
   itr_row++;
   if (itr_row == row.end()) {itr_row --; is_eof = true;}
   current_row->from(*itr_row);
  }
  else
  {
   current_row->from((*itr_idx)->next());
   is_eof = (*itr_idx)->eof();
   is_bof = (*itr_idx)->bof();
  }
  after_scroll();
 } // if (!row.empty())
 else
 {
  is_eof = true; is_bof = true;
 }
}
//---------------------------------------------------------------------------
void TDynamicList::prior(void)
{
 if (!row.empty())
 {
  is_bof = false; is_eof = false;
  if (current_index_num < 0)
  {
   if (itr_row != row.begin()) {itr_row--; is_bof = true;}
   current_row->from(*itr_row);
  }
  else
  {
   current_row->from((*itr_idx)->prior());
   is_eof = (*itr_idx)->eof();
   is_bof = (*itr_idx)->bof();
  }
  after_scroll();
 } // if (!row.empty())
 else
 {
  is_eof = true; is_bof = true;
 }
}
//---------------------------------------------------------------------------
void TDynamicList::move_by(const int num)
{
 int i;
// TODO: this func with index
 int count = row.size();
 if (count > 0)
 {
  is_bof = false; is_eof = false;
  if (current_index_num < 0)
  {
   //if (num <= count/2)
   {
    itr_row = row.begin();
    for(i=0; i<num; i++) itr_row++;
   }
  /* else
   {
    itr_row = row.end(); itr_row--;
    for(i=0; i < (count - num); i++) itr_row--;
   }*/
   current_row->from(*itr_row);
  }
  else
  {
   current_row->from((*itr_idx)->move_by(num));
  }
  after_scroll();
 } // if (count > 0)
 else
 {
  is_eof = true; is_bof = true;
 }

}
//---------------------------------------------------------------------------
bool TDynamicList::eof(void) const
{
 return (is_eof);
}
//---------------------------------------------------------------------------
bool TDynamicList::bof(void) const
{
 return (is_bof);
}
//---------------------------------------------------------------------------
TDynamicColumn * TDynamicList::field_by_name(const std::string field_name)
{
 std::vector<TDynamicColumn *>::iterator itr;
 std::string ufname (field_name);
 std::string u;

 transform(ufname.begin(), ufname.end(), ufname.begin(), toupper);

 for(itr = header.begin(); itr != header.end(); itr++)
 {
  u = (*itr)->name;
  transform(u.begin(), u.end(), u.begin(), toupper);

  if (u == ufname)
  {
   return (*itr);
   break;
  } // if
 } // for
 // throw Exception
 throw TDynamicListLogicException("TDynamicList::field_by_name field name " + field_name + " not found.");
 return (NULL);
}
//---------------------------------------------------------------------------
TDynamicColumn * TDynamicList::field_by_number(const int n)
{
 // нумерация с 0
  if (n < 0 || n > header.size() - 1)
 {
  // throw Exception
  std::stringstream stm;
  stm << "TDynamicList::field_by_number field number " << n << " is failed.";
  throw TDynamicListLogicException(stm.str());
  return (NULL);
 }

 return (header[n]);
}
//---------------------------------------------------------------------------
int  TDynamicList::get_row_count(void) const
{
 return(row.size());
}
//---------------------------------------------------------------------------
void TDynamicList::clear_row(void)
{
 std::list<TDynamicRow *>::iterator itr;
 for(itr = row.begin(); itr != row.end(); ++itr)
 { delete(*itr);}
 row.clear();
}
//---------------------------------------------------------------------------
void TDynamicList::clear(void)
{
 std::list<TDynamicCell *>::iterator itr;
 for(itr = current_row->cell.begin(); itr != current_row->cell.end(); ++itr)
 {
  (*itr)->clear();
 } // for
}
//---------------------------------------------------------------------------
void TDynamicList::append(void)
{
 clear();
 state = Append;
}
//---------------------------------------------------------------------------
void TDynamicList::edit(void)
{
 state = Edit;
}
//---------------------------------------------------------------------------
void TDynamicList::remove(void)
{
 state = Remove;
}
//---------------------------------------------------------------------------
void TDynamicList::post(void)
{
 std::list<TDynamicListIndex *>::iterator itr;

 switch(state)
 {
  case Edit:   {
                for (itr = idx.begin(); itr != idx.end(); ++itr)
                {
                 (*itr)->update(*itr_row, current_row);
                } 
                (*itr_row)->from(current_row);
                after_edit();
                 break;
               }
  case Append: {
                 TDynamicRow * n = new TDynamicRow(); 
                 n->from(current_row); 
                 row.push_back(n);
 
                 for (itr = idx.begin(); itr != idx.end(); ++itr)
                 {
                  (*itr)->add(n);
                 }
                 after_insert();
                 break;
               }
  case Remove: {
                 if (!before_delete()) break;
//std::cout << " 001" << std::endl;
                 for (itr = idx.begin(); itr != idx.end(); ++itr)
                 {
                  (*itr)->remove(*itr_row);
                 }
//std::cout << "  002" << std::endl;
                 itr_row = row.erase(itr_row);
                 if (itr_row == row.end()){is_eof = true;}
                 else
                  current_row->from(*itr_row);
//std::cout << "   003" << std::endl;
                 after_delete();
                 break;
                }
  default: break;
 } // switch

 state = Browse;
}
//---------------------------------------------------------------------------
void TDynamicList::cancel(void)
{
 state = Browse;
}
//---------------------------------------------------------------------------
bool TDynamicList::create_index(const std::string field_name)
{
 int field_num = -1;
 field_num = field_by_name(field_name)->pos;
 return (create_index(field_num));
}
//---------------------------------------------------------------------------
bool TDynamicList::create_index(const int field_num)
{
 TDynamicListIndex * new_idx = NULL;
 bool ret = false;

 switch (field_by_number(field_num)->column_type)
 {
  case dlSmallint:
  {
   new_idx = new TDynamicListIndexType<int>();
   break;
  }
  case dlInteger:
  {
   new_idx = new TDynamicListIndexType<int>();
   break;
  }
  case dlLargeint:
  {
   new_idx = new TDynamicListIndexType<int64_t>();
   break;
  }
  case dlString:
  {
   new_idx = new TDynamicListIndexType<std::string>();
   break;
  }
  case dlFloat:
  {
   new_idx = new TDynamicListIndexType<float>();
   break;
  }
  case dlDouble:
  {
   new_idx = new TDynamicListIndexType<double>();
   break;
  }
  case dlNumeric:
  {
   new_idx = new TDynamicListIndexType<TNumeric>();
   break;
  }
  case dlTimestamp:
  {
//   new_idx = new TDynamicListIndexType<IBPP::Timestamp>();
   break;
  }
  case dlDate:
  {
//   new_idx = new TDynamicListIndexType<IBPP::Date>();
   break;
  }
  case dlTime:
  {
//   new_idx = new TDynamicListIndexType<IBPP::Time>();
   break;
  }
//  case dlBlob:
//  {
//   break;
//  }
  default: /*TODO: Exception*/ break;
 } // switch

 if (new_idx != NULL)
 {
  new_idx -> index_name = field_by_number(field_num)->name; 
  new_idx -> field_num  = field_num;
  idx.push_back(new_idx);
  ret = true;
 }
 else
 {
  std::stringstream stm;
  stm << "unknown index type for field " << field_num;
  throw TDynamicListLogicException (stm.str()); 
 }

 rebuild_index(field_num);

 return (ret);
}
//---------------------------------------------------------------------------
void TDynamicList::use_index(const std::string key_name)
{
 int key_num = 0;
 std::list<TDynamicListIndex *>::iterator itr;
 for (itr = idx.begin(); itr != idx.end(); ++itr, ++key_num)
 {
  if ((*itr)->index_name == key_name)
  {
   use_index(key_num);
   return;
  }
 }

 std::stringstream stm;
 stm << "index " << key_name << " not found.";
 throw TDynamicListLogicException (stm.str()); 
}
//---------------------------------------------------------------------------
void TDynamicList::use_index(const int key_num)
{
 int num = 0;
 if (key_num > idx.size() || key_num < 0)
 {
  std::stringstream stm;
  stm << "TDynamicList::use_index: key_num " << key_num << " is more then index number " << idx.size() << ".";
  throw TDynamicListLogicException (stm.str());
 }

 current_index_num = key_num;

 for (itr_idx = idx.begin(); itr_idx != idx.end(); ++itr_idx, ++num)
 {
  if (num == key_num) break;
 }
 
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void TDynamicList::clear_index(void)
{
 std::list<TDynamicListIndex *>::iterator itr;
 for(itr = idx.begin(); itr != idx.end(); ++itr)
 {
  delete(*itr);
 }
 idx.clear();
 current_index_num = -1;
}
//---------------------------------------------------------------------------
void TDynamicList::remove_index(const std::string key_name)
{
 int key_num = 0;
 std::list<TDynamicListIndex *>::iterator itr;
 for (itr = idx.begin(); itr != idx.end(); ++itr, ++key_num)
 {
  if ((*itr)->index_name == key_name)
  {
   remove_index(key_num);
   return;
  }
 }

 std::stringstream stm;
 stm << "index " << key_name << " not found.";
 throw TDynamicListLogicException (stm.str());
}
//---------------------------------------------------------------------------
void TDynamicList::remove_index(const int key_num)
{
 std::list<TDynamicListIndex *>::iterator itr;
 int num = 0;
 if (key_num > idx.size() || key_num < 0)
 {
  // TODO: Error
 }
 for (itr = idx.begin(); itr != idx.end(); ++itr, ++num)
 {
  if (num == key_num)
  {
   delete(*itr);
   idx.erase(itr);
   break;
  }
 }

}
//---------------------------------------------------------------------------
void TDynamicList::rebuild_index(const std::string key_name)
{
 int key_num = 0;
 std::list<TDynamicListIndex *>::iterator itr;
 for (itr = idx.begin(); itr != idx.end(); ++itr, ++key_num)
 {
  if ((*itr)->index_name == key_name)
  {
   rebuild_index(key_num);
   return;
  }
 }

 std::stringstream stm;
 stm << "index " << key_name << " not found.";
 throw TDynamicListLogicException (stm.str());
}
//---------------------------------------------------------------------------
void TDynamicList::rebuild_index(const int key_num)
{
 std::list<TDynamicListIndex *>::iterator itr;
 std::list<TDynamicRow *>::iterator l_itr_row;
 int num = 0;

 if (key_num > idx.size() || key_num < 0)
 {
  // TODO: Error
 }
 for (itr = idx.begin(); itr != idx.end(); ++itr, ++num)
 {
  if (num == key_num)
  {
   break;
  }
 } // for (itr = idx.begin();


 (*itr)->clear_row();
 for(l_itr_row  = row.begin(); l_itr_row != row.end(); ++l_itr_row)
 {
  (*itr)->add(*l_itr_row); 
 } // for(l_itr_row  = row.begin(); 
 (*itr)->first();
}
//---------------------------------------------------------------------------
bool TDynamicList::locate(const std::string field_name, const TDynamicCell * p)
{
 bool ret = false;
 TDynamicRow *       l_row;
 TDynamicListIndex * l_idx;
 std::list<TDynamicListIndex *>::iterator itr;
 int field_num = -1;
 std::string u_field_name = Upper(field_name);

 for(int i=0; i < header.size(); i++)
 {
  if (Upper(header[i]->name) == u_field_name){field_num = i; break;}
 }
 if (field_num == -1)
 {
  std::stringstream stm;
  stm << "TDynamicList::locate: field " << field_name << " not found.";
  throw TDynamicListLogicException (stm.str()); 
 }

 if (!idx.empty())
 {
  for (itr = idx.begin(); itr != idx.end(); ++itr)
  {
   l_idx = *itr;
   if (Upper(l_idx->index_name) == u_field_name)
   {
    l_row = l_idx->locate(p);
    if (l_row != NULL)
    {
     current_row->from(l_row);
     ret = true;
     // если использовался не текущий индекс, нужно установить текущий курсор
     if (l_idx->field_num != current_index_num && current_index_num >= 0)
     {
      // значение ключевого поля текущего индекса для искомой строки
      TDynamicCell * key_cell = l_row->get_cell(current_index_num);
      TDynamicRow  * ci_row   = (*itr_idx)->locate(key_cell);
      while(ci_row != l_row) // еслю индекс не уникальный - ищем строку
      {
       ci_row = (*itr_idx)->next();
       // защита от зацикливания, которого не может быть, ну вдруг...
       if ( !key_cell->touch(ci_row->get_cell(current_index_num)) )
       {
        // TODO : exception
        std::cout << "TDynamicList::locate unknown error" << std::endl;
        return(false);
       }
      }
     } // if (l_idx->field_num != current_index_num && current_index_num >= 0)
     else
     {
      
     }
    } // if (l_row != NULL)
    else
    { return (ret); }
   } // if (l_idx->index_name == field_name)
  } // for (itr = idx.begin();
 } // if (!idx.emty())
 else // full scan
 {
  std::list<TDynamicRow *>::iterator itr;
  for (itr = row.begin(); itr != row.end(); ++itr)
  {
   if ((*itr)->get_cell(field_num) == p)
   {
    itr_row = itr;
    current_row->from(*itr_row);
    ret = true;
    break;
   }
  } // for (itr = row.begin(); itr != row.end(); ++itr)
  ret = false;
 } // else
 return(ret);
}
//---------------------------------------------------------------------------
bool TDynamicList::locate(const std::string field_name, const int v)
{
 TDynamicCellType<int> p(v);
 return(locate(field_name, &p));
}
//---------------------------------------------------------------------------
bool TDynamicList::locate(const std::string field_name, const int64_t v)
{
 TDynamicCellType<int64_t> p(v);
 return(locate(field_name, &p));
}
//---------------------------------------------------------------------------
void TDynamicList::receive_from_relation(const int command, void * param)
{

}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
