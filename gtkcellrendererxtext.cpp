//#include <iostream>

#include "gtkcellrendererxtext.h"

/* This is based mainly on GtkCellRendererXText
 *  in GAIM, written and (c) 2002 by Sean Egan
 *  (Licensed under the GPL), which in turn is
 *  based on Gtk's GtkCellRenderer[Text|Toggle|Pixbuf]
 *  implementation by Jonathan Blandford */

/* Some boring function declarations: GObject type system stuff */

static void     gtk_cell_renderer_xtext_init       (GtkCellRendererXText      *cellxtext);

static void     gtk_cell_renderer_xtext_class_init (GtkCellRendererXTextClass *klass);

static void     gtk_cell_renderer_xtext_get_property  (GObject                    *object,
                                                             guint                       param_id,
                                                             GValue                     *value,
                                                             GParamSpec                 *pspec);

static void     gtk_cell_renderer_xtext_set_property  (GObject                    *object,
                                                             guint                       param_id,
                                                             const GValue               *value,
                                                             GParamSpec                 *pspec);

static void     gtk_cell_renderer_xtext_finalize (GObject *gobject);


/* These functions are the heart of our custom cell renderer: */

static void     gtk_cell_renderer_xtext_get_size   (GtkCellRenderer            *cell,
                                                          GtkWidget                  *widget,
                                                          GdkRectangle               *cell_area,
                                                          gint                       *x_offset,
                                                          gint                       *y_offset,
                                                          gint                       *width,
                                                          gint                       *height);

static void     gtk_cell_renderer_xtext_render     (GtkCellRenderer            *cell,
                                                          GdkWindow                  *window,
                                                          GtkWidget                  *widget,
                                                          GdkRectangle               *background_area,
                                                          GdkRectangle               *cell_area,
                                                          GdkRectangle               *expose_area,
                                                          GtkCellRendererState        flags);


enum
{
  PROP_HOR_LINE = 1,
  PROP_VER_LINE = 2,
  PROP_NUMBER_OF_LINE = 3,
};

static   gpointer  parent_class    = NULL;
static   prender   parent_render   = NULL;
static   pget_size parent_get_size = NULL;

G_DEFINE_TYPE (GtkCellRendererXText, gtk_cell_renderer_xtext, GTK_TYPE_CELL_RENDERER_TEXT);

/***************************************************************************
 *
 *  gtk_cell_renderer_xtext_get_type: here we register our type with
 *                                          the GObject type system if we
 *                                          haven't done so yet. Everything
 *                                          else is done in the callbacks.
 *
 ***************************************************************************/

/*GType
gtk_cell_renderer_xtext_get_type (void)
{
  static GType cell_xtext_type = 0;

  if (cell_xtext_type == 0)
  {
    static const GTypeInfo cell_xtext_info =
    {
      sizeof (GtkCellRendererXTextClass),
      NULL,                                                     // base_init
      NULL,                                                     // base_finalize
      (GClassInitFunc) gtk_cell_renderer_xtext_class_init,
      NULL,                                                     // class_finalize
      NULL,                                                     // class_data
      sizeof (GtkCellRendererXText),
      0,                                                        // n_preallocs
      (GInstanceInitFunc) gtk_cell_renderer_xtext_init,
    };

    // Derive from GtkCellRenderer 
    cell_xtext_type = g_type_register_static (GTK_TYPE_CELL_RENDERER,
                                              "GtkCellRendererXText",
                                              &cell_xtext_info,
                                              G_TYPE_FLAG_VALUE_ABSTRACT);
  }

  return cell_xtext_type;
}*/

/***************************************************************************
 *
 *  gtk_cell_renderer_xtext_init: set some default properties of the
 *                                      parent (GtkCellRenderer).
 *
 ***************************************************************************/

static void
gtk_cell_renderer_xtext_init (GtkCellRendererXText *cellrendererxtext)
{
  GTK_CELL_RENDERER(cellrendererxtext)->mode = GTK_CELL_RENDERER_MODE_INERT;
  GTK_CELL_RENDERER(cellrendererxtext)->xpad = 2;
  GTK_CELL_RENDERER(cellrendererxtext)->ypad = 2;

  cellrendererxtext -> number_of_line        = 1;
  cellrendererxtext -> wrap_width            = 0;
//  cellrendererxtext->text_buffer = gtk_text_buffer_new(NULL);
//  cellrendererxtext->text_view   = gtk_text_view_new_with_buffer(cellrendererxtext->text_buffer);

//  gtk_container_add(GTK_CONTAINER(cellrendererxtext), cellrendererxtext->text_view);
}


/***************************************************************************
 *
 *  gtk_cell_renderer_xtext_class_init:
 *
 *  set up our own get_property and set_property functions, and
 *  override the parent's functions that we need to implement.
 *  And make our new "percentage" property known to the type system.
 *  If you want cells that can be activated on their own (ie. not
 *  just the whole row selected) or cells that are editable, you
 *  will need to override 'activate' and 'start_editing' as well.
 *
 ***************************************************************************/

static void
gtk_cell_renderer_xtext_class_init (GtkCellRendererXTextClass *klass)
{
  GtkCellRendererClass *cell_class   = GTK_CELL_RENDERER_CLASS(klass);
  GObjectClass         *object_class = G_OBJECT_CLASS(klass);

  parent_class           = g_type_class_peek_parent (klass);

//  parent_cell_class      = object_class->parent_class;

  object_class->finalize = gtk_cell_renderer_xtext_finalize;

  /* Hook up functions to set and get our
   *   custom cell renderer properties */
  object_class->get_property = gtk_cell_renderer_xtext_get_property;
  object_class->set_property = gtk_cell_renderer_xtext_set_property;

  /* Override the two crucial functions that are the heart
   *   of a cell renderer in the parent class */

  parent_get_size = cell_class->get_size;
  cell_class->get_size = gtk_cell_renderer_xtext_get_size;

  parent_render = cell_class->render;
  cell_class->render   = gtk_cell_renderer_xtext_render;

  /* Install our very own properties */
  g_object_class_install_property (object_class,
                                   PROP_HOR_LINE,
                                   g_param_spec_boolean ("hor_line","hor_line",
                                                         "The to display",
                                                         FALSE,
                                                         (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE)));

  g_object_class_install_property (object_class,
                                   PROP_VER_LINE,
                                   g_param_spec_boolean ("ver_line","ver_line",
                                                         "The to display",
                                                         FALSE,
                                                         (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE)));

  g_object_class_install_property (object_class,
                                   PROP_NUMBER_OF_LINE,
                                   g_param_spec_char ("number_of_line","number_of_line",
                                                         "The to display",
                                                         1, 10, 1,
                                                         (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE)));

//(* G_OBJECT_CLASS (parent_class)->set_property)
//       (G_OBJECT(&(cellxtext->parent)), param_id, value, pspec);
//  GTK_TYPE_CELL_RENDERER.

}


/***************************************************************************
 *
 *  gtk_cell_renderer_xtext_finalize: free any resources here
 *
 ***************************************************************************/

static void
gtk_cell_renderer_xtext_finalize (GObject *object)
{
/*
  GtkCellRendererXText *cellrendererxtext = GTK_CELL_RENDERER_XTEXT(object);
*/

  /* Free any dynamically allocated resources here */

  (* G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/***************************************************************************
 *
 *  gtk_cell_renderer_xtext_get_property: as it says
 *
 ***************************************************************************/

static void
gtk_cell_renderer_xtext_get_property (GObject    *object,
                                      guint       param_id,
                                      GValue     *value,
                                      GParamSpec *psec)
{
  GtkCellRendererXText  *cellxtext = GTK_CELL_RENDERER_XTEXT(object);

  switch (param_id)
  {
    case PROP_HOR_LINE:
      g_value_set_boolean(value, cellxtext->draw_hor_line);
      break;
    case PROP_VER_LINE:
      g_value_set_boolean(value, cellxtext->draw_ver_line);
      break;
    case PROP_NUMBER_OF_LINE:
      g_value_set_char(value, cellxtext->number_of_line);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, psec);
      break;
  }
}


/***************************************************************************
 *
 *  gtk_cell_renderer_xtext_set_property: as it says
 *
 ***************************************************************************/

static void
gtk_cell_renderer_xtext_set_property (GObject      *object,
                                      guint         param_id,
                                      const GValue *value,
                                      GParamSpec   *pspec)
{
  GtkCellRendererXText *cellxtext = GTK_CELL_RENDERER_XTEXT (object);

  switch (param_id)
  {
    case PROP_HOR_LINE:
      cellxtext->draw_hor_line = g_value_get_boolean(value);
      break;
    case PROP_VER_LINE:
      cellxtext->draw_ver_line = g_value_get_boolean(value);
      break;
    case PROP_NUMBER_OF_LINE:
      cellxtext->number_of_line = g_value_get_char(value);
      break;
    default:
//       (* G_OBJECT_CLASS (parent_class)->set_property)
//       (G_OBJECT(&(cellxtext->parent)), param_id, value, pspec);
 //    (* G_OBJECT_CLASS (parent_class)->finalize) (object);
 //     gtk_cell_renderer_text_set_property(cellxtext->parent, param_id, value, pspec);
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, param_id, pspec);
      break;
  }
}

/***************************************************************************
 *
 *  gtk_cell_renderer_xtext_new: return a new cell renderer instance
 *
 ***************************************************************************/

GtkCellRenderer *
gtk_cell_renderer_xtext_new (void)
{
  return ((GtkCellRenderer *)g_object_new(GTK_TYPE_CELL_RENDERER_XTEXT, NULL));
}


/***************************************************************************
 *
 *  gtk_cell_renderer_xtext_get_size: crucial - calculate the size
 *                                          of our cell, taking into account
 *                                          padding and alignment properties
 *                                          of parent.
 *
 ***************************************************************************/

static void
gtk_cell_renderer_xtext_get_size (GtkCellRenderer *cell,
                                  GtkWidget       *widget,
                                  GdkRectangle    *cell_area,
                                  gint            *x_offset,
                                  gint            *y_offset,
                                  gint            *width,
                                  gint            *height)
{
 (parent_get_size)(cell, widget, cell_area, x_offset, y_offset, width, height);

 if (height != NULL) 
  {(*height) *= GTK_CELL_RENDERER_XTEXT(cell)->number_of_line;
   cell->height =  (*height);
  }

 if (cell_area != NULL) { (cell_area->height) *= GTK_CELL_RENDERER_XTEXT(cell)->number_of_line;}

}

/*
static void
add_attr (PangoAttrList  *attr_list,
          PangoAttribute *attr)
{
  attr->start_index = 0;
  attr->end_index = G_MAXINT;

  pango_attr_list_insert (attr_list, attr);
}
*/

/***************************************************************************
 *
 *  gtk_cell_renderer_xtext_render: crucial - do the rendering.
 *
 ***************************************************************************/

static void
gtk_cell_renderer_xtext_render (GtkCellRenderer *cell,
                                GdkWindow       *window,
                                GtkWidget       *widget,           // TreeView
                                GdkRectangle    *background_area,
                                GdkRectangle    *cell_area,
                                GdkRectangle    *expose_area,
                                GtkCellRendererState     flags)
{
  GtkCellRendererXText *cellxtext = GTK_CELL_RENDERER_XTEXT (cell);
  GtkCellRendererText *celltext  = GTK_CELL_RENDERER_TEXT (cell);
  GtkStateType                state;
  gint                        width, height;
  gint                        x_offset, y_offset;

  if (cell_area != NULL)
  {
   if (cellxtext -> wrap_width != cell_area->width)
   {
    cellxtext -> wrap_width = cell_area->width;
    g_object_set (G_OBJECT (cell), "wrap-mode", PANGO_WRAP_CHAR, "wrap-width", cellxtext -> wrap_width,  NULL);
   }
  }

  (parent_render)(cell, window, widget, background_area, cell_area, expose_area, flags);
/*
//if (expose_area!=NULL){printf("expose_area->width=%d expose_area->height=%d\n",expose_area->width, expose_area->height);}
//if (cell_area != NULL){printf("cell_area->width=%d cell_area->height=%d\n",cell_area->width, cell_area->height);}

  PangoAttrList *attr_list;
  PangoLayout *layout = gtk_widget_create_pango_layout (widget, celltext->text);
  
  pango_layout_set_wrap(layout, PANGO_WRAP_WORD);
//  pango_layout_set_ellipsize(layout, PANGO_ELLIPSIZE_END);

  attr_list = pango_attr_list_new ();

  add_attr (attr_list, pango_attr_font_desc_new (celltext->font));

  pango_layout_set_attributes (layout, attr_list);

  pango_attr_list_unref (attr_list);
 
  gtk_paint_layout (widget->style,
                    window,
                    state,
                    TRUE,
                    expose_area,
                    widget,
                    "cellrenderertext",
                    cell_area->x + x_offset + cell->xpad,
                    cell_area->y + y_offset + cell->ypad,
                    layout);
*/
 /*gtk_paint_layout (widget->style,
                    window,
                    state,
                    TRUE,
                    expose_area,
                    widget,
                    "cellrenderertext",
                    cell_area->x + x_offset + cell->xpad,
                    cell_area->y + y_offset + cell->ypad + cell_area->height/2,
                    layout);
*/
//  g_object_unref (layout);
  

  if (GTK_WIDGET_HAS_FOCUS (widget))
    state = GTK_STATE_ACTIVE;
  else
    state = GTK_STATE_NORMAL;

//  width  -= cell->xpad*2;
//  height -= cell->ypad*2;

/*  gtk_paint_box (widget->style,
                 window,
                 GTK_STATE_NORMAL, GTK_SHADOW_IN,
                 NULL, widget, "trough",
                 background_area->x ,
                 background_area->y ,
                 background_area->width , background_area->height );
*/
//  gdk_draw_rectangle()


/*  gtk_paint_box (widget->style,
                 window,
                 GTK_STATE_NORMAL, GTK_SHADOW_IN,
                 NULL, widget, "trough",
                 cell_area->x + x_offset + cell->xpad,
                 cell_area->y + y_offset + cell->ypad,
                 width - 1, height - 1);
*/
/*  gtk_paint_box (widget->style,
                 window,
                 state, GTK_SHADOW_OUT,
                 NULL, widget, "bar",
                 cell_area->x + x_offset + cell->xpad,
                 cell_area->y + y_offset + cell->ypad,
                 width,
                 height - 1);
*/

//  int border;
//  g_object_get(G_OBJECT(cellxtext), "border-width", &border, NULL);

  if (cellxtext->draw_hor_line)
  {
     gdk_draw_line (window, *(widget->style->dark_gc),
                   cell_area->x - cell->xpad,       cell_area->y + cell_area->height + cell->ypad -1,
                   cell_area->x + cell_area->width, cell_area->y + cell_area->height + cell->ypad -1);
  } //  if (cellxtext->draw_hor_line)

  if (cellxtext->draw_ver_line)
  {
     gdk_draw_line (window, *(widget->style->dark_gc),
                   cell_area->x - cell->xpad -1, cell_area->y - cell->ypad,
                   cell_area->x - cell->xpad -1, cell_area->y + cell_area->height + cell->ypad -1);
  } //  if (cellxtext->draw_ver_line)


}

