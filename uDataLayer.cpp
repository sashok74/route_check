//---------------------------------------------------------------------------
#include <gtk/gtk.h>

#include "uDataLayer.h"
#include "service.h"
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// ---------------------  TDataLayer ----------------------------------------
//---------------------------------------------------------------------------
TDataLayer::TDataLayer(IBPP::Database p_db)
{
  db = p_db;
  try
  {
   q = NULL;
   q = new TIBPPQuery(db, "cp1251", "utf8");
  }
  catch(...)
  {errorstr = "Ошибка выделения памяти ibppquery.";}
// log("TDataLayer::TDataLayer\n");
  pthread_mutex_init(&mutex, NULL);
}
//---------------------------------------------------------------------------
TDataLayer::~TDataLayer(void)
{
//  log("TDataLayer::~TDataLayer\n");
  if (q != NULL) delete(q);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//Список нарядов
int TDataLayer::RtRouteListSMem (list<RPackRtItem> * rs, int label_num)
{
 int ret(-1);
 errorstr.clear();
 std::string sql, s;
 RPackRtItem rec;
 IBPP::Date t1, t2;

 sql = "select pack_route_id, pack_id, rt_item_id, cnt_in, cnt_out, cnt_defect, date_b, date_e, ";
 sql+= "employee_id, descript, oper_price, str_id, str_name, cash, pack_name, emp_name, doc_id, ";
 sql+= "bom_id, oper_time, time_sec, time_str_w, time_str_r, rt_time_w, rt_time_r, label_id, ";
 sql+= "label_num, oper_num, priority ";
 sql+= "from rt_route_list_s(:label_id, :label_num, :pack_id_in, :nom_id_in, :str_id_in, ";
 sql+= "                     :employee_id_in, :date_b_in, :date_e_in, :all_items) ";

 pthread_mutex_lock(&mutex);
 
 t1.Today(); t2.Today();
 t1.StartOfMonth(); t2.EndOfMonth();

 q->SetNewQuery(sql, IBPP::amRead);
 q->SetParam("label_id",  0);
 q->SetParam("label_num", label_num);
 q->SetParam("pack_id_in",0);
 q->SetParam("nom_id_in", 0);
 q->SetParam("str_id_in", 0);
 q->SetParam("employee_id_in", 0);
 q->SetParam("date_b_in", t1);
 q->SetParam("date_e_in", t2);
 q->SetParam("all_items", 0); // bool

//  QueryMemLoad (mtList);
 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  log("Error: TDataLayer::RtRouteListSMem - " + errorstr + q->ParamValueStr());
 }
 else
 {
  rs->clear();
  ret = 0;
  while (q->Fetch())
  {
   ret++;
   rec.SetDefault();
   q->GetValue("pack_route_id", rec.pack_route_id);
   q->GetValue("pack_id",       rec.pack_id);
   q->GetValue("rt_item_id",    rec.rt_item_id); 
   q->GetValue("cnt_in",        rec.cnt_in);
   q->GetValue("cnt_out",       rec.cnt_out);
   q->GetValue("cnt_defect",    rec.cnt_defect);
   if (!q->IsNull("date_b")) {q->GetValue("date_b", rec.date_b);}
   if (!q->IsNull("date_e")) {q->GetValue("date_e", rec.date_e);}
   q->GetValue("employee_id",   rec.employee_id);
   q->GetValue("descript",      rec.descript);
   q->GetValue("oper_price",    rec.oper_price);
   q->GetValue("str_id",        rec.str_id);
   q->GetValue("str_name",      rec.str_name);
   q->GetValue("cash",          rec.cash);
   q->GetValue("pack_name",     rec.pack_name); 
   q->GetValue("emp_name",      rec.employee_name);
   q->GetValue("doc_id",        rec.doc_id);
   q->GetValue("bom_id",        rec.bom_id);
   q->GetValue("oper_time",     rec.oper_time);
   q->GetValue("time_sec",      rec.time_sec);
   q->GetValue("time_str_w",    rec.time_str_w);
   q->GetValue("time_str_r",    rec.time_str_r);
   q->GetValue("rt_time_w",     rec.rt_time_w);
   q->GetValue("rt_time_r",     rec.rt_time_r);
   q->GetValue("label_id",      rec.label_id);
   q->GetValue("label_num",     rec.label_num);
   q->GetValue("oper_num",      rec.oper_num);
   q->GetValue("priority",      rec.priority);
   rs->push_back(rec);
  }
  errorstr = q->errorstr;
 
  if (errorstr.length() > 0)
  { 
   log("RtRouteListSMem: " + errorstr + q->ParamValueStr());
   ret = -1;
  }
  q->commit();
 } // else

 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
int TDataLayer::PackRtItemS(list<RPackRtItem> * rs, int pack_route_id)
{
 int ret(-1);
 RPackRtItem rec;
 std::string sql, s;

 errorstr.clear();

 sql = "select cnt_in, cnt_out, date_b, date_e, employee_id, emp_name ";
 sql+= "from nom_pack_route_sel(:pack_route_id) ";

 pthread_mutex_lock(&mutex);

 q->SetNewQuery(sql, IBPP::amRead);
 q->SetParam("pack_route_id", pack_route_id);

 if (!q->exec(st))
 {
  ret      = -1;
  errorstr = q->errorstr;
  log("Error: TDataLayer::PackRtItemS - " + errorstr + q->ParamValueStr());
 }
 else
 {
  rs->clear();
  try
  {
   while (st->Fetch())
   {
    ret++;
    rec.SetDefault();
    st->Get("Cnt_in",  rec.cnt_in);
    st->Get("Cnt_out", rec.cnt_out);
    st->Get("Date_b", rec.date_b);
    st->Get("Date_e", rec.date_e);
    st->Get("Employee_id", rec.employee_id);
    st->Get("emp_name", s); rec.employee_name = cp2koi(s, 1);
    rs->push_back(rec);
   }
  } // try
  catch(IBPP::Exception& e)
  {
   errorstr = cp2koi(e.what(),1);
   ret = -1;
   pthread_mutex_unlock(&mutex);
   return (ret);
  } // catch

  q->commit();
 }
 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
int TDataLayer::PackRtItemIU(RPackRtItem * rec)
{
 int ret;
 std::string sql;

 errorstr.clear();

 sql = "select res_id ";
 sql+= "from nom_pack_route_iu(:pack_route_id, :label_id, :pack_id, :rt_item_id, :cnt_in, :cnt_out, ";
 sql+= "               :cnt_defect, :date_b, :date_e, :employee_id, :descript, :oper_price, :str_id, ";
 sql+= "               :oper_time, :oper_num, :cash) ";

 pthread_mutex_lock(&mutex);
 q->SetNewQuery(sql, IBPP::amWrite);
 q->SetParam("pack_route_id", rec->pack_route_id);
 q->SetParam("label_id",      rec->label_id);
 q->SetParam("pack_id",       rec->pack_id);
 q->SetParam("rt_item_id",    rec->rt_item_id);
 q->SetParam("cnt_in",        rec->cnt_in);
 q->SetParam("cnt_out",       rec->cnt_out);
 q->SetParam("cnt_defect",    rec->cnt_defect);
 if(!(rec->date_b.Year() == 1970 && rec->date_b.Month() == 1 && rec->date_b.Day() == 1)) {q->SetParam("date_b", rec->date_b);}
 else {q->SetParamNull("date_b");}
 if(!(rec->date_e.Year() == 1970 && rec->date_e.Month() == 1 && rec->date_e.Day() == 1)) {q->SetParam("date_e", rec->date_e);}
 else {q->SetParamNull("date_e");}
 q->SetParam("employee_id",   rec->employee_id);
// q->SetParam("descript",      cp2koi(rec->descript, 2));
 q->SetParam("descript",      rec->descript);
 q->SetParam("oper_price",    rec->oper_price);
 q->SetParam("str_id",        rec->str_id);
 q->SetParam("oper_time",     rec->oper_time);
 q->SetParam("oper_num",      rec->oper_num);
 q->SetParam("cash",          rec->cash);

//log("employee_id=%d\n",rec->employee_id);

 if (q->exec(st))
 {
  try
  {
   while(st->Fetch())
   {
     st->Get("res_id", ret);
   }
  } // try
  catch (IBPP::Exception& e)
  {
   errorstr = cp2koi(e.what(),1);
   ret      = -1;
   log("Error: TDataLayer::PackRtItemIU - " + errorstr + q->ParamValueStr());
   pthread_mutex_unlock(&mutex);
   return (ret);
  }

  q->commit();
 }
 else
 {
  ret                      = -1;
  SQLException_SqlCode     = q->SQLException_SqlCode;
  SQLException_EngineCode  = q->SQLException_EngineCode;
  errorstr                 = q->errorstr;
  log("Error: TDataLayer::PackRtItemIU: " + errorstr + q->ParamValueStr());
 }

 if (ret>0) rec->pack_route_id = ret;

//log("TDataLayer::PackRtItemIU ret = %d\n", ret);
 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
int TDataLayer::EmpEmployeesInfBySnS(int64_t sn, TEmployee &prm)
{
 int ret(-1);
 std::string s, sql;

 errorstr.clear();

 sql = "select * from emp_employees_inf_by_sn_s(:sn)";

 pthread_mutex_lock(&mutex);

 q->SetNewQuery(sql, IBPP::amRead);
 q->SetParam("sn", sn);

 if (q->exec(st))
 {
  try
  {
   while(st->Fetch())
   {
    st->Get("id", prm.id);
    st->Get("first_name", s); prm.first_name  = cp2koi(s, 1);
    st->Get("middle_name",s); prm.middle_name = cp2koi(s, 1);
    st->Get("last_name", s); prm.last_name    = cp2koi(s, 1);
    st->Get("firm_id", prm.firm_id);
    st->Get("car_number",s);  prm.car_number  = cp2koi(s, 1);
    st->Get("permissions", prm.permissions);
    st->Get("work_begin", prm.work_begin);
    st->Get("work_end",  prm.work_end);
    st->Get("staff_id", prm.staff_id);
    st->Get("employee_id", prm.employee_id);
    st->Get("prof_id", prm.prof_id);
    st->Get("category", prm.category);
    st->Get("prof_name", s); prm.prof_name   = cp2koi(s, 1);
    st->Get("emp_name", s); prm.emp_name     = cp2koi(s, 1);
    st->Get("str_id", prm.str_id);
    st->Get("str_name", s); prm.str_name     = cp2koi(s, 1);
    st->Get("cur_time", prm.cur_time);
   } // while
  } // try
  catch (IBPP::Exception& e)
  {
   errorstr = cp2koi(e.what(),1);
   ret      = -1;
   log("Error TDataLayer::EmpEmployeesInfBySnS: - " + errorstr + q->ParamValueStr());
   pthread_mutex_unlock(&mutex);
   return (ret);
  }

  q->commit();
  ret = 1;
 }
 else
 {
  ret      = -1;
  errorstr = (string) q->errorstr.c_str();
 }
 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
/*int TDataLayer::NomPackRouteLabelSMem(list<RNomPackRouteLabel> * rs, int label_num)
{
 int ret = -1;
 RNomPackRouteLabel rec;
 std::string sql;

 sql = "select rl.number, rl.nom_id, rl.pack_id, rl.doc_id, rl.doc_number, ";
 sql +="rl.pack_route_id, rl.start_rt_item_id, rl.stop_rt_item_id, rl.cnt, ";
 sql +="rl.cnt_in, rl.time_b, rl.time_e, rl.prev_label_id, rl.prev_label_number,";
 sql +="rl.printed, rl.user_id, rl.date_create, rl.nom_name, rl.nom_sheet, ";
 sql +="rl.user_name, rl.emp_name, rl.str_id, rl.str_name, rl.status, ";
 sql +="rl.doc_registed, rl.priority, rl.archiv, rl.first_label_id, ";
 sql +="rl.first_label_number ";
 sql +="from nom_pack_route_label_s(:NUMBER_IN, 0, 0, 0, 0, 0, 0) rl ";

 pthread_mutex_lock(&mutex);

 q->SetNewQuery(sql, IBPP::amRead);
 q->SetParam("NUMBER_IN", label_num);

 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  log("error %s\n",  errorstr.c_str());
 }
 else
 {
  rs->clear();
  ret = 0;
  while (q->fetch())
  {
   ret++;
   rec.clear();

   q->GetValue("number", rec.number);
   q->GetValue("nom_id", rec.nom_id);
   q->GetValue("pack_id", rec.pack_id);
   q->GetValue("doc_id", rec.doc_id);
   q->GetValue("doc_number", rec.doc_number);
   q->GetValue("pack_route_id", rec.pack_route_id);
   q->GetValue("start_rt_item_id", rec.start_rt_item_id);
   q->GetValue("stop_rt_item_id", rec.stop_rt_item_id);
   q->GetValue("cnt", rec.cnt);
   q->GetValue("cnt_in", rec.cnt_in);
   if (!q->IsNull("time_b")) {q->GetValue("time_b", rec.time_b);} 
   if (!q->IsNull("time_e")) {q->GetValue("time_e", rec.time_e);}
   q->GetValue("prev_label_id", rec.prev_label_id);
   q->GetValue("prev_label_number", rec.prev_label_number);
   q->GetValue("printed", rec.printed);
   q->GetValue("user_id", rec.user_id);
   if (!q->IsNull("date_create")) {q->GetValue("date_create", rec.date_create);}
   q->GetValue("nom_name", rec.nom_name);
   q->GetValue("nom_sheet", rec.nom_sheet);
   q->GetValue("user_name", rec.user_name);
   q->GetValue("emp_name", rec.emp_name);
   q->GetValue("str_id", rec.str_id);
   q->GetValue("str_name", rec.str_name);
   q->GetValue("status", rec.status);
   q->GetValue("doc_registed", rec.doc_registed);
   q->GetValue("priority", rec.priority);
   q->GetValue("archiv", rec.archiv);
   q->GetValue("first_label_id", rec.first_label_id);
   q->GetValue("first_label_number", rec.first_label_number);

   rs->push_back(rec);
//log("2\n");
  }
  errorstr = q->errorstr;

  if (errorstr.length() > 0)
  {
   log("NomPackRouteLabelSMem: %s\n", errorstr.c_str());
   ret = -1;
  }
  q->commit();
 } // else


 pthread_mutex_unlock(&mutex);
 return (ret);
}*/
//---------------------------------------------------------------------------
int TDataLayer::NomPackRouteLabelSMem(TDynamicList * mt, const int label_num)
{
 int ret(-1);
 std::string sql;

 errorstr.clear();

 sql = "select rl.number, rl.nom_id, rl.pack_id, rl.doc_id, rl.doc_number, ";
 sql +="rl.pack_route_id, rl.start_rt_item_id, rl.stop_rt_item_id, rl.cnt, ";
 sql +="rl.cnt_in, rl.time_b, rl.time_e, rl.prev_label_id, rl.prev_label_number,";
 sql +="rl.printed, rl.user_id, rl.date_create, rl.nom_name, rl.nom_sheet, ";
 sql +="rl.user_name, rl.emp_name, rl.str_id, rl.str_name, rl.status, ";
 sql +="rl.doc_registed, rl.priority, rl.archiv, rl.first_label_id, ";
 sql +="rl.first_label_number ";
 sql +="from nom_pack_route_label2_s(:NUMBER_IN, 0, 0, 0, 0, 0, 0, NULL, NULL) rl ";

 pthread_mutex_lock(&mutex);

 q->SetNewQuery(sql, IBPP::amRead);
 q->SetParam("NUMBER_IN", label_num);

 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  log("Error: TDataLayer::NomPackRouteLabelSMem - " + errorstr + q->ParamValueStr());
 }
 else
 {
  ret = q->st_to_mt(mt);
  if (errorstr.length() > 0)
  {
   log("Error: NomPackRouteLabelSMem - " + errorstr + q->ParamValueStr());
   ret = -1;
  }
  q->commit();
 } // else
 
 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
int TDataLayer::SnToRouteI(const int pack_route_id, const std::string& sn_text)
{
 int ret(0);
 std::string sql;

 errorstr.clear();

 sql = "select res_id from sn_to_route_i(:pack_route_id, :sn_text) ";

 pthread_mutex_lock(&mutex);
 q->SetNewQuery(sql, IBPP::amWrite);
 q->SetParam("pack_route_id", pack_route_id);
 q->SetParam("sn_text",       sn_text);
 if (q->exec(st))
 {
  try
  {
   while(st->Fetch())
   {
     st->Get("res_id", ret);
   }
  } // try
  catch (IBPP::Exception& e)
  {
   errorstr = cp2koi(e.what(),1);
   ret      = -1;
   log("Error: TDataLayer::PackRtItemIU - " + errorstr + q->ParamValueStr());
   pthread_mutex_unlock(&mutex);
   return (ret);
  }

  q->commit();
 }
 else
 {
  ret                      = -1;
  SQLException_SqlCode     = q->SQLException_SqlCode;
  SQLException_EngineCode  = q->SQLException_EngineCode;
  errorstr                 = q->errorstr;
  log("Error: TDataLayer::SnToRouteI: " + errorstr + q->ParamValueStr());
 }

 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
int TDataLayer::SnToRouteSString(const int pack_route_id, std::list<std::string> * Items)
{
 int ret(0);
 std::string sql;

 errorstr.clear();

 sql = "select sn_text from sn_to_route_s(:pack_route_id) ";

 pthread_mutex_lock(&mutex);

 q->SetNewQuery(sql, IBPP::amRead);
 q->SetParam("pack_route_id", pack_route_id);

 if (!q->exec(st))
 {
  ret      = -1;
  errorstr = q->errorstr;
  log("Error: TDataLayer::NomPackRouteLabelSMem - " + errorstr + q->ParamValueStr());
 }
 else
 {
  std::string sn_text;
  Items->clear();
  try
  {
   while(st->Fetch())
   {
    sn_text.clear();
    st->Get("sn_text", sn_text);
    Items->push_back(cp2koi(sn_text, 1));
    ret++;
   }
  } // try
  catch (IBPP::Exception& e)
  {
   errorstr = cp2koi(e.what(), 1);
   ret      = -1;
   log("Error: TDataLayer::SnToRouteSString - " + errorstr + q->ParamValueStr());
   pthread_mutex_unlock(&mutex);
   return (ret);
  }

  q->commit();
 } // else

 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
int TDataLayer::DefPackDefectsS(TDynamicList * mt, const int defect_type_list_id, const int pack_id, const int label_id)
{
 int ret(-1);
 std::string sql;

 errorstr.clear();

 sql = "select * from DEF_PACK_DEFECTS_S(:DEFECT_TYPE_LIST_ID, :PACK_ID, :LABEL_ID, NULL, NULL) ";

 pthread_mutex_lock(&mutex);

 q->SetNewQuery(sql, IBPP::amRead);
 q->SetParam("DEFECT_TYPE_LIST_ID", defect_type_list_id);
 q->SetParam("PACK_ID", pack_id);
 q->SetParam("LABEL_ID", label_id);

 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  log("Error: TDataLayer::DefPackDefectsS - " + errorstr + q->ParamValueStr());
 }
 else
 {
  ret = q->st_to_mt(mt);
  if (errorstr.length() > 0)
  {
   log("Error: DefPackDefectsS - " + errorstr + q->ParamValueStr());
   ret = -1;
  }
  q->commit();
 } // else

 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
int TDataLayer::DefPackDefectsIU(const int pack_defects_id, const int pack_id, const int label_id, const int defect_list_id,
                     const float defect_count, const std::string defect_descript)
{
 int ret(0);
 std::string sql;

 errorstr.clear();

 sql = "select RES_ID ";
 sql+= "from DEF_PACK_DEFECTS_IU(:PACK_DEFECTS_ID, NULL /*NOM_ID*/, :PACK_ID, :LABEL_ID, NULL, ";
 sql+= "                         :DEFECT_LIST_ID, :DEFECT_COUNT, :DEFECT_DESCRIPT) ";

 pthread_mutex_lock(&mutex);
 q->SetNewQuery(sql, IBPP::amWrite);
 q->SetParam("PACK_DEFECTS_ID", pack_defects_id);
 q->SetParam("PACK_ID",         pack_id);
 q->SetParam("LABEL_ID",        label_id);
 q->SetParam("DEFECT_LIST_ID",  defect_list_id);
 q->SetParam("DEFECT_COUNT",    (double)defect_count);
 q->SetParam("DEFECT_DESCRIPT", defect_descript);

 if (q->exec(st))
 {
  try
  {
   if(st->Fetch())
   {
     st->Get("res_id", ret);
   }
  } // try
  catch (IBPP::Exception& e)
  {
   errorstr = cp2koi(e.what(),1);
   ret      = -1;
   log("Error: TDataLayer::DefPackDefectsIU - " + errorstr + q->ParamValueStr());
   pthread_mutex_unlock(&mutex);
   return (ret);
  }
  q->commit();
 }
 else
 {
  ret                      = -1;
  SQLException_SqlCode     = q->SQLException_SqlCode;
  SQLException_EngineCode  = q->SQLException_EngineCode;
  errorstr                 = q->errorstr;
  log("Error: TDataLayer::DefPackDefectsIU: " + errorstr + q->ParamValueStr());
 }

 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
int TDataLayer::DefDefectTypeListS(TDynamicList * mt)
{
 int ret (-1);
 std::string sql;

 errorstr.clear();

 sql = "select * from DEF_DEFECT_TYPE_LIST_S ";

 pthread_mutex_lock(&mutex);

 q->SetNewQuery(sql, IBPP::amRead);

 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  log("Error: TDataLayer::DefDefectTypeListS - " + errorstr + q->ParamValueStr());
 }
 else
 {
  ret = q->st_to_mt(mt);
  if (errorstr.length() > 0)
  {
   log("Error: DefDefectTypeListS - " + errorstr + q->ParamValueStr());
   ret = -1;
  }
  q->commit();
 } // else

 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
