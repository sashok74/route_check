//
//
//

#include <gtk/gtk.h>
#include <ibpp/ibpp.h>
#include <string>

#include "uLoginfrm.h"
#include "service.h"
//---------------------------------------------------------------------------
/*void LoginfrmShow(TRefresh2 p_refresh, int p_type, IBPP::Database * db)
{
 std::string dbhost="192.168.1.206", dbport="3050", dbname="planar_i", dblogin="sysdba", dbpass="", dbrole="";

 Tloginfrmptr win = new Tloginfrm(p_refresh, p_type, db);
 win->build();
 gtk_entry_set_text(GTK_ENTRY(win->edbserver), dbhost.c_str());
 gtk_entry_set_text(GTK_ENTRY(win->edbport),  dbport.c_str());
 gtk_entry_set_text(GTK_ENTRY(win->edbname), dbname.c_str());
 gtk_entry_set_text(GTK_ENTRY(win->edbrole), dbrole.c_str());
 gtk_entry_set_text(GTK_ENTRY(win->edblogin), dblogin.c_str());
 gtk_entry_set_text(GTK_ENTRY(win->edbpass), dbpass.c_str());

 win->show();
}*/
//---------------------------------------------------------------------------
void LoginfrmShow(TRefresh2 p_refresh , int p_type, IBPP::Database * db, std::string dbhost, std::string dbport, std::string dbname, std::string dbrole, std::string dbuser, std::string dbpassword)
{

 Tloginfrmptr win = new Tloginfrm(p_refresh, p_type, db);
 win->dbhost = dbhost;
 win->dbport = dbport;
 win->dbname = dbname;
 win->dbrole = dbrole;
 win->dbuser = dbuser;
 win->dbpassword = dbpassword;

 if (dbpassword.length() > 0)
 {
  win->connect();
  delete(win);
 }
 else
 {
  win->build();
  win->show();
 }

}
//---------------------------------------------------------------------------
static void loginfrm_evnok(GtkWidget *widget, gpointer data)
{
  Tloginfrmptr self = NULL;
  self = (Tloginfrmptr)data;

  log((std::string)"Try connect:");
  log((std::string)"server: " + (std::string) gtk_entry_get_text(GTK_ENTRY(self->edbserver)));
  log((std::string)"port: "   + (std::string) gtk_entry_get_text(GTK_ENTRY(self->edbport)));
  log((std::string)"dbname: " + (std::string) gtk_entry_get_text(GTK_ENTRY(self->edbname)));
  log((std::string)"dbrole: " + (std::string) gtk_entry_get_text(GTK_ENTRY(self->edbrole)));
  log((std::string)"dblogin: "+ (std::string) gtk_entry_get_text(GTK_ENTRY(self->edblogin)));
//  log(("dbpass: %s\n", gtk_entry_get_text(GTK_ENTRY(self->edbpass)));

  self->dbhost    = gtk_entry_get_text(GTK_ENTRY(self->edbserver));
  self->dbport    = gtk_entry_get_text(GTK_ENTRY(self->edbport));
  self->dbname    = gtk_entry_get_text(GTK_ENTRY(self->edbname));
  self->dbrole    = gtk_entry_get_text(GTK_ENTRY(self->edbrole));
  self->dbuser    = gtk_entry_get_text(GTK_ENTRY(self->edblogin));
  self->dbpassword= gtk_entry_get_text(GTK_ENTRY(self->edbpass));


/*  try
  {
   std::string hostport = gtk_entry_get_text(GTK_ENTRY(self->edbserver));
   hostport += ("/" + (std::string) gtk_entry_get_text(GTK_ENTRY(self->edbport)));

  *self->db = IBPP::DatabaseFactory(hostport.c_str(),
                               gtk_entry_get_text(GTK_ENTRY(self->edbname)),
                               gtk_entry_get_text(GTK_ENTRY(self->edblogin)),
                               gtk_entry_get_text(GTK_ENTRY(self->edbpass)),
                               gtk_entry_get_text(GTK_ENTRY(self->edbrole)),
                               "WIN1251", "");
   (*self->db)->Connect();
   if ((*self->db)->Connected())
   {
    log("Connect successfull !\n");
   }
  }
  catch(IBPP::Exception& e)
  {
   log("Connect failed !\n%s", e.what());
  }
*/
  self->connect();
// loginfrm_destroy(NULL, data);
  gtk_widget_destroy(self->window);
}
//---------------------------------------------------------------------------
static void loginfrm_evncancel(GtkWidget *widget, gpointer data)
{
//  log(("Cancel\n");
  gtk_main_quit ();
}
//---------------------------------------------------------------------------
static gboolean loginfrm_delete_event(GtkWidget *widget, GdkEvent  *event, gpointer data)
{
    /* If you return FALSE in the "delete_event" signal handler,
     * GTK will emit the "destroy" signal. Returning TRUE means
     * you don't want the window to be destroyed.
     * This is useful for popping up 'are you sure you want to quit?'
     * type dialogs. */

//    log(("delete event occurred\n");

    /* Change TRUE to FALSE and the main window will be destroyed with
     * a "delete_event". */

    return TRUE;
}
//---------------------------------------------------------------------------
static void loginfrm_destroy(GtkWidget *widget, gpointer data)
{
 Tloginfrmptr self = NULL;

// log("loginfrm_destroy\n");

 if (data != NULL)
 {
  self = (Tloginfrmptr)data;
//  gtk_widget_destroy(self->window);
//  log("loginfrm close\n");
//  close(self->window);
  delete(self);
 }
// gtk_main_quit ();      // replace to close window
}
//---------------------------------------------------------------------------
static void loginfrm_entry_activate(GtkWidget *widget, gpointer data)
{
 Tloginfrmptr self = NULL;
 self = (Tloginfrmptr)data;

 if (gtk_widget_is_focus(self->edbserver))
 {
  gtk_widget_grab_focus(self->edbport);
  return;
 } 
 if (gtk_widget_is_focus(self->edbport))
 {
  gtk_widget_grab_focus(self->edbname);
  return;
 }
 if (gtk_widget_is_focus(self->edbname))
 {
  gtk_widget_grab_focus(self->edbrole);
  return;
 }
 if (gtk_widget_is_focus(self->edbrole))
 {
  gtk_widget_grab_focus(self->edblogin);
  return;
 }
 if (gtk_widget_is_focus(self->edblogin))
 {
  gtk_widget_grab_focus(self->edbpass);
  return;
 }
 if (gtk_widget_is_focus(self->edbpass))
 {
  gtk_widget_grab_focus(self->bok);
  return;
 }

}
//---------------------------------------------------------------------------
Tloginfrm::Tloginfrm(TRefresh2 p_refresh, int ptype, IBPP::Database * p_db):
 pix_sec(NULL)
{
 prefresh = p_refresh;
 p_type   = ptype;
 db       = p_db;
}
//---------------------------------------------------------------------------
Tloginfrm::~Tloginfrm(void)
{
 int ret = 0;
 if (prefresh != NULL)
 {
  if ((*db)->Connected())
  {
   ret = 1;
  }
  prefresh(p_type, ret);
 }
}
//---------------------------------------------------------------------------
void Tloginfrm::build(void)
{
// extern const guint8 secwin[];
 extern const guint8 *psecwin; 
 extern const guint8 *pkey_16;
// extern unsigned int ssecwin;
// extern guint8 test_v;
// test_v = 10;

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window),"Login");
    gtk_window_set_position(GTK_WINDOW(window),GTK_WIN_POS_CENTER);

    icon = gdk_pixbuf_new_from_inline (-1, pkey_16, FALSE, NULL);
    if (icon != NULL)
    {
     // TODO: replace to icon list
     gtk_window_set_icon(GTK_WINDOW(window), icon);
    }
 
    g_signal_connect (G_OBJECT (window), "delete_event",
		      G_CALLBACK (loginfrm_delete_event), (gpointer)this);
    
    g_signal_connect (G_OBJECT (window), "destroy",
		      G_CALLBACK (loginfrm_destroy), (gpointer)this);
    
    /* Sets the border width of the window. */
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);
  
    hbox0   = gtk_hbox_new(FALSE, 2);
    gtk_container_add (GTK_CONTAINER (window), hbox0);


    pix_sec = gdk_pixbuf_new_from_inline (-1, psecwin, FALSE, NULL);
    if (pix_sec != NULL)
    {
     img_sec = gtk_image_new_from_pixbuf(pix_sec);
     gtk_box_pack_start(GTK_BOX(hbox0), img_sec, TRUE, TRUE, 0);
    }
    else
    {
     log("pix_sec == NULL");
    }

    wtable = gtk_table_new(7, 2, FALSE);
//    gtk_container_add (GTK_CONTAINER (window), wtable);
    gtk_box_pack_start(GTK_BOX(hbox0), wtable, TRUE, TRUE, 0);


    ldbserver = gtk_label_new_with_mnemonic("server:");
    edbserver = gtk_entry_new();
    g_signal_connect (G_OBJECT (edbserver), "activate",
                      G_CALLBACK (loginfrm_entry_activate), (gpointer)this);
    ldbport   = gtk_label_new_with_mnemonic("port:");
    edbport   = gtk_entry_new();
    g_signal_connect (G_OBJECT (edbport), "activate",
                      G_CALLBACK (loginfrm_entry_activate), (gpointer)this);
    ldbname   = gtk_label_new_with_mnemonic("name:");
    edbname   = gtk_entry_new();
    g_signal_connect (G_OBJECT (edbname), "activate",
                      G_CALLBACK (loginfrm_entry_activate), (gpointer)this);
    ldbrole   = gtk_label_new_with_mnemonic("role:");
    edbrole   = gtk_entry_new();
    g_signal_connect (G_OBJECT (edbrole), "activate",
                      G_CALLBACK (loginfrm_entry_activate), (gpointer)this);
    ldblogin  = gtk_label_new_with_mnemonic("login:");
    edblogin  = gtk_entry_new();
    g_signal_connect (G_OBJECT (edblogin), "activate",
                      G_CALLBACK (loginfrm_entry_activate), (gpointer)this);
    ldbpass   = gtk_label_new_with_mnemonic("password:");
    edbpass   = gtk_entry_new();
    g_signal_connect (G_OBJECT (edbpass), "activate",
                      G_CALLBACK (loginfrm_entry_activate), (gpointer)this);
    gtk_entry_set_visibility(GTK_ENTRY(edbpass), FALSE);
    gtk_entry_set_invisible_char(GTK_ENTRY(edbpass), g_utf8_get_char("*"));

    gtk_table_attach(GTK_TABLE(wtable), ldbserver, 0, 1, 0, 1, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_table_attach(GTK_TABLE(wtable), edbserver, 1, 2, 0, 1, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_table_attach(GTK_TABLE(wtable), ldbport,   0, 1, 1, 2, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_table_attach(GTK_TABLE(wtable), edbport,   1, 2, 1, 2, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_table_attach(GTK_TABLE(wtable), ldbname,   0, 1, 2, 3, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_table_attach(GTK_TABLE(wtable), edbname,   1, 2, 2, 3, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_table_attach(GTK_TABLE(wtable), ldbrole,   0, 1, 3, 4, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_table_attach(GTK_TABLE(wtable), edbrole,   1, 2, 3, 4, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_table_attach(GTK_TABLE(wtable), ldblogin,  0, 1, 4, 5, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_table_attach(GTK_TABLE(wtable), edblogin,  1, 2, 4, 5, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_table_attach(GTK_TABLE(wtable), ldbpass,   0, 1, 5, 6, GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_table_attach(GTK_TABLE(wtable), edbpass,   1, 2, 5, 6, GTK_SHRINK, GTK_SHRINK, 0, 0);

    hbox    = gtk_hbox_new(FALSE, 3);

    gtk_table_attach(GTK_TABLE(wtable), hbox,      1, 3, 6, 7, GTK_SHRINK, GTK_SHRINK, 0, 2);

    bok     = gtk_button_new_with_label ("OK");
    bcancel = gtk_button_new_with_label ("Cancel");

    gtk_box_pack_end(GTK_BOX(hbox), bcancel, TRUE, TRUE, 0);
    gtk_box_pack_end(GTK_BOX(hbox), bok, TRUE, TRUE, 0);

    g_signal_connect (G_OBJECT (bok), "clicked",
                      G_CALLBACK (loginfrm_evnok), (gpointer)this);

//    log("%d\n", self->edbserver);

//    g_signal_connect_swapped (G_OBJECT (bok), "clicked",
//                              G_CALLBACK (gtk_widget_destroy),
//                              G_OBJECT (window));

    g_signal_connect (G_OBJECT (bcancel), "clicked",
                      G_CALLBACK (loginfrm_evncancel), (gpointer)this);

//    g_signal_connect_swapped (G_OBJECT (bcancel), "clicked",
//                              G_CALLBACK (gtk_widget_destroy),
//                              G_OBJECT (window));
 gtk_entry_set_text(GTK_ENTRY(edbserver),dbhost.c_str());
 gtk_entry_set_text(GTK_ENTRY(edbport),  dbport.c_str());
 gtk_entry_set_text(GTK_ENTRY(edbname),  dbname.c_str());
 gtk_entry_set_text(GTK_ENTRY(edbrole),  dbrole.c_str());
 gtk_entry_set_text(GTK_ENTRY(edblogin), dbuser.c_str());
 gtk_entry_set_text(GTK_ENTRY(edbpass),  dbpassword.c_str());

}
//---------------------------------------------------------------------------
void Tloginfrm::show(void)
{
 gtk_widget_show_all (window); 
}
//---------------------------------------------------------------------------
char Tloginfrm::connect(void)
{
 char ret = 0;

 try
 {
   std::string hostport = dbhost;
   hostport += ("/" + dbport);

   *db = IBPP::DatabaseFactory(hostport.c_str(),
                               dbname.c_str(),
                               dbuser.c_str(),
                               dbpassword.c_str(),
                               dbrole.c_str(),
                               "WIN1251", "");
   (*db)->Connect();
   if ((*db)->Connected())
   {
    log("Connect successfull !");
    ret = 1;
   }
 }
 catch(IBPP::Exception& e)
 {
   log((std::string)"Connect failed !" + (std::string)e.what());
   ret = 0;
 }

 return (ret);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
