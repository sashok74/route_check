//---------------------------------------------------------------------------
#pragma hdrstop       

#include <errno.h>
#include <string.h>
#include <iostream>

#include <iconv.h>

#include "ibppquery.h"

#if defined __BCPLUSPLUS__
#include <Dialogs.hpp>
#include <DateUtils.hpp>
#endif

//---------------------------------------------------------------------------
//                        TStatementEx
//---------------------------------------------------------------------------
/*void ITStatementEx::set_enc(const std::string p_db_enc, const std::string p_cl_enc)
{
 db_enc = p_db_enc;
 cl_enc = p_cl_enc;
}
//---------------------------------------------------------------------------
#if defined IBPP_LINUX
void ITStatementEx::Set(int n, const std::string& value)
{
 //return (IBPP::IStatement::Set(n, value));
}
//---------------------------------------------------------------------------
bool ITStatementEx::Get(int n, std::string& value)
{
 std::string v;
 bool ret;
// ret = IBPP::IStatement::Get(n, v);
 value = v;
// return (ret);
 return (true);
}
//---------------------------------------------------------------------------
bool ITStatementEx::Get(const std::string& par_name, std::string& value)
{
 bool ret;
 std::string name, v;
 name  = par_name;
 v     = value;
// ret   = IBPP::IStatement::Get(name, v);
 value = v;
 return (true);
}
//---------------------------------------------------------------------------
#endif

#if defined __BCPLUSPLUS__
IBPP::Timestamp ITStatementEx::TDateTime2Timestamp(TDateTime value)
{
 IBPP::Timestamp ret;
 unsigned short year, month, day, hour, min, sec, msec;

 value.DecodeDate(&year,&month,&day);
 value.DecodeTime(&hour,&min,&sec,&msec);

 ret.SetDate(year, month, day);
 ret.SetTime(hour, min, sec, msec*10);

 return (ret);
}
//---------------------------------------------------------------------------
TDateTime       ITStatementEx::Timestamp2TDateTime(IBPP::Timestamp value)
{
  return (EncodeDateTime(value.Year(), value.Month(), value.Day(), value.Hours(),
                         value.Minutes(), value.Seconds(), value.SubSeconds()/10));
}
//---------------------------------------------------------------------------
void ITStatementEx::Set(int n, const TDateTime& value)
{
 IBPP::Timestamp ts;
 ts = TDateTime2Timestamp(value);
 return (IBPP::IStatement::Set(n, ts));
}
//---------------------------------------------------------------------------
void ITStatementEx::SetDate(int n, const TDateTime& value)
{
 IBPP::Date date;
 unsigned short year, month, day;
 value.DecodeDate(&year,&month,&day);
 date.SetDate(year, month, day);
 return (IBPP::IStatement::Set(n, date));
}
//---------------------------------------------------------------------------
void ITStatementEx::SetTime(int n, const TDateTime& value)
{
 IBPP::Time time;
 unsigned short hour, min, sec, msec;
 value.DecodeTime(&hour,&min,&sec,&msec);
 time.SetTime(hour, min, sec, msec*10);
 return (IBPP::IStatement::Set(n, time));
}
//---------------------------------------------------------------------------
bool ITStatementEx::Get(int n, IBPP::Timestamp& value)
{
 bool ret;
 IBPP::Timestamp ts;
 ret =  IBPP::IStatement::Get(n, value);
 return (ret);
}
//---------------------------------------------------------------------------
bool ITStatementEx::GetDate(int n, IBPP::Date& value)
{
 return (IBPP::IStatement::Get(n, value));
}
//---------------------------------------------------------------------------
bool ITStatementEx::GetTime(int n, IBPP::Time& value)
{
 return (IBPP::IStatement::Get(n, value));
}
//---------------------------------------------------------------------------
#endif
*/
//---------------------------------------------------------------------------
// ---------------------  TQParam ----------------------------------------
//---------------------------------------------------------------------------
bool TQParamSortPos(const TQParam& p1,const TQParam& p2)
{
  return (p1.pos < p2.pos);
}
//---------------------------------------------------------------------------
TQParam::TQParam(void): v_ts(2000,1,1),
  number    (0),             // номер параметра
  pos       (0),             // позиция параметра в оригинальном запросе
  isNull    (0),            // not Null
  ft        (IBPP::sdString),
  v_int     (0),
  v_int64   (0),
  v_float   (0)
{
 paramName.clear();
 v_str.clear();
//  v_ts.Clear();
}
//---------------------------------------------------------------------------
TQParam::TQParam(TQParam * p): v_ts(2000,1,1)
{
  number    = p->number;             // номер параметра
  pos       = p->pos;                // позиция параметра в оригинальном запросе
  paramName = p->paramName;       // Название параметра

  isNull    = p->isNull;
  ft        = p->ft;
  v_str     = p->v_str;       // значение
  v_int     = p->v_int;
  v_int64   = p->v_int64;
  v_float   = p->v_float;
  v_ts      = p->v_ts;
  v_date    = p->v_date;
  v_time    = p->v_time;
}
//---------------------------------------------------------------------------
void TQParam::SetString(const std::string p)
{
 ft    = IBPP::sdString;
 v_str = p;
 isNull= 0;
}
//---------------------------------------------------------------------------
void TQParam::SetInt(const int p)
{
 ft    = IBPP::sdInteger;
 v_int = p;
 isNull= 0;
}
//---------------------------------------------------------------------------
void TQParam::SetInt64(const int64_t p)
{
 ft      = IBPP::sdLargeint;
 v_int64 = p;
 isNull  = 0;
}
//---------------------------------------------------------------------------
void TQParam::SetFloat(const double p)
{
 ft      = IBPP::sdDouble;
 v_float = p;
 isNull= 0;
}
//---------------------------------------------------------------------------
void TQParam::SetDateTime(const IBPP::Timestamp p)
{
 ft    = IBPP::sdTimestamp;
 v_ts  = p;
 isNull= 0;
}
//---------------------------------------------------------------------------
void TQParam::SetDate(const IBPP::Date p)
{
 ft      = IBPP::sdDate;
 v_date  = p;
 isNull  = 0;
}
//---------------------------------------------------------------------------
void TQParam::SetTime(const IBPP::Time p)
{
 ft      = IBPP::sdTime;
 v_time  = p;
 isNull  = 0;
}
//---------------------------------------------------------------------------
void TQParam::SetNull(void)
{
 isNull = 1;
}
//---------------------------------------------------------------------------
const std::string TQParam::Value() const
{
 std::stringstream stm;
 if (isNull == 1)
 {
  stm << "isNull=true";
 }
 else
 {
  if (ft    == IBPP::sdString) stm << v_str;
  if (ft    == IBPP::sdInteger) stm << v_int;
  if (ft    == IBPP::sdLargeint) stm << v_int64;
  if (ft    == IBPP::sdDouble) stm << v_float;
  if (ft    == IBPP::sdTimestamp) stm << v_ts.Year() << "-" << v_ts.Month() << "-" << v_ts.Day() << " "
                                      << v_ts.Hours() << ":" << v_ts.Minutes() << ":" << v_ts.Seconds();
  if (ft    == IBPP::sdDate) stm << v_ts.Year() << "-" << v_ts.Month() << "-" << v_ts.Day();
  if (ft    == IBPP::sdTime) stm << v_ts.Hours() << ":" << v_ts.Minutes() << ":" << v_ts.Seconds();
 }
 return(stm.str());
}
//---------------------------------------------------------------------------
// ---------------------  TIBPPQuery ----------------------------------------
//---------------------------------------------------------------------------
TIBPPQuery::TIBPPQuery():
 am (IBPP::amWrite),
 db_enc (""),
 cl_enc ("")
{
 set_db(db);
 reset();
}
//---------------------------------------------------------------------------
TIBPPQuery::TIBPPQuery(IBPP::Database p_db):
 db (p_db),
 am (IBPP::amWrite),
 db_enc (""),
 cl_enc ("")
{
 reset();
}
//---------------------------------------------------------------------------
TIBPPQuery::TIBPPQuery(IBPP::Database p_db, const std::string p_db_enc, const std::string p_cl_enc):
 db     (p_db),
 am     (IBPP::amWrite),
 db_enc (p_db_enc),
 cl_enc (p_cl_enc)
{
 reset();
}
//---------------------------------------------------------------------------
TIBPPQuery::~TIBPPQuery(void)
{
 reset();
}
//---------------------------------------------------------------------------
// Очистить параметры и запрос
void TIBPPQuery::reset(void)
{
 numParams    = 0;
 u_sql        = "";
 p_sql        = "";
 errorstr     = "";
 SQLException_SqlCode    = 0;
 SQLException_EngineCode = 0;
 pqu.clear();
}
//---------------------------------------------------------------------------
std::string TIBPPQuery::Upper(const std::string p)
{
  std::string ret = p;
  transform(ret.begin(), ret.end(), ret.begin(), toupper);
  return (ret);
}
//---------------------------------------------------------------------------
// Замена параметров
void TIBPPQuery::parse(void)
{
// заменяем названия параметров в запросе на ?
// согласно заданным параметрам
 TQParam qparam;
 std::list<TQParam>::iterator itr;
 std::string paramName, sql;
 int x1;

 sql = u_sql;
// 1) найти все вхождения параметров в запрос
 for(itr = pqu.begin(); itr != pqu.end(); ++itr)
 {
  paramName = ":" + Upper(itr->paramName);
  {
   // TODO : много одинаковых параметров
   x1 = Upper(sql).find(paramName, 0);//          InString(sql.UpperCase(), paramName, 0);
   if (x1>0)
   {
    itr->pos = x1;
   } // if
  }
 } // for

// 2) отсортировать вхождения по порядку вхождения
 pqu.sort(TQParamSortPos);
// 3) заменить параметры на ?
 x1    = 0;
 p_sql = "";
 for(itr = pqu.begin(); itr != pqu.end(); ++itr)
 {
  p_sql += u_sql.substr(x1, itr->pos - x1);
  p_sql += "?";
  x1    = itr->pos + itr->paramName.length() + 1;
 } // for
 p_sql += u_sql.substr(x1, u_sql.length() - x1);
}
//---------------------------------------------------------------------------
void TIBPPQuery::SetQuery(const std::string sql)
{
 u_sql = sql;
}
//---------------------------------------------------------------------------
void TIBPPQuery::SetQuery(const std::string sql, IBPP::TAM p_am)
{
 u_sql = sql;
 am    = p_am;
}
//---------------------------------------------------------------------------
void TIBPPQuery::SetNewQuery(const std::string sql, IBPP::TAM p_am)
{
 reset();
 SetQuery(sql, p_am);
}
//---------------------------------------------------------------------------
void TIBPPQuery::SetParam(const std::string paramName, const std::string val)
{
// list<TQParam>::iterator itr;
 TQParam p;
// int n;
// bool append = true;

 // Перед добавлением проверить на уникальность
/* for(itr = pqu.begin(); itr != pqu.end(); ++itr)
 {
   if (Upper(itr->paramName) == Upper(paramName))
   {
    append = false;
    n      = itr->number;
    pqu.erase(itr);
    break;
   }
 }

 if (append)        // Уникальный
 {   */
  p.number    = numParams + 1;
  p.paramName = paramName;
  numParams++;
/* }
 else               // не Уникальный
 {
  p.number    = n;
  p.paramName = paramName;
 }  */
 if (db_enc.length() > 0 && cl_enc.length() > 0 && db_enc != cl_enc)
 {
  std::string val2;
  cp2koi(val, val2, 2);
  p.SetString(val2);
//std::cout<<"enc " << val << std::endl;
 }
 else
 {
  p.SetString(val);
//std::cout<<"NOT enc " << val << std::endl;
 }
 pqu.push_back(p);
}
//---------------------------------------------------------------------------
void TIBPPQuery::SetParam(const std::string paramName, const int val)
{
 TQParam p;

 p.number    = numParams + 1;
 p.paramName = paramName;
 numParams++;
 p.SetInt(val);

 try
 {
  pqu.push_back(p);
 }
 catch (IBPP::Exception& e)
 {
   //errorstr = e.what();
   //ret = false;
#if defined __BCPLUSPLUS__
   ShowMessage("Error set param int.");
#endif
 }
}
//---------------------------------------------------------------------------
void TIBPPQuery::SetParam(const std::string paramName, const int64_t val)
{
 TQParam p;

 p.number    = numParams + 1;
 p.paramName = paramName;
 numParams++;
 p.SetInt64(val);

 try
 {
  pqu.push_back(p);
 }
  catch (IBPP::Exception& e)
  {
   //errorstr = e.what();
   //ret = false;
#if defined __BCPLUSPLUS__
   ShowMessage("Error set param int64.");
#endif
  }
}
//---------------------------------------------------------------------------
void TIBPPQuery::SetParam(const std::string paramName, const double val)
{
 TQParam p;

 p.number    = numParams + 1;
 p.paramName = paramName;
 numParams++;
 p.SetFloat(val);
 pqu.push_back(p);
}
//---------------------------------------------------------------------------
void TIBPPQuery::SetParam(const std::string paramName, const IBPP::Timestamp val)
{
 TQParam p;

 p.number    = numParams + 1;
 p.paramName = paramName;
 numParams++;
 p.SetDateTime(val);
 pqu.push_back(p);
}
//---------------------------------------------------------------------------
void TIBPPQuery::SetParam(const std::string paramName, const IBPP::Date val)
{
 TQParam p;

 p.number    = numParams + 1;
 p.paramName = paramName;
 numParams++;
 p.SetDate(val);
 pqu.push_back(p);
}
//---------------------------------------------------------------------------
void TIBPPQuery::SetParam(const std::string paramName, const IBPP::Time val)
{
 TQParam p;

 p.number    = numParams + 1;
 p.paramName = paramName;
 numParams++;
 p.SetTime(val);
 pqu.push_back(p);
}
//---------------------------------------------------------------------------
void TIBPPQuery::SetParamNull(const std::string paramName)
{
 TQParam p;

 p.number    = numParams + 1;
 p.paramName = paramName;
 numParams++;
 p.SetNull();
 pqu.push_back(p);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Выполнение запроса
bool TIBPPQuery::exec(void)
{
 return (exec(st));
}
//---------------------------------------------------------------------------
// Выполнение запроса
bool TIBPPQuery::exec(IBPP::Statement& p_st)
{
 std::list<TQParam>::iterator itr;
 int parNum = 1;
 bool ret = true;

 parse();
 try
 {
  tr = IBPP::TransactionFactory(db, am, IBPP::ilReadCommitted, IBPP::lrNoWait);  // Start transaction for read
  tr->Start();
  st = IBPP::StatementFactory(db, tr);
  st->Prepare(p_sql);

  for(itr = pqu.begin(); itr != pqu.end(); ++itr)
  {
   if (itr->isNull != 1)
   {
    switch (itr->ft)
    {
     case IBPP::sdString:    st->Set(parNum, itr->v_str); break;
     case IBPP::sdInteger:   st->Set(parNum, itr->v_int); break;
     case IBPP::sdLargeint:  st->Set(parNum, itr->v_int64); break;
     case IBPP::sdDouble:    st->Set(parNum, itr->v_float); break;
     case IBPP::sdTimestamp: st->Set(parNum, itr->v_ts); break;
     case IBPP::sdDate:      st->Set(parNum, itr->v_date); break;
     case IBPP::sdTime:      st->Set(parNum, itr->v_time); break;
    }
   }
   else {st->SetNull(parNum);}
   parNum++;
  } // for

  st->Execute();

  p_st = st;
 }
 catch (IBPP::SQLException& e)
 {
  SQLException_SqlCode    = e.SqlCode();
  SQLException_EngineCode = e.EngineCode();
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }

 if (!ret)
 {
  try { tr->Rollback(); }
  catch (IBPP::Exception& e) { }
 }

 return (ret);
}
//---------------------------------------------------------------------------
// commit
bool TIBPPQuery::commit(void)
{
 bool ret = true;
 try
 {
  tr->Commit();
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }

 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::Fetch(void)
{
 bool ret = true;
 try
 {
  ret = st->Fetch();
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::IsNull(const std::string paramName)
{
 bool ret = false;
 try
 {
  ret = st->IsNull(paramName);
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
 }
 return (ret);
}
bool TIBPPQuery::IsNull(const int paramNumber)
{
 bool ret = false;
 try
 {
  ret = st->IsNull(paramNumber);
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const std::string paramName, std::string& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramName, val))) {val.clear();}
  else if (db_enc.length() > 0 && cl_enc.length() > 0 && db_enc != cl_enc)
  {
   cp2koi(val, val, 1);
  }
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }

 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const int paramNumber, std::string& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramNumber, val))) {val.clear();}
  else if (db_enc.length() > 0 && cl_enc.length() > 0 && db_enc != cl_enc)
  {
   cp2koi(val, val, 1);
  }
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }

 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const std::string paramName, int& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramName, val))) {val = 0;}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const int paramNumber, int& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramNumber, val))) {val = 0;}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const std::string paramName, int64_t& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramName, val))) {val = 0;}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const int paramNumber, int64_t& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramNumber, val))) {val = 0;}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const std::string paramName, float& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramName, val))) {val = 0;}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const int paramNumber, float& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramNumber, val))) {val = 0;}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const std::string paramName, double& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramName, val))) {val = 0;}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const int paramNumber, double& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramNumber, val))) {val = 0;}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const std::string paramName, IBPP::Timestamp& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramName, val))) {val.Clear();}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const int paramNumber, IBPP::Timestamp& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramNumber, val))) {val.Clear();}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const std::string paramName, IBPP::Date& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramName, val))) {val.Clear();}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const int paramNumber, IBPP::Date& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramNumber, val))) {val.Clear();}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const std::string paramName, IBPP::Time& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramName, val))) {val.Clear();}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
bool TIBPPQuery::GetValue (const int paramNumber, IBPP::Time& val)
{
 bool ret = true;
 try
 {
  if ((ret = st->Get(paramNumber, val))) {val.Clear();}
 }
 catch (IBPP::Exception& e)
 {
  if (db_enc.length()>0 && cl_enc.length()>0) {errorstr = cp2koi(e.what(), 1);} else {errorstr = e.what();}
  ret      = false;
 }
 return (ret);
}
//---------------------------------------------------------------------------
// 1 - db -> loc, 2 - loc -> db
int TIBPPQuery::cp2koi(const std::string & src, std::string & dst, const int direction) const
{
 iconv_t cd;
 size_t k, f, t;
 int se;

 if (src.length() == 0) {dst.clear(); return(0);}

 char * code = (char*) src.c_str();
 char * in   = code;
 // char buf[100];
 char * buf = new char[(src.length()+1)*2];
 char * out = buf;

 if (direction == 2) {cd = iconv_open(db_enc.c_str(), cl_enc.c_str());}
 else                {cd = iconv_open(cl_enc.c_str(), db_enc.c_str());}
 if( cd == (iconv_t)(-1) ) {std::cout << "Error iconv" << std::endl;}
  // err( 1, "iconv_open" );
 f = strlen(code);
 t = (src.length()+1) * 2;
 memset( buf, 0, t );
 errno = 0;

 k = iconv(cd, &in, &f, &out, &t);
 se = errno;

 dst = buf;

 delete [] buf;
 iconv_close(cd);

 return(1);
}
//---------------------------------------------------------------------------
std::string TIBPPQuery::cp2koi(const std::string & src, const int direction) const
{
 std::string res;
 cp2koi(src, res, direction);
 return(res);
}
//---------------------------------------------------------------------------
void TIBPPQuery::set_db(IBPP::Database pdb)
{
 db = pdb;
}
//---------------------------------------------------------------------------
void TIBPPQuery::set_enc(const std::string pdb_enc, const std::string pcl_enc)
{
 db_enc = pdb_enc;
 cl_enc = pcl_enc;
}
//---------------------------------------------------------------------------
const std::string TIBPPQuery::ParamValueStr()
{
 std::stringstream stm;

 for(std::list<TQParam>::iterator itr = pqu.begin(); itr != pqu.end(); ++itr)
 {
  if (!stm.str().empty()) stm << " ";
  stm << itr->paramName << "=" << itr->Value();
 }
 if (!stm.str().empty()) stm << std::endl;

 return(stm.str());
}
//---------------------------------------------------------------------------
//IBPP::SDT   TIBPPQuery::ColumnType(const int i)
//{
//}
//---------------------------------------------------------------------------
#if defined USE_DYNAMIC_LIST
int TIBPPQuery::st_to_mt(TDynamicList * mt, IBPP::Statement st)
{
 int ret = 0;
 int cols;

 if (mt == NULL) return (-1);

 try
 {
  mt->clear_column();
  cols = st->Columns();
 }
 catch (IBPP::Exception& e)
 {
  errorstr = e.what();
  return (-1);
 }
 catch(...)
 {
  //log("Error: TDataLayer::st_to_mt - unknown error\n");
  errorstr = "TDataLayer::st_to_mt - unknown error";
  return (-1);
 }

 //  Заголовки
 for(int i=1; i <= cols; i++)
 {
  std::string name;
  if(strlen(st->ColumnAlias(i))>0) {name = cp2koi(st->ColumnAlias(i),1);}
  else
  {
   if(strlen(st->ColumnName(i))>0) {name = cp2koi(st->ColumnName(i),1);}
   else {/*char s[128]; sprintf(s, "Column_%d", i); name = s;*/
         std::stringstream stm("Column_"); stm << std::dec << i; name = stm.str();
        }
  }

  if (st->ColumnType(i) == IBPP::sdDate || st->ColumnType(i) == IBPP::sdTime ||
      st->ColumnType(i) == IBPP::sdTimestamp || st->ColumnType(i)==IBPP::sdString ||
      st->ColumnType(i) == IBPP::sdSmallint || st->ColumnType(i) ==IBPP::sdInteger ||
      st->ColumnType(i) == IBPP::sdLargeint || st->ColumnType(i) == IBPP::sdFloat ||
      st->ColumnType(i) == IBPP::sdDouble || st->ColumnType(i) == IBPP::sdBlob)
  {
   try
   {
    if (st->ColumnType(i) == IBPP::sdLargeint && st->ColumnScale(i) > 0)   // TNumeric
    {mt->add_column(dlNumeric, name);}
    else                                                                   // largeint
    { mt->add_column(st->ColumnType(i), name);}
   }
   catch(TDynamicListLogicException& e)
   {
    ret      = -1;
    errorstr = e.what();
   }
  }
  else
  {
   // TODO: throw exception
   return (-1);
  }
 } // for

// Данные
  try
  {
   while(st->Fetch())
   {
     mt->append();
     for(int i=1; i <= cols; i++)
     {
      if (!st->IsNull(i))
      {
       switch (st->ColumnType(i))
       {
        case IBPP::sdDate:
            {IBPP::Date d; st->Get(i,d); mt->field_by_number(i-1)->set_date(d); break;}
        case IBPP::sdTime:
            {IBPP::Time d; st->Get(i,d); mt->field_by_number(i-1)->set_time(d); break;}
        case IBPP::sdTimestamp:
            {IBPP::Timestamp d; st->Get(i,d); mt->field_by_number(i-1)->set_timestamp(d); break;}
        case IBPP::sdString:
            {std::string s; st->Get(i,s); mt->field_by_number(i-1)->set_string(cp2koi(s, 1));break;}
        case IBPP::sdSmallint:
             {int v; st->Get(i, v);  mt->field_by_number(i-1)->set_integer(v); break;}
        case IBPP::sdInteger:
            {int v; st->Get(i, v); mt->field_by_number(i-1)->set_integer(v); break;}
        case IBPP::sdLargeint:
            {if (st->ColumnType(i) == IBPP::sdLargeint && st->ColumnScale(i) > 0) // TNumeric
             {int64_t v; TNumeric n;
              st->Get(i,v); n.set_v(v); n.set_scale(st->ColumnScale(i));
              mt->field_by_number(i-1)->set_numeric(n);}
             else {int64_t v; st->Get(i,v); mt->field_by_number(i-1)->set_largeint(v);} // largeint
             break; }
        case IBPP::sdFloat:
            {float d; st->Get(i,d); mt->field_by_number(i-1)->set_float(d); break; }
        case IBPP::sdDouble:
            {double d; st->Get(i,d); mt->field_by_number(i-1)->set_double(d); break; }
        case IBPP::sdBlob:
            {TBytea p; /*st->Get(i,d);*/ // FixMe
              mt->field_by_number(i-1)->set_bytea(p); break; }
        default:
            {// TODO: throw exception
              return (-1);
              break;
            }
       }  // switch
      } // if
      else
      {
       mt->field_by_number(i-1)->set_null();
      }
     } // for
     mt->post();
     ret++;
   } // while
  } // try
  catch (IBPP::Exception& e)
  {
   ret      = -1;
   errorstr = e.what();
  }
  catch(TDynamicListLogicException& e)
  {
   ret      = -1;
   errorstr = e.what();
  }
 return(ret);
}
//---------------------------------------------------------------------------
int TIBPPQuery::st_to_mt(TDynamicList * mt)
{
 return(st_to_mt(mt, st));
}
#endif
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#pragma package(smart_init)
