//---------------------------------------------------------------------------
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stropts.h>
#include <list>
#include <iostream>

#include "log.h"
#include "serial.h"
#include "service.h"

extern TLog * l;
//---------------------------------------------------------------------------
// ---------------------  TSerial ----------------------------------------
//---------------------------------------------------------------------------
TSerial::TSerial(void):
 opened (0),
 runned (0),
 port   ("/dev/ttyS0"),
 flags  (O_RDWR | O_NOCTTY | O_NDELAY),
 sreaded(NULL),
 interval(500),
 serial_port_interval(10)
{
 tfuncptr = SerialThread;
 pthread_mutex_init(&mutex, NULL);
}
//---------------------------------------------------------------------------
TSerial::~TSerial(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 runned = 0;
 close_port();
}
//---------------------------------------------------------------------------
int TSerial::open_port(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (opened == 1) {return fd;}
 fd = open (port.c_str(), flags);  // (O_RDWR | O_NOCTTY | O_NDELAY)
 if (fd == -1)
 {
  opened = 0;
 }
 else 
 {
  termios opt;
  
  opened = 1;
// настройка параметров порта
  fcntl(fd, F_SETFL, FNDELAY);       // читать без блокировок

  if (tcgetattr(fd, &opt) != -1)
  {
//   opt.c_lflag = 0;
   opt.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);  // Выбор неканонического (Raw) ввода
   opt.c_lflag &= ~ECHOPRT;                         // если включены ICANON и IECHO, то символы печатаются по мере того, как они уничтожаются
   opt.c_lflag |= ECHOK;                            // если запущен ICANON, то символ KILL удаляет всю текущую строку      
   opt.c_lflag |= ECHOKE;                           // если ICANON также включен, то KILL обозначается как уничтожение каждого символа в строке
   opt.c_lflag |= ECHOCTL;                          // если запущен ECHO, то управляющие сигналы ASCII, отличающиеся от TAB, NL, START и STOP, отображаются как ^X, где X есть символ из таблицы ASCII с кодом на 0x40 больше, чем у управляющего сигнала
   opt.c_lflag |= NOFLSH;                           // отключить ускоренную запись вводимых и выводимых очередей во время генерации сигналов SIGINT и SIGQUIT и записывать вводимую очередь при генерации сигнала SIGSUSP
   opt.c_lflag |= IEXTEN;                           // включить режим ввода, определяемый реализацией по умолчанию

   opt.c_oflag &= ~OPOST;                           // Выбор необработанного (raw) вывода

//  opt.c_iflag &= ~(IXON | IXOFF | IXANY); // Установка аппаратно управляемого управления потоком передачи данных
//  opt.c_cflag |= CRTSCTS;  // Установка аппаратного управления потоком передаваемых данных
   opt.c_iflag &= ~(IXON | IXOFF);
   opt.c_iflag |= IGNBRK;  // игнорировать режим BREAK при вводе
   opt.c_iflag &= ~ICRNL;  // преобразовывать перевод каретки в конец строки при вводе (пока не будет запущен IGNCR)
   opt.c_cflag &= ~CRTSCTS;

// SERIAL_PARITY_NONE - Отсутствие проверки на четность
   opt.c_cflag &= ~PARENB;
   opt.c_cflag &= ~CSTOPB;  // ONESTOPBIT 
   opt.c_cflag &= ~CSIZE;
   opt.c_cflag |= CS8;      // Установка 8 битов данных

   opt.c_cflag &= ~CRDLY;   // маска задержки перевода каретки. Значениями будут: CR0, CR1, CR2 или CR3.
   opt.c_cflag &= ~CR2;

   cfsetispeed(&opt, B9600);      // 9600
   cfsetospeed(&opt, B9600);

   //opt.c_cc[VEOF  ] = sc->EofChar;
   /* FIXME: sc->ErrorChar is not supported */
   /* FIXME: sc->BreakChar is not supported */
   /* FIXME: sc->EventChar is not supported */
//   opt.c_cc[VSTART] = 0x13;
//   opt.c_cc[VSTOP ] = 0x19;

   opt.c_cc[VEOF/* 4 */]   = 0; // Символ конца файла.
   opt.c_cc[VMIN/* 6 */]   = 0; // Минимальное количество символов для неканонического ввода.
   opt.c_cc[VSTART/* 8 */] = 0; // Символ запуска. Перезапускает вывод, остановленный символом останова.
   opt.c_cc[VSTOP/* 9 */]  = 0; // Символ останова. Приостанавливает вывод до появления символа запуска.

   tcsetattr(fd, TCSAFLUSH, &opt);
//   {
//    int status;
//    ioctl(fd, TIOCMGET, &status);
//    status |= TIOCM_DTR;
//    ioctl(fd, TIOCMSET, status);
//   }

//  printf("c_iflag=%d c_oflag=%d c_cflag=%d c_lflag=%d\n",opt.c_iflag, opt.c_oflag, opt.c_cflag, opt.c_lflag);
//  printf("c_ispeed=%d c_ospeed=%d\n", opt.c_ispeed, opt.c_ospeed);
//  {
//   int i;
//   for (i=0; i< NCCS; i++) printf("c_cc[%d]=%d\n", i, opt.c_cc[i]);
//  } 
  }
  else
  {
   std::cout << "Error: tcgetattr" << std::endl;
  }
 }

 return (fd);
}
//---------------------------------------------------------------------------
void TSerial::close_port(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (opened == 1)
 {
  close(fd);
  opened = 0;
 }
}
//---------------------------------------------------------------------------
int TSerial::read_port(std::list<unsigned char> * lst)
{
 unsigned char buf;
 int ret(0);

 pthread_mutex_lock(&mutex);
 if (opened == 0)
 {
  pthread_mutex_unlock(&mutex);
  return (0);
 }

 lst->clear();
 while(::read(fd, &buf, sizeof(buf)) > 0)
 {
  lst->push_back(buf);
 }

 ret = lst->size();
 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
int TSerial::write_port(std::list<unsigned char> * lst)
{
 int c, ret(0);
 std::list<unsigned char>::iterator itr;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 pthread_mutex_lock(&mutex);

 if (lst->size() == 0 || opened == 0)
 {pthread_mutex_unlock(&mutex); return (ret);}

 for(itr = lst->begin(); itr != lst->end(); ++itr)
 {
  unsigned char v;
  v = *itr;
  c = write(fd, &v, sizeof(v));
  if (c == -1)
  {
   pthread_mutex_unlock(&mutex);
   return (ret);
  }
  nnsleep(serial_port_interval * 1000000);
  ret += c;
 }

 pthread_mutex_unlock(&mutex);
 return (ret);
}
//---------------------------------------------------------------------------
int TSerial::run(void)
{
 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 if (runned == 1) {return (-1);}
 runned = 1;
 pthread_create(&thread_id, NULL, tfuncptr, this);

 return (1);
}
//---------------------------------------------------------------------------
int TSerial::set_speed(speed_t speed)
{
 int ret(0);

 termios opt;

 if (opened != 1) return(ret);

 if (tcgetattr(fd, &opt) != -1)
 {
  cfsetispeed(&opt, speed);
  cfsetospeed(&opt, speed);

  tcsetattr(fd, TCSAFLUSH, &opt);
  ret = 1;
  nnsleep(serial_port_interval * 1000000);
 }

 return(ret);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void * SerialThread(void * p)
{
 TSerial * s;
 s = (TSerial *) p;
 std::list<unsigned char> lst;
 std::list<unsigned char>::iterator itr;

 l->add(LOG_SHOW_TRACE, (std::string)__PRETTY_FUNCTION__);

 pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
 while(s->runned == 1)
 {
  lst.clear();
  s->read_port(&lst);
  if (lst.size() > 0)
  {
   if (s->sreaded != NULL)
   {
    for(itr = lst.begin(); itr != lst.end(); ++itr)
    {
     s->sreaded(*itr, lst.size(), s->sreaded_param);
    }
   }
  nnsleep(s->interval * 100000);
  pthread_testcancel();  
  continue; 
  }

  //sleep(s->interval);
  nnsleep(s->interval * 1000000);
  pthread_testcancel();
 }
 return (NULL);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
