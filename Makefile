#all: route_check.cpp
#	g++ route_check.cpp -o route_check `pkg-config --cflags --libs gtk+-2.0` `pkg-config --cflags --libs cairo`
# Вписать сюда путь к исходникам GTK+
#GTK_SRC  = -I/home/serik/src/gtk+-2.8.10

CC       = g++ #-std=c++0x
LGTK     = -lgtk-x11-2.0 #`pkg-config --libs gtk+-2.0`
CGTK     = `pkg-config --cflags gtk+-2.0`
LGTHREAD = -pthread -lgthread-2.0 -lglib-2.0 #`pkg-config --libs gthread-2.0`
LIBPP    = -L/usr/lib -libpp
CIBPP    = -I/usr/include -I/usr/include/ibpp -DIBPP_LINUX
CDLIST   = -DUSE_DYNAMIC_LIST
#E        = gtkextra-3.3.3
#ENAME    = libgtkextra-x11-3.0.so
#E        = gtk+extra-2.1.2
#ENAME    = gtkextra-x11-2.0
#CGTKEXTRA= -I./$(E)/gtkextra
#LGTKEXTRA= -l$(ENAME) -L./$(E)/gtkextra/.libs
CGTKEXTRA = `pkg-config --cflags gtkextra-2.0` 
LGTKEXTRA = -lgtkextra-x11-2.0 #`pkg-config --libs gtkextra-2.0`

# Добавлено для Debian GNU/Linux.
DESTDIR  = /tmp

all: route_check.o uMainfrm.o uLoginfrm.o ibppquery.o uDataLayer.o service.o uKeyReader.o serial.o timer.o uPackRtItem.o dynamic_list.o resource.o gtkcellrendererxtext.o relation.o parameter.o log.o 
	$(CC) -Wall -W -O2 -o route_check route_check.o uMainfrm.o uLoginfrm.o ibppquery.o uDataLayer.o service.o uKeyReader.o serial.o timer.o uPackRtItem.o dynamic_list.o resource.o gtkcellrendererxtext.o relation.o parameter.o log.o $(LGTK) $(LIBPP) $(LGTHREAD) $(LGTKEXTRA) -Wl,--rpath -Wl,/usr/local/lib
	rm -f svninfo.tmp

install: all
	install -m 755 route_check $(DESTDIR)/usr/bin
	mkdir -p $(DESTDIR)/etc/
	install -m 644 route_check.conf $(DESTDIR)/etc/
	mkdir -p $(DESTDIR)/usr/share/doc/routecheck
	install -m 644 readme_rus.txt $(DESTDIR)/usr/share/doc/routecheck
#	mkdir -p $(DESTDIR)/usr/lib
#	install -m 644 ./$(E)/gtkextra/.libs/lib$(ENAME).so $(DESTDIR)/usr/lib

route_check.o: route_check.cpp route_check.h uMainfrm.o
	$(CC) route_check.cpp -c -o route_check.o $(CGTK) $(CIBPP) $(CGTKEXTRA)

#uMainevn: uMainevn.cpp uMainevn.h
#	$(CC) uMainevn.cpp -c -o uMainevn.o $(CGTK) 

uMainfrm.o: uMainfrm.cpp uMainfrm.h timer.h
	$(CC) uMainfrm.cpp -c -o uMainfrm.o $(CGTK) $(CIBPP) $(CGTKEXTRA)

uLoginfrm.o: uLoginfrm.cpp uLoginfrm.h
	$(CC) uLoginfrm.cpp -c -o uLoginfrm.o $(CGTK) $(CIBPP) $(CGTKEXTRA)

uPackRtItem.o: uPackRtItem.cpp uPackRtItem.h uDataLayer.h 
	$(CC) uPackRtItem.cpp -c -o uPackRtItem.o $(CIBPP)

ibppquery.o: ibppquery.cpp ibppquery.h
	$(CC) ibppquery.cpp -c -o ibppquery.o $(CIBPP) $(CDLIST)

uDataLayer.o: uDataLayer.cpp uDataLayer.h ibppquery.h 
	$(CC) uDataLayer.cpp -c -o uDataLayer.o $(CIBPP) $(CGTK) $(CDLIST)

service.o: service.cpp service.h
	$(CC) service.cpp -c -o service.o $(CIBPP) $(CGTK)

serial.o: serial.cpp serial.h
	$(CC) serial.cpp -c -o serial.o $(CIBPP)

timer.o: timer.cpp timer.h
	$(CC) timer.cpp -c -o timer.o

uKeyReader.o: uKeyReader.h uKeyReader.cpp serial.h
	$(CC) uKeyReader.cpp -c -o uKeyReader.o $(CIBPP)

#linuxlnk.o: ibutton/lib/userial/link/Linux/linuxlnk.c
#	$(CC) ibutton/lib/userial/link/Linux/linuxlnk.c -c -o linuxlnk.o -Iibutton/lib/userial/shared -Iibutton/common

serial_test: serial_test.cpp serial.o uKeyReader.o service.o parameter.o log.o 
	$(CC) serial_test.cpp -c -o serial_test.o
	$(CC) -Wall -W -O2 -o serial_test serial_test.o serial.o uKeyReader.o service.o parameter.o log.o -lpthread -Wl,--rpath -Wl,/usr/local/lib $(LIBPP)

dynamic_list.o: dynamic_list.cpp dynamic_list.h
	$(CC) dynamic_list.cpp -c -o dynamic_list.o $(CIBPP)

relation.o: relation.cpp relation.h
	$(CC) relation.cpp -c -o relation.o

parameter.o: parameter.cpp parameter.h
	$(CC) parameter.cpp -c -o parameter.o

log.o: log.cpp log.h
	$(CC) log.cpp -c -o log.o

#gtktreeview2.o: gtktreeview2.c gtktreeview2.h
#	$(CC) gtktreeview2.c -c -o gtktreeview2.o $(CGTK) -I/home/serik/src/gtk+-2.8.10

# resource
resource.o: resource.cpp
	$(CC) resource.cpp -c -o resource.o $(CGTK)
resource.cpp: pixmaps/secwin.png pixmaps/key_16.png pixmaps/key_24.png pixmaps/linux_logo_64.png
	echo "#include <gtk/gtk.h>"                >  resource.cpp
#	echo "#include <string>"                   >> resource.cpp
	#echo "const guint8 secwin[] = {"           >> resource.cpp
	#perl -w bin2hex.pl --file secwin.png       >> resource.cpp
	#echo "};"                                  >> resource.cpp 
	#echo "const guint8 *psecwin = &secwin[0];" >> resource.cpp
	#echo "unsigned int ssecwin = sizeof(secwin);" >> resource.cpp
	gdk-pixbuf-csource --raw --name=secwin pixmaps/secwin.png >>resource.cpp
	echo "const guint8 *psecwin = &secwin[0];" >> resource.cpp
	gdk-pixbuf-csource --raw --name=key_16 pixmaps/key_16.png >>resource.cpp
	echo "const guint8 *pkey_16 = &key_16[0];" >> resource.cpp
	gdk-pixbuf-csource --raw --name=key_24 pixmaps/key_24.png >>resource.cpp
	echo "const guint8 *pkey_24 = &key_24[0];" >> resource.cpp
	gdk-pixbuf-csource --raw --name=linux_logo_64 pixmaps/linux_logo_64.png >>resource.cpp
	echo "const guint8 *plinux_logo_64 = &linux_logo_64[0];" >> resource.cpp
#svninfo.h:
#	echo "#include <string>"  > svninfo.h
#	echo "std::string svninfo = \"" >> resource.cpp
#	svn info >> svninfo.tmp 
#	echo "\";" >> resource.cpp
#	echo "const char svninfo[] = {"            >> resource.cpp
#	perl -w bin2hex.pl --file svninfo.tmp      >> resource.cpp
#	echo "};"                                  >>  resource.cpp
#	echo "const char *psvninfo = &svninfo[0];" >>  resource.cpp 

#svninfo.tmp:
#	svn info > svninfo.tmp

clean:
	rm -f *.o *.bak route_check dlist_test resource.cpp

bz2: clean
	tar cf - ./ | bzip2 -f > ../route_check.tar.bz2

cell: main.o custom-cell-renderer-progressbar.o
	gcc -Wall -W -O2 -o cell main.o custom-cell-renderer-progressbar.o $(LGTK) -Wl,--rpath -Wl,/usr/local/lib

main.o: main.c
	gcc main.c -c -o main.o $(CGTK) 

gtkcellrendererxtext.o: gtkcellrendererxtext.cpp gtkcellrendererxtext.h
	$(CC) gtkcellrendererxtext.cpp -c -o gtkcellrendererxtext.o $(CGTK)

custom-cell-renderer-progressbar.o: custom-cell-renderer-progressbar.c custom-cell-renderer-progressbar.h
	gcc custom-cell-renderer-progressbar.c -c -o custom-cell-renderer-progressbar.o $(CGTK)

dynamic_test: dynamic_test.o dynamic_list.o relation.o
	$(CC) -Wall -W -O2 -o dynamic_test dynamic_list.o relation.o dynamic_test.o $(LIBPP) -Wl,--rpath -Wl,/usr/local/lib

dynamic_test.o: dynamic_test.cpp dynamic_list.h relation.h
	$(CC) dynamic_test.cpp -c -o dynamic_test.o $(CIBPP)
